# Generated by Django 2.0.1 on 2018-01-25 22:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0014_group_company'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='group',
            name='can_select',
        ),
        migrations.RemoveField(
            model_name='group',
            name='system_profile',
        ),
        migrations.AddField(
            model_name='group',
            name='is_default',
            field=models.BooleanField(default=False, verbose_name='Es un grupo predefinido?'),
        ),
        migrations.AddField(
            model_name='group',
            name='is_public',
            field=models.BooleanField(default=True, verbose_name='Esta reservado?'),
        ),
        migrations.AlterField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='Perfiles de usuario', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name=''),
        ),
    ]
