function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}
function validatePhone(phoneNumber){
   //var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;  
   var phoneNumberPattern = /^(\+91-|\+91|0)?\d{10}$/;  
   return phoneNumberPattern.test(phoneNumber); 
}
function remove_this_file(id, filename)
{
	var file_upload_val = $('input#_multiple_files')[0].files;
	//console.log(file_upload_val.length+'__'+filename.length+'__'+filename);
	$("#removed_files").append('<input type="hidden" class="input_box" id="'+id+'" value="'+id+'">');
		$("#add_fileID"+id).hide();
	var remove_file_len = $("#removed_files input[type='hidden']").length;
	$('input#_multiple_files')[0].files.length = file_upload_val-remove_file_len;
	
	if(remove_file_len == file_upload_val.length){
		$(".submit").attr('disabled','disabled');
	}
	return false;
}

function send_mail_fn(send_mail_id){
	var user_name = $("#user_name").val();
	var user_email = $("#user_email").val();
	var user_phone = $("#user_phone").val();
	var user_desc = $("#user_desc").val();
	var file_upload_val = $('input#_multiple_files')[0].files;
	//console.log($('input[name="check"]:checked').serialize());
	var services = "";
	$('input[name="check"]:checked').each(function() {
	   services += this.value+','; 
	});
	
	services = services.slice(0, -1);
	    
	if(send_mail_id == "skip_this_step"){
		post_skip_data = {
			'send_mail_id':send_mail_id, 
			'user_name':user_name, 
			'user_email':user_email, 
			'user_phone':user_phone, 
			'services':services
		};
			//Ajax post data to server
		$.ajax({
			type:"POST",
			url:'send_mail.php',
			dataType: "json",
			data:post_skip_data, 
			cache: false,
			beforeSend: function() 
			{
				output = '<div class="succ_img"><img src="img/uploading.png"/></div><div class="success">Uploading...</div>';
				$("#popup").html(output);
				$('#popup').bPopup({
					modalClose: false,
					opacity: 0.6,
					positionStyle: 'fixed'
				});
			},
			success:function(response) 
			{
				var output = "";
				if(response.type == 'error'){ //load json data from server and output message     
					output = '<div class="error">'+response.text+'</div>';
				} else {
					output = '<div class="succ_img"><img src="img/success.png"/></div><div class="success">'+response.text+'</div>';
				}
				$("#popup").html(output);
				setTimeout(function(){
					$('#popup').bPopup().close();
				}, 1000);
				
				$("form.cell input[type='text'],form.cell input[type='email'],form.cell input[type='tel'],form.cell input[type='file'], form.cell textarea").val(''); 
				$('form.cell input[type="checkbox"]').removeAttr("checked");
				$(".form_row,.submit,.skip_step,.counter").hide();
				$(".file_boxes").html("");
				$(".submit").attr('disabled','disabled');
				$(".current.form_row,.next").show();
				$(".start_project").hide();
			}
			
		});
	} else {
		var m_data = new FormData();
		m_data.append( 'send_mail_id', send_mail_id);
		m_data.append( 'user_name', user_name);
		m_data.append( 'user_email', user_email);
		m_data.append( 'user_phone', user_phone);
		m_data.append( 'services', services);
		m_data.append( 'user_desc', user_desc);
		m_data.append( 'file_attach',$('input#_multiple_files')[0].files[0]);
		//console.log(m_data);
		$.ajax({
			type:"POST",
			url:'send_mail.php',
			dataType: "json",
			contentType: false,
			processData: false,
			data:m_data,
			cache: false,
			beforeSend: function() 
			{
				output = '<div class="succ_img"><img src="img/uploading.png"/></div><div class="success">Uploading...</div>';
				$("#popup").html(output);
				$('#popup').bPopup({
					modalClose: false,
					opacity: 0.6,
					positionStyle: 'fixed'
				});
			},
			success:function(response) 
			{
				var output = "";
				if(response.type == 'error'){ //load json data from server and output message     
					output = '<div class="error">'+response.text+'</div>';
				} else {
					output = '<div class="succ_img"><img src="img/success.png"/></div><div class="success">'+response.text+'</div>';
				}
				$("#popup").html(output);
				setTimeout(function(){
					$('#popup').bPopup().close();
				}, 1000);
				
				
				$("form.cell input[type='text'],form.cell input[type='email'],form.cell input[type='tel'],form.cell input[type='file'], form.cell textarea").val(''); 
				$('form.cell input[type="checkbox"]').removeAttr("checked");
				$(".file_boxes").html("");
				$(".submit").attr('disabled','disabled');
				$(".form_row,.submit,.skip_step,.counter").hide();
				$(".current.form_row,.next").show();
				$(".start_project").hide();
			}		
		});
	}
}
$(document).ready(function(){
	$(".start_pro_btn").click(function(){
		$(".start_project").show();
		//$(".form_row,.submit,.skip_step").hide();
		//$(".current.form_row,.next").show();
		setTimeout(function(){
		  $(".input_box").focus();
		}, 1000);
	});
	$(".start_project .close_icon").click(function(){
		$(".start_project").fadeOut();
	});
	/*---------------------------Start Code for Start your Project-------------------------------*/
	$('.next').on('click', function() {
		var btn_id = $(this).attr('id');
		//console.log($('#'+btn_id).parent().parent().attr("id"));
		$('#questions>div.form_row').each(function() {
			var id = $(this).index();
			if ($(this).is(':visible')) {
				$(this).hide();
				if($("#"+id).css('display') == "block"){
					
					$(".next").removeAttr('disabled');
				} else {
					$(".next").attr('disabled','disabled');
				}
				if (id == $('#questions>div.form_row').length - 2) {
					$('#questions>div.form_row').eq(3).fadeIn(1000);
					$('#questions>div.form_row .input_box').eq(3).focus();
					$(".next").hide();
					$(".submit,.skip_step,.counter").fadeIn(1000);
				} else {
					$('#questions>div.form_row').eq(id + 1).fadeIn(1000);
					$('#questions>div.form_row .input_box').eq(id + 1).focus();
				}
				return false;
				
			}
		});
	});
	$('.input_box').on('change keydown keypress keyup drop', function(event){
		var input_id = $(this).attr('id');
		var input_val = $("#"+input_id).val();
		var user_name = $("#user_name").val();
		var input_val_length = user_name.replace(/ /g,'').length;
		var user_email = $("#user_email").val();
		var user_phone = $("#user_phone").val();
		var user_desc = $("#user_desc").val();
		//var file_upload_val = $("#_multiple_files").val();
		var file_upload_val = $('input#_multiple_files')[0].files;
		//var text_area_len = user_desc.replace(/ /g,'').length;
		var text_area_len = user_desc.length;
		var empty = false;
		//console.log(file_upload_val+'__'+user_desc);
		if($("#user_name_form_row").css('display') == "block"){
			if(user_name != '' && input_val_length >=3) { // hvis feltet er tomt
				$(".next").removeAttr('disabled');
			} else {
				$(".next").attr('disabled','disabled');
			}
		}
		if($("#user_email_form_row").css('display') == "block"){
			//console.log(user_email);
			if(user_email != '' && user_phone != '') { // hvis feltet er tomt
				if(validateEmail(user_email) && validatePhone(user_phone)) {
					$(".next").removeAttr('disabled');	
				} else {
					//console.log('false');
					$(".next").attr('disabled','disabled');
				}
			} else {
				//console.log('false');
				$(".next").attr('disabled','disabled');
			}
			 
		} 
		
		var checkboxes = $("form.cell input[type='checkbox']");
		checkboxes.click(function() {
			if(checkboxes.is(":checked")){
				$(".next").removeAttr('disabled');
			} else {
				$(".next").attr('disabled','disabled');
			
			}
		});
		
		if($("#user_desc_form_row").css('display') != "none"){
			var txtarea_ht = $("#user_desc").prop('scrollHeight');
			var total_len = 140 - text_area_len;
			if(total_len  >= 0){
				$(".counter .count_text").html(140 - text_area_len);
			} else {
				$(".counter .count_text").html('0');
			}
			if(text_area_len != 140){
				$(".counter .count").html(text_area_len);
			} else {
				$(".counter .count").html('140');
			}
			if(user_desc == "" && file_upload_val.length == ""){
				$(".submit").attr('disabled','disabled');
			} else {
				if(user_desc != ""){
					if(text_area_len == 140) { 
						$(".submit").removeAttr('disabled');
					} else {
						$("#user_desc").removeAttr();
						$(".submit").attr('disabled','disabled');
					}
				}
				remove_file_len = $("#removed_files input[type='hidden']").length;
				
				//console.log(remove_file_len);
				if(remove_file_len != file_upload_val.length){
					if(file_upload_val.length != ""){
						
						$(".submit").removeAttr('disabled');
					}
				}
			}
		}
	});
	$("input#_multiple_files").on('change',function(){
			//console.log(file_upload_val);
			var file_upload_val = $(this)[0].files;
			//console.log(file_upload_val);
			//console.log(file_upload_val.length);
			//file_upload_val = value;
			if(file_upload_val.length > 0)
			{
				var added_files_displayer = file_id = "";
				for(var i = 0; i<file_upload_val.length; i++)
				{
					var files_name_without_extensions = file_upload_val[i].name.substr(0, file_upload_val[i].name.lastIndexOf('.')) || file_upload_val[i].name;
					file_id = files_name_without_extensions.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');
							
					var fileSize = (file_upload_val[i].size / 1024);

					if(typeof file_upload_val[i] != undefined && file_upload_val[i].name != ""){
						added_files_displayer += '<div id="add_fileID'+file_id+'" class="bx_main"><div class="bx_2"><span class="fname">'+file_upload_val[i].name.substring(0, 30)+'</span> </div><div class="bx_3 input_box" id="bx_3_'+file_id+'"> <a htef="#null" onclick="remove_this_file(\''+file_id+'\',\''+file_upload_val[i].name+'\');" class="input_box"> <img border="0" src="./img/remove_icon.png" > </div> </div>';
					}
				}
				$(".file_boxes").html("");
				$("#removed_files").html("");
				$(".file_boxes").append(added_files_displayer);
			}
		});
	/*---------------------------End Code for Start your Project-------------------------------*/
});