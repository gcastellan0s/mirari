$(document).ready(function(){
	var screen_height = window.innerHeight;
	var scrollbar_ht = screen_height-$('.menu_logo').height();
	var portfolio_ban_ht_txt = screen_height-90;
	
	$(".menu").css('height',screen_height+'px');
	$("#menuScrollbar").css('height',scrollbar_ht+'px');
	$(".section .intro").css('padding-top',portfolio_ban_ht_txt+'px');
	
	$(".menu-opener").on('click',function(){
		$("#menuScrollbar").mCustomScrollbar({
			theme:"minimal-dark"
		});
		$(".menu-opener, .menu-opener-inner, .menu").toggleClass("active");
		if ($(".menu").hasClass("active")) {
			$('.fullpage').fullpage.setMouseWheelScrolling(false);
			$('.fullpage').fullpage.setAllowScrolling(false);
			$('.fullpage').fullpage.setKeyboardScrolling(false);
			$(".logo").css('opacity', '0.4');
			$(".menu__overlay").css({'display': 'block', 'opacity': '0.7'});
		} else {
			$('.fullpage').fullpage.setMouseWheelScrolling(true);
			$('.fullpage').fullpage.setAllowScrolling(true);
			$('.fullpage').fullpage.setKeyboardScrolling(true);
			$(".logo").css('opacity', '1');
			$(".menu__overlay").css({'display': 'none', 'opacity': '0'});
		}
	});
	
	$(".menu__overlay").on('click',function(){
		$(".menu-opener, .menu-opener-inner, .menu").removeClass("active");
		$('.fullpage').fullpage.setMouseWheelScrolling(true);
		$('.fullpage').fullpage.setAllowScrolling(true);
		$('.fullpage').fullpage.setKeyboardScrolling(true);
		$(".logo").css('opacity', '1');
		$(".menu__overlay").css({'display': 'none', 'opacity': '0'});
	});
	
	
	$("#portfolio").click(function(){
		$(".port_menu").slideToggle();
	});
	
	$('.fullpage').fullpage({
		//anchors: ['project-1', 'project-2', 'project-3','project-4','project-5','project-6','project-7','project-8','project-9','project-10','project-11','footer'],
		navigation: true,
		navigationPosition: 'right',
		verticalCentered: false
		//navigationTooltips: ['First page', 'Second page', 'Third','forth page','last page']
	});
	$(window).resize(function(){
		var screen_height = window.innerHeight;
		var scrollbar_ht = screen_height-$('.menu_logo').height();
		var portfolio_ban_ht_txt = screen_height-90;
		$(".menu").css('height',screen_height+'px');
		$("#menuScrollbar").css('height',scrollbar_ht+'px');
		$(".section .intro").css('padding-top',portfolio_ban_ht_txt+'px');
	});
});

function view_pro_clk(){
	//alert('wel');
	$('.fullpage').fullpage.setMouseWheelScrolling(false);
	$('.fullpage').fullpage.setAllowScrolling(false);
	$('.fullpage').fullpage.setKeyboardScrolling(false);
}
