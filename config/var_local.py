VAR_LOCAL = [{
	'DEFAULT': 
	{	
		'NAME':'MIRARI',
		'COLOR_DEFAULT':'#2b2b2b',#color principal
		'COLOR_1':'#FFFFFF',#color que contraste principal
		'COLOR_2':'#2b2b2b',#siempre obscuro
		'COLOR_3':'#FFFFFF',#siempre claro
		'COLOR_4':'#6d6c39',#color secundario para decoración resalta
		'COLOR_5':'#8a8a8a',#color para los comentarios
		'ICO':'img/default_ico.png',
		'SELLPOINT':{
			'TICKETSIZE':'248',
			'IVA':.16,
			'IEPS':.08,
			'COST1':800,
			'COST2':600,
			'COST3':400,
		},
		'PAGES':{
			'LANDING':'default/',
			'DESCRIPTION':'BY ZARESDELWEB',
			'SLOGAN':'ERP',
			'DOMAIN':'mirari.mx',
			'IMAGE':'img/ogindex.jpg',
			'STATIC':'pages/default/',
			'TITLE':'MIRARI :: MX',
		},
	},
	'PASTELERIASLAESTRELLA': 
	{	
		'NAME':'GRUPO ESTRELLA',
		'COLOR_DEFAULT':'#4a0000',#color principal
		'COLOR_1':'#FFFFFF',#color que contraste principal
		'COLOR_2':'#2b2b2b',#siempre obscuro
		'COLOR_3':'#FFFFFF',#siempre claro
		'COLOR_4':'#6d6c39',#color secundario para decoración resalta
		'COLOR_5':'#8a8a8a',#color para los comentarios
		'ICO':'pages/pasteleriaslaestrella/img/default_ico.png',
		'SELLPOINT':{
			'TICKETSIZE':'237',
			'IVA':.16,
			'IEPS':.08,
			'COST1':800,
			'COST2':600,
			'COST3':400,
		},
		'PAGES':{
			'LANDING':'pasteleriaslaestrella/',
			'DESCRIPTION':'Somos una empresa  orgullosamente mexicana. Fabricamos productos de la más alta calidad, gran tamaño y buen precio. nuestro pan es artesanal, con ingredientes naturales, hecho por manos mexicanas, que ponen el corazón en cada pieza.',
			'SLOGAN':'Comienza tu día con aroma a pan',
			'DOMAIN':'pasteleriaslaestrella.pasteleriaslaestrella.com',
			'IMAGE':'img/ogindex.jpg',
			'STATIC':'pages/pasteleriaslaestrella/',
			'TITLE':'PASTELERIAS LA ESTRELLA',
		},
	},
	'CREDIPYME': 
	{	
		'NAME':'CREDIPYME',
		'COLOR_DEFAULT':'#141831',#color principal
		'COLOR_1':'#FFFFFF',#color que contraste principal
		'COLOR_2':'#2b2b2b',#siempre obscuro
		'COLOR_3':'#FFFFFF',#siempre claro
		'COLOR_4':'#141831',#color secundario para decoración resalta
		'COLOR_5':'#8a8a8a',#color para los comentarios
		'ICO':'pages/credipyme/img/default_ico.png',
		'PAGES':{
			'LANDING':'credipyme/',
			'DESCRIPTION':'Unión de crédito',
			'SLOGAN':'Unión de crédito',
			'DOMAIN':'credipyme.mx',
			'IMAGE':'img/ogindex.jpg',
			'STATIC':'pages/credipyme/',
			'TITLE':'CREDIPyME ',
		},
	},
	'MIRARI': 
	{	
		'NAME':'MIRARI',
		'COLOR_DEFAULT':'#141831',#color principal
		'COLOR_1':'#FFFFFF',#color que contraste principal
		'COLOR_2':'#2b2b2b',#siempre obscuro
		'COLOR_3':'#FFFFFF',#siempre claro
		'COLOR_4':'#141831',#color secundario para decoración resalta
		'COLOR_5':'#8a8a8a',#color para los comentarios
		'ICO':'pages/mirari/assets/img/ico.png',
		'PAGES':{
			'LANDING':'mirari/',
			'DESCRIPTION':'',
			'SLOGAN':'Creamos aplicaciones',
			'DOMAIN':'mirari.mx',
			'IMAGE':'img/ogindex.jpg',
			'STATIC':'pages/mirari/',
			'TITLE':'Mirari',
		},
	},
	'TAHONA': 
	{	
		'NAME':'TAHONA',
		'COLOR_DEFAULT':'#1b1d3b',#color principal
		'COLOR_1':'#FFFFFF',#color que contraste principal
		'COLOR_2':'#2b2b2b',#siempre obscuro
		'COLOR_3':'#FFFFFF',#siempre claro
		'COLOR_4':'#141831',#color secundario para decoración resalta
		'COLOR_5':'#8a8a8a',#color para los comentarios
		'ICO':'pages/tahona/img/tahonaico.png',
		'ANDROID':{
			'USER':'tahona.mirari@gmail.com',
			'PASS':'Tahona.MX',
		},
		'SELLPOINT':{
			'TICKETSIZE':'244',
			'IVA':.16,
			'IEPS':.08,
			'COST1':800,
			'COST2':600,
			'COST3':400,
		},
		'PAGES':{
			'LANDING':'tahona/',
			'DESCRIPTION':'Boutique de Pan, desde 1960. Panaderías y Pastelerías, además contamos con pollos rostizados a la leña, chocolatería, tortería, restaurante, etc.',
			'SLOGAN':'AMAMOS EL PAN',
			'DOMAIN':'tahona.mx',
			'IMAGE':'images/ogindex.png',
			'STATIC':'pages/tahona/',
			'TITLE':'TAHONA',
		},
	},
	'MEXICOF': 
	{	
		'NAME':'MEXICOF',
		'COLOR_DEFAULT':'#1b1d3b',#color principal
		'COLOR_1':'#FFFFFF',#color que contraste principal
		'COLOR_2':'#2b2b2b',#siempre obscuro
		'COLOR_3':'#FFFFFF',#siempre claro
		'COLOR_4':'#141831',#color secundario para decoración resalta
		'COLOR_5':'#8a8a8a',#color para los comentarios
		'ICO':'pages/mexicof/img/mexicofico.png',
		'PAGES':{
			'LANDING':'mexicof/',
			'DESCRIPTION':'',
			'SLOGAN':'Centro de servicio',
			'DOMAIN':'mexicof.com',
			'IMAGE':'images/ogindex.png',
			'STATIC':'pages/mexicof/',
			'TITLE':'México Fitness',
		},
	},
	'INOTEC': 
	{	
		'NAME':'INOTEC',
		'ICO':'pages/mexicof/img/inotec.png',
		'PAGES':{
			'LANDING':'inotec/',
			'DESCRIPTION':'',
			'SLOGAN':'Innovando Soluciones',
			'DOMAIN':'inotec.mx',
			'IMAGE':'images/ogindex.png',
			'STATIC':'pages/inotec/',
			'TITLE':'INOTEC',
			'CONTACT':'teo.torres22@gmail.com',
		},
	},
	'MAGICPROSERIES': 
	{	
		'NAME':'MAGICPROSERIES',
		'COLOR_DEFAULT':'#2b2b2b',#color principal
		'COLOR_1':'#FFFFFF',#color que contraste principal
		'COLOR_2':'#2b2b2b',#siempre obscuro
		'COLOR_3':'#FFFFFF',#siempre claro
		'COLOR_4':'#6d6c39',#color secundario para decoración resalta
		'COLOR_5':'#8a8a8a',#color para los comentarios
		'ICO':'pages/magicproseries/images/icon.png',
		'PAGES':{
			'LANDING':'magicproseries/',
			'DESCRIPTION':'',
			'SLOGAN':'YO SOY PRO',
			'DOMAIN':'magicproseries.com',
			'IMAGE':'images/ogindex.png',
			'STATIC':'pages/magicproseries/',
			'TITLE':'MPS',
			'LOGIN_BACKGROUND':'images/login_bg.jpg',
			'SIGNIN':True,
		},
	},
}]

VAR_GLOBAL = [{
	'FONT_AWESOME':'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
	'VUE':'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
	'SOCKETIO':'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
	'MASONRY':'https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.1/masonry.pkgd.min.js',
	'SYTEM_NAME': 'MIRARI',
	'SIZE_DASHBOARD':380,
	'GOOGLEMAPS':'AIzaSyBkthehObTcDFk_YuWTgXmy4QFtUIHsqUY',
}]




