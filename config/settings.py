from .var_production import VAR_PRODUCTION
import os
from django.utils import six

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = ')z2k*1wi5g2g45cd77a)3m^w+a*uk7(8+j4p57yno2m7j95g(1'

DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    # My apps...
    'pages',
    'project',
    'sellpoint',
    'invoice',
    'financialpartners',
    'intranet',
    'magicproseries',
    #'intranet',
    #'credito',
    #'pages',
    # Other apps...
    'django_user_agents',
    'mptt',
    'rest_framework',
    'djmoney',
    'storages',
    'crispy_forms',
    'localflavor',
    'django_countries',
    'corsheaders'
)

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_user_agents.middleware.UserAgentMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates/'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'


#DATABASES = {
    #'default': {
        #'ENGINE': 'django.db.backends.postgresql',
        #'NAME': 'mirari',
        #'USER': 'mirari',
        #'PASSWORD': 'E1S5wXFJh:l>t7)e6uvj5Z7X*17>M2',
        #'HOST': 'mirari.crxawrf1kvug.us-west-1.rds.amazonaws.com',
        #'PORT': '5432',
    #},
#}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'mirari',
        'USER': 'mirari',
        'PASSWORD': VAR_PRODUCTION[0]['VAR_DATABASE_PASSWORD'],
        'HOST': 'dbmirari.crxawrf1kvug.us-west-1.rds.amazonaws.com',
        'PORT': '5432',
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

USER_AGENTS_CACHE = 'default'

AUTH_PASSWORD_VALIDATORS = [
    #{
        #'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    #},
    #{
        #'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    #},
    #{
        #'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    #},
    #{
        #'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    #},
]

# EMAIL
# ------------------------------------------------------------------------------
DEFAULT_FROM_EMAIL = VAR_PRODUCTION[0]['VAR_DEFAULT_FROM_EMAIL']
EMAIL_HOST = 'email-smtp.us-west-2.amazonaws.com'
EMAIL_HOST_PASSWORD = VAR_PRODUCTION[0]['VAR_EMAIL_HOST_PASSWORD']
EMAIL_HOST_USER = VAR_PRODUCTION[0]['VAR_EMAIL_HOST_USER']
EMAIL_PORT = '587'
EMAIL_SUBJECT_PREFIX = 'PASTELERIASLAESTRELLA'
EMAIL_USE_TLS = True


LANGUAGE_CODE = 'es-MX'
TIME_ZONE = 'America/Mexico_City'
USE_I18N = True
USE_L10N = True
USE_TZ = True

AUTH_USER_MODEL = 'project.User'
LOGIN_URL = 'project:login_user'
LOGIN_REDIRECT_URL = 'project:login_user'

AUTOSLUG_SLUGIFY_FUNCTION = 'slugify.slugify'
CRISPY_TEMPLATE_PACK = 'bootstrap4'

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static/'),
)

MEDIA_URL = '/media/'

SOCKETIO = 8001
SOCKETIODOMAIN = 'localhost'

CRISPY_TEMPLATE_PACK = 'bootstrap4'

if 'PRODUCTION' in VAR_PRODUCTION[0]:

    VAR_PRODUCTION = VAR_PRODUCTION[0]

    if VAR_PRODUCTION['PRODUCTION'] == True:

        from boto.s3.connection import OrdinaryCallingFormat

        AWS_ACCESS_KEY_ID = VAR_PRODUCTION['VAR_DJANGO_AWS_ACCESS_KEY_ID']
        AWS_SECRET_ACCESS_KEY = VAR_PRODUCTION['VAR_AWS_SECRET_ACCESS_KEY']
        AWS_STORAGE_BUCKET_NAME = VAR_PRODUCTION['VAR_AWS_STORAGE_BUCKET_NAME']

        AWS_AUTO_CREATE_BUCKET = True
        AWS_QUERYSTRING_AUTH = False
        AWS_S3_CALLING_FORMAT = OrdinaryCallingFormat()
        AWS_EXPIRY = 60 * 60 * 24 * 7
        AWS_HEADERS = {
            'Cache-Control': six.b('max-age=%d, s-maxage=%d, must-revalidate' % (AWS_EXPIRY, AWS_EXPIRY))
        }
        AWS_S3_OBJECT_PARAMETERS = {
            'CacheControl': 'max-age=86400',
        }
        #EMAIL_HOST = VAR_PRODUCTION['VAR_EMAIL_HOST']
        #EMAIL_HOST_USER = VAR_PRODUCTION['VAR_EMAIL_HOST_USER']
        #EMAIL_HOST_PASSWORD = VAR_PRODUCTION[' VAR_EMAIL_HOST_PASSWORD']
        
        DEFAULT_FILE_STORAGE = 'config.s3utils.MediaRootS3BotoStorage'
        MEDIA_URL = 'https://s3.amazonaws.com/%s/' % VAR_PRODUCTION['VAR_AWS_STORAGE_BUCKET_NAME']
        MEDIA_ROOT = os.path.join(BASE_DIR, VAR_PRODUCTION['VAR_AWS_STORAGE_BUCKET_NAME']+'/media')
        StaticRootS3BotoStorage = lambda: S3BotoStorage(location='static')
        STATICFILES_STORAGE = 'config.s3utils.StaticRootS3BotoStorage'
        STATIC_URL = 'https://s3.amazonaws.com/%s/static/' % VAR_PRODUCTION['VAR_AWS_STORAGE_BUCKET_NAME']

        AWS_PRELOAD_METADATA = True

        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.postgresql',
                'NAME': VAR_PRODUCTION['VAR_DATABASE_NAME'],
                'USER': 'mirari',
                'PASSWORD': VAR_PRODUCTION['VAR_DATABASE_PASSWORD'],
                'HOST': 'dbmirari.crxawrf1kvug.us-west-1.rds.amazonaws.com',
                'PORT': '5432',
            },
            
        }

        SECRET_KEY = VAR_PRODUCTION['VAR_SECRET_KEY']
        DEBUG = True
        PRODUCTION = VAR_PRODUCTION['PRODUCTION']
        
        SOCKETIO = 8001
        SOCKETIODOMAIN = 'mirari.mx'
