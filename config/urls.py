from django.contrib import admin
from django.urls import include, path

urlpatterns = [
	path('', include('pages.urls')),
 	path('', include('project.urls')),
 	path('', include('invoice.urls')),
 	path('', include('sellpoint.urls')),
 	path('', include('financialpartners.urls')),
 	path('', include('intranet.urls')),
 	path('', include('magicproseries.urls')),
    path('admin/', admin.site.urls),
]
