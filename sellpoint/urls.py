from django.urls import path
from .views import *

app_name = "sellpoint"

urlpatterns = [
   path('puntos-de-venta/', SellpointList.as_view(), name='sellpoint-list'),
   path('punto-de-venta/agregar/', SellpointCreate.as_view(), name='sellpoint-add'),
   path('punto-de-venta/actualizar/<int:pk>/', SellpointUpdate.as_view(), name='sellpoint-update'),
   path('punto-de-venta/eliminar/<int:pk>/', SellpointDelete.as_view(), name='sellpoint-delete'),

   path('menus/', MenuList.as_view(), name='menu-list'),
   path('menu/agregar/', MenuCreate.as_view(), name='menu-add'),
   path('menu/actualizar/<int:pk>/', MenuUpdate.as_view(), name='menu-update'),
   path('menu/eliminar/<int:pk>/', MenuDelete.as_view(), name='menu-delete'),

   path('productos/', ProductList.as_view(), name='product-list'),
   path('producto/agregar/', ProductCreate.as_view(), name='product-add'),
   path('producto/ver/<int:pk>/', ProductDetail.as_view(), name='product-detail'),
   path('producto/actualizar/<int:pk>/', ProductUpdate.as_view(), name='product-update'),
   path('producto/eliminar/<int:pk>/', ProductDelete.as_view(), name='product-delete'),

   path('facturas/', InvoiceList.as_view(), name='invoice-list'),
   path('factura/ver/<int:pk>/', InvoiceDetail.as_view(), name='invoice-detail'),

   path('atributo-de-producto/actualizar/<int:pk>/', Product_attributesUpdate.as_view(), name='product_attributes-update'),

   path('tickets/', TicketList.as_view(), name='ticket-list'),

   path('cortes/', CutList.as_view(), name='cut-list'),
   path('corte/ver/<int:pk>/', CutDetail.as_view(), name='cut-detail'),
   path('corte/ver_/<int:pk>/', CutDetail_.as_view(), name='cut-detail-'),
   path('corte/ver/<int:pk>/porciento/<int:cant>/', CutDetail_porciento.as_view(), name='cut-porciento'),
   path('corte/ver/<int:pk>/<int:cant>/', CutDetail_porciento_.as_view(), name='cut-porciento_'),

   path('punto-de-venta/vendedor/', sellpoint_vendor, name='sellpoint_vendor'),
   path('punto-de-venta/cajero/', sellpoint_casher, name='sellpoint_casher'),

   path('sellpoint-api/', sellpoint_api, name='sellpoint_api'),
]
