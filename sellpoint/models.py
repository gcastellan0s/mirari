# -*- encoding: utf-8 -*-
from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from config.var_local import VAR_LOCAL, VAR_GLOBAL
from mptt.models import MPTTModel, TreeForeignKey
from django.urls import reverse_lazy, reverse
import datetime
from django.db.models.signals import m2m_changed
from djmoney.money import Money
from django.utils import timezone
from invoice.models import FiscalData, Invoice
from django.utils.html import mark_safe
import json
import re

def path_sellpoint_logo(self, filename):
	date = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
	upload_to = "companys/%s_%s/sellpoints/%s_%s/logo/%s__%s" % (self.company.id, self.company.name, self.id, self.name, date, filename)
	return upload_to

type_ = (
	('Panaderia','Panaderia'),
)

class Sellpoint(models.Model):

	SELLPOINT_TYPE = (
		('PUBLICO', 'PUBLICO'),
		('MAYOREO', 'MAYOREO'),
		('TRANSITO', 'TRANSITO'),
		('PEDIDOS', 'PEDIDOS'),
	)
	name = models.CharField('Nombre del punto de venta', max_length=250)
	alias = models.CharField('Alias punto de venta', max_length=250, blank=True, null=True)
	sellpoint_type = models.CharField('Tipo de Punto de venta', max_length=250, choices=SELLPOINT_TYPE, default="PUBLICO")
	company = models.ForeignKey('project.Company',blank=True, null=True, related_name='+', on_delete=models.CASCADE)
	serial = models.ForeignKey('project.Serial', related_name='+', on_delete=models.SET_NULL, blank=True, null=True, verbose_name='', help_text='Asocia una serie a este punto de venta. <strong>Déjalo vacio para asignar folios aleatorios </strong>')
	creation_date = models.DateTimeField(auto_now_add=True)
	modified_date = models.DateTimeField(auto_now=True)
	is_active = models.BooleanField('Esta activa?', default=True, help_text='Desactiva un punto de venta, ningún usuario podrá acceder.')
	auto_cash = models.BooleanField('Cobran vendedores?', default=False, help_text='Tienes vendedores autorizados para cobrar en esta sucursal?')
	invoice_in_point = models.BooleanField('Factura en sitio?', default=False, help_text='Tiene un punto de facturación en el mismo sitio?')
	cut_in_point = models.BooleanField('Corte en sitio?', default=False, help_text='Puede realizar cortes en el mismo sitio?')
	number_tickets = models.IntegerField('Numero de tickets que imprime', default=1)
	logo = ProcessedImageField(blank=True, null=True, upload_to=path_sellpoint_logo, processors=[ResizeToFill(600, 600)],format='JPEG',options={'quality': 60})
	cost = models.FloatField(default=0)
	active = models.BooleanField(default=True)
	ras = models.PositiveIntegerField(default=100)
	color = models.CharField('Color', max_length=100, blank=True, null=True)
	have_orders = models.BooleanField('Se usa para pedidos?', default=False)
	rfc_cuts = models.ForeignKey('invoice.FiscalData', related_name='+', on_delete=models.SET_NULL, blank=True, null=True, verbose_name='', help_text='RFC de facturación para cortes. <strong>Déjalo vacio para desactivar la facturación </strong>')
	rfc_tickets = models.ForeignKey('invoice.FiscalData', related_name='+', on_delete=models.SET_NULL, blank=True, null=True, verbose_name='', help_text='RFC de facturación para clientes. <strong>Déjalo vacio para desactivar la facturación </strong>')
	header_line_black_1 = models.CharField('Linea 1 del encabezado del ticket', max_length=80, blank=True, null=True)
	header_line_black_2 = models.CharField('Linea 2 del encabezado del ticket', max_length=80, blank=True, null=True)
	header_line_black_3 = models.CharField('Linea 3 del encabezado del ticket', max_length=80, blank=True, null=True)
	header_line_1 = models.CharField('Linea 1 del texto del ticket', max_length=80, blank=True, null=True)
	header_line_2 = models.CharField('Linea 2 del texto del ticket', max_length=80, blank=True, null=True)
	header_line_3 = models.CharField('Linea 3 del texto del ticket', max_length=80, blank=True, null=True)
	header_line_4 = models.CharField('Linea 4 del texto del ticket', max_length=80, blank=True, null=True)
	header_line_5 = models.CharField('Linea 5 del texto del ticket', max_length=80, blank=True, null=True)
	footer_line_1 = models.CharField('Linea 1 del pie del ticket', max_length=80, blank=True, null=True)
	footer_line_2 = models.CharField('Linea 2 del pie del ticket', max_length=80, blank=True, null=True)
	footer_line_3 = models.CharField('Linea 3 del pie del ticket', max_length=80, blank=True, null=True)
	class Meta:
		default_permissions = ()
		verbose_name = "Punto de venta"
		verbose_name_plural = "Puntos de venta"
		permissions = (
			('can_view_serial','Ve lista de series'),
			('can_edit_serial','Modifica series'),
			('can_create_serial','Crea series'),
			('can_delete_serial','Elimina series'),

			('can_view_sellpoint','Ve lista de puntos de venta'),
			('can_edit_sellpoint','Modifica puntos de venta'),
			('can_create_sellpoint','Crea puntos de venta'),
			('can_delete_sellpoint','Elimina puntos de venta'),

			('can_view_user','Ve lista de usuarios'),
			('can_edit_user','Modifica usuarios'),
			('can_create_user','Crea usuarios'),
			('can_delete_user','Elimina usuarios'),

			('admin_sellpoints','Administra puntos de venta'),
		)

	def __str__(self):
		return '{0} ({1} {2})'.format(self.name, self.company.name, self.company.id)

	def save(self, *args, **kwargs):
		self.name = self.name.upper()
		super().save()

	def add_text(self):
		return "NUEVO PUNTO DE VENTA","Agregar nuevo punto de venta","Crea un nuevo punto de venta en el sistema"

	def edit_text(self):
		return "EDITAR PUNTO DE VENTA","Editar este punto de venta","Edita los datos de este punto de venta en el sistema"

	def delete_text(self):
		return "ELIMINAR PUNTO DE VENTA","Elimina este punto de venta","Elimina este punto de venta del sistema","Se eliminará este punto de venta y todos sus datos asociados."

	def get_absolute_url_list(self):
		return reverse('sellpoint:sellpoint-list')

	def get_absolute_url_add(self):
		return reverse('sellpoint:sellpoint-add')

	def get_absolute_url_update(self):
		return reverse('sellpoint:sellpoint-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse('sellpoint:sellpoint-delete',kwargs={'pk': self.pk})

	def get_menus(self):
		menus = []
		products = Product_attributes.objects.filter(sellpoint = self, is_active=True, active=True, product__is_active=True, product__active=True, product__menu__active=True, product__menu__is_active=True)
		for product in products:
			for menu in product.product.menu.get_ancestors(ascending=False, include_self=True):
				menus.append(menu.id)
		return Menu.objects.filter(id__in=menus)

	def get_new_barcode(self, barcode):
		if self.serial:
			if self.serial.hexadecimal:
				return str(hex(self.serial.get_next())).rjust(12, '0')
			else:
				return str(self.serial.get_next()).rjust(12, '0')
		else:
			return barcode

	def assign_cut(self):
		cut = Cut.objects.filter(sellpoint = self, final_time__isnull = True)
		if cut:
			if not Ticket.objects.filter(cut=cut[0]):
				cut[0].init_time = datetime.datetime.now()
				cut[0].save()
			return cut[0]
		else:
			cut = Cut()
			cut.sellpoint = self
			cut.save()
			return cut



class Menu(MPTTModel):
	name = models.CharField('Nombre del menú', max_length=150)
	color = models.CharField('', help_text='Color del menú y los productos', default=VAR_LOCAL[0]['DEFAULT']['COLOR_DEFAULT'], max_length=100)
	company = models.ForeignKey('project.Company', on_delete=models.PROTECT)
	parent = TreeForeignKey('self', null=True, blank=True, related_name='+', db_index=True, on_delete=models.PROTECT, verbose_name='', help_text='Elige otro menú solo si este menú depende de otro')
	nivel = models.PositiveIntegerField(default=1)

	pull_cut = models.BooleanField('Separar del corte?', default=False, help_text='Se muestra en un corte separado?')

	is_active = models.BooleanField('Esta activo?', default=True, help_text='Desactiva este menú para desactivar toda su famila de productos')
	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = "Menú"
		verbose_name_plural = "Menús"
		permissions = (
			('can_view_menu','Ve lista de menus'),
			('can_edit_menu','Modifica menus'),
			('can_create_menu','Crea menus'),
			('can_delete_menu','Elimina menus'),
		)

	def __str__(self):
		return '{0}'.format(self.name,)

	def add_text(self):
		return "NUEVO MENÚ","Agregar nuevo menú","Crea un nuevo menú en el sistema"

	def edit_text(self):
		return "EDITAR MENÚ","Editar este menú","Edita los datos de este menú en el sistema"

	def delete_text(self):
		return "ELIMINAR MENÚ","Elimina este menú","Elimina este menú del sistema","Se eliminará este menú y todos sus datos asociados."

	def get_absolute_url_list(self):
		return reverse('sellpoint:menu-list')

	def get_absolute_url_add(self):
		return reverse('sellpoint:menu-add')

	def get_absolute_url_update(self):
		return reverse('sellpoint:menu-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse('sellpoint:menu-delete',kwargs={'pk': self.pk})


def path_product_image(self, filename):
	date = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
	upload_to = "companys/%s_%s/products/%s__%s" % (self.company.id, self.company.name, date, filename)
	return upload_to

class Product(models.Model):
	image = ProcessedImageField(blank=True, null=True, upload_to=path_product_image, processors=[ResizeToFill(400, 400)],format='JPEG',options={'quality': 40},)
	name = models.CharField('Nombre del producto', max_length=250)
	code = models.ForeignKey('project.Clave_Productos_Servicios', blank=True, null=True,on_delete=models.PROTECT, verbose_name="", help_text='Código de registro ante el SAT')
	units = models.ForeignKey('project.Clave_Unidades_Medida', blank=True, null=True,on_delete=models.PROTECT, verbose_name="", help_text="Unidad de medida para este producto")
	quantity = models.IntegerField(verbose_name="", help_text="Cantidad de producto que contiene", default=1)
	sellpoints = models.ManyToManyField('Sellpoint', blank=True, related_name='+', verbose_name="", help_text="Se vende en estas sucursales")
	menu = models.ForeignKey('Menu', on_delete=models.PROTECT, verbose_name="", help_text="Elige el menú donde se vende este producto")

	is_active = models.BooleanField('Esta activo?', default=True, help_text='Desactivar producto?')
	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = "Producto"
		verbose_name_plural = "Productos"
		permissions = (
			('can_view_product','Ve lista de productos'),
			('can_edit_product','Modifica productos'),
			('can_create_product','Crea productos'),
			('can_delete_product','Elimina productos'),
		)
		ordering = ['-id']

	def __str__(self):
		return '{0}'.format(self.name,)

	def add_text(self):
		return "NUEVO PRODUCTO","Agregar nuevo producto","Crea un nuevo producto en el sistema"

	def edit_text(self):
		return "EDITAR PRODUCTO","Editar este producto","Edita los datos de este producto en el sistema"

	def delete_text(self):
		return "ELIMINAR PRODUCTO","Elimina este producto","Elimina este producto del sistema","Se eliminará este producto y todos sus datos asociados."

	def get_absolute_url_list(self):
		return reverse('sellpoint:product-list')

	def get_absolute_url_add(self):
		return reverse('sellpoint:product-add')

	def get_absolute_url_detail(self):
		return reverse('sellpoint:product-detail',kwargs={'pk': self.pk})

	def get_absolute_url_update(self):
		return reverse('sellpoint:product-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse('sellpoint:product-delete',kwargs={'pk': self.pk})

	def print_my_sellpoints(self):
		p = ''
		for sellpoint in self.sellpoints.all():
			p += sellpoint.name + ', '
		return p[0:len(p)-2]

	def _company(self):
		return self.menu.company

	def make_menu(self):
		menu = ''
		for sellpoint in self.sellpoints.all():
			product_attribute = Product_attributes.objects.filter(sellpoint=sellpoint, product=self).first()
			menu += mark_safe(''+
				product_attribute.string_product_attributes()
			)
		return menu

class Product_attributes(models.Model):
    product = models.ForeignKey('Product', related_name='product', on_delete=models.CASCADE)
    sellpoint = models.ForeignKey('Sellpoint', related_name='sellpoint', on_delete=models.CASCADE)
    alias = models.CharField(max_length=250, blank=True, help_text="Nombre con el que se imprime este producto")
    inventory = models.IntegerField(default=0)
    price = models.FloatField('Precio en esta sucursal',default=0)
    iva = models.BooleanField('I.V.A.', default=False, help_text="Graba IVA?")
    ieps = models.BooleanField('IEPS.', default=False, help_text="Graba IEPS?")
    is_dynamic = models.BooleanField('Precio dinámico', default=False, help_text='Este producto tiene precio variable?')
    bar_code = models.CharField('Código de Barras', max_length=250, blank=True, null=True,)
    is_primary = models.BooleanField(default=False)
    is_favorite = models.BooleanField('Es favorito?', default=False, help_text='Se muestra siempre este producto?')

    is_active = models.BooleanField('Esta activo?', default=True, help_text='Desactivar producto?')
    active = models.BooleanField(default=True)

    class Meta:
        default_permissions = ()
        verbose_name = "Atributos de producto"
        verbose_name_plural = "Atributos de producto"
        permissions = (
            ('can_view_product_attributes','Ve lista de atributos de producto'),
            ('can_edit_product_attributes','Modifica atributos de producto'),
            ('can_create_product_attributes','Crea atributos de producto'),
            ('can_delete_product_attributes','Elimina atributos de producto'),
        )
        ordering = ['price']

    def __str__(self):
        return '{0}'.format(self.product.name,)

    def edit_text(self):
        return "EDITAR ATRIBUTOS","Editar estos atributos","Edita los atributos de este producto en el sistema"

    def get_absolute_url_list(self):
        return reverse('sellpoint:product_attributes-list',kwargs={'sellpoint': self.sellpoint.pk})

    def get_absolute_url_detail(self):
        return reverse('sellpoint:product_attributes-detail',kwargs={'pk': self.pk})

    def get_absolute_url_update(self):
        return reverse('sellpoint:product_attributes-update',kwargs={'pk': self.pk})

    @property
    def name(self):
        return self.product.name

    @property
    def product_pk(self):
        return self.product.id

    @property
    def menu_pk(self):
        return self.product.menu.id

    @property
    def units(self):
        return self.product.units.nombre

    @property
    def quantity(self):
        return self.product.quantity

    @property
    def image_product(self):
        return self.image.url

    @property
    def str_price(self):
        return '$ ' + str(Money(amount=self.price, currency='MXN')).split('$')[1]

    @property
    def color(self):
        return self.product.menu.color

    def return_icon(self, field):
        if field:
            return '<i class="fa fa-check"></i>'
        else:
            return '<i class="fa fa-times"></i>'

    def return_barcode(self):
        if self.bar_code:
            return self.bar_code
        else:
            return 'N/A'

    def string_product_attributes(self):
        try:
            return """
                <a href="{0}" class="color-default"><i class="fas fa-edit color-5"></i> ${1:.2f} - {3}
                <small mt-0>
                {4} {2} - {5} <br />
                IVA: {6} IEPS: {7} DINAMICO: {8} FAVORITO: {9} <br />
                <i class="fas fa-barcode"></i> {10} <i class="fas fa-shopping-basket"></i> {11}
                </small>
                </a><br />""".format(
                    self.get_absolute_url_update(),
                    self.price,
                    self.alias,
                    self.sellpoint.name,
                    self.return_icon(self.is_active),
                    self.product.name,
                    self.return_icon(self.iva),
                    self.return_icon(self.ieps),
                    self.return_icon(self.is_dynamic),
                    self.return_icon(self.is_favorite),
                    self.return_barcode(),
                    str(self.inventory),
                )
        except Exception as e:
            return str(e)
            
def products_changed(sender, instance, action, **kwargs):
    if action == 'post_add':
        product_attributes = Product_attributes.objects.filter(product = instance)
        for product in product_attributes:
            if not product.sellpoint in instance.sellpoints.all():
                producto.active = False
                producto.save()
        for sellpoint in instance.sellpoints.all():
            if not Product_attributes.objects.filter(product = instance, sellpoint = sellpoint):
                product_attributes = Product_attributes()
                product_attributes.product = instance
                product_attributes.sellpoint = sellpoint
                product_attributes.alias = instance.name
                product_attributes.save()
m2m_changed.connect(products_changed, sender=Product.sellpoints.through)

class Ticket(models.Model):

	STATUS_TICKET = (
		('COB','COBRADO'),
		('PEN','PENDIENTE'),
		('CAN','CANCELADO'),
		('RAS','RASURADO'),
		('FAC','FACTURADO'),
		('OCU','OCULTO'),
	)

	FILTER_STATUS_TICKET = (
		('COBRADO'),
		('PENDIENTE'),
		('CANCELADO'),
		('FACTURADO'),
	)

	barcode = models.CharField(max_length=13, default="0000000000000")
	sellpoint = models.ForeignKey('Sellpoint', null=True, blank=True, on_delete=models.SET_NULL)
	user = models.ForeignKey('project.User', null=True, blank=True, on_delete=models.SET_NULL)
	user_string = models.CharField(max_length=250, null=True, blank=True)
	status = models.CharField(choices=STATUS_TICKET, max_length=50, default="Pendiente")
	date = models.DateTimeField(auto_now_add=True)
	cut = models.ForeignKey('Cut', null=True, blank=True, on_delete=models.SET_NULL)
	total = models.FloatField(default=0)
	iva = models.FloatField(default=0)
	ieps = models.FloatField(default=0)
	secret_code = models.CharField(max_length=12)
	invoice = models.BooleanField(default=False)
	format_time = models.CharField(max_length=50, null=True, blank=True)
	format_date = models.CharField(max_length=50, null=True, blank=True)
	live = models.BooleanField(default=True)
	userid = models.CharField(max_length=8, null=True, blank=True)
	sellpointid = models.CharField(max_length=8, null=True, blank=True)
	hide_ras = models.BooleanField(default=False)
	
	is_order = models.BooleanField(default=False)
	order_name = models.CharField('Nombre del cliente',max_length=250, blank=True, null=True)
	order_notes = models.CharField('Notas',max_length=500, blank=True, null=True)
	order_phone = models.CharField('Telefono del cliente',max_length=250, blank=True, null=True)
	order_datetime = models.DateTimeField('Fecha', blank=True, null=True, default=datetime.datetime(2018, 12, 24, 14, 0, 0, 477671))
	order_tocount = models.FloatField('Cantidad que deja a cuenta',blank=True, null=True)

	class Meta:
		default_permissions = ()
		verbose_name = "Ticket"
		verbose_name_plural = "Tickets"
		permissions = (
			('can_view_ticket','Ve lista de tickets'),
			('can_edit_ticket','Modifica tickets'),
			('can_create_ticket','Crea tickets'),
			('can_delete_ticket','Elimina tickets'),
			('admin_ticket','Administra tickets'),
		)
		ordering = ['-id']

	def __str__(self):
		return '{0}'.format(self.barcode,)

	def get_absolute_url_list(self):
		return reverse('sellpoint:ticket-list')

	def get_absolute_url_detail(self):
		return reverse('sellpoint:ticket-detail',kwargs={'pk': self.pk})

	def make_cfdi(self, *args, **kwargs):

		email = kwargs.pop('email')
		company = self.sellpoint.company

		fecha = str(datetime.datetime.now().isoformat())[:19]
		serie = 'TICKETS'
		folio = self.barcode
		nocertificado = self.sellpoint.rfc_tickets.nocertificado
		subtotal = '{0:.2f}'.format(self.total - self.iva - self.ieps)
		moneda = 'MXN'
		total = '{0:.2f}'.format(self.total)
		formapago = '01'
		tipodecomprobante = 'I'
		condicionesdepago = 'CONDICIONES'
		metodopago = 'PUE'
		tipocambio = '1'
		lugarexpedicion = self.sellpoint.rfc_tickets.codigo_postal

		emisor_rfc =  self.sellpoint.rfc_tickets.rfc
		emisor_nombre = self.sellpoint.rfc_tickets.razon_social
		emisor_regimenfiscal = '601'

		emisor_calle = self.sellpoint.rfc_tickets.calle
		emisor_numero_exterior = self.sellpoint.rfc_tickets.numero_exterior
		emisor_numero_interior = self.sellpoint.rfc_tickets.numero_interior
		emisor_colonia = self.sellpoint.rfc_tickets.colonia
		emisor_localidad = self.sellpoint.rfc_tickets.localidad
		emisor_municipio_delegacion = self.sellpoint.rfc_tickets.municipio_delegacion
		emisor_estado = self.sellpoint.rfc_tickets.estado
		emisor_codigo_postal = self.sellpoint.rfc_tickets.codigo_postal
		emisor_pais = self.sellpoint.rfc_tickets.pais

		receptor_rfc = kwargs.pop('rfc').upper()
		receptor_nombre = kwargs.pop('name')
		receptor_usocfdi = 'G03'

		footer = 'PASTELERIAS LA ESTRELLA :: NUESTRO SECRETO ES LA CALIDAD'

		conceptos = []
		for concepto in self.get_ticket_lines():
			importe = concepto.total
			if concepto.iva:
				importe = importe - concepto.iva
			if concepto.ieps:
				importe = importe - concepto.ieps
			try:
				claveprodserv = concepto.product.product.code.codigo
			except:
				claveprodserv = '50181900'
			try:
				claveunidad = concepto.product.product.units.codigo
				unitsnombre = concepto.product.product.units.nombre
			except:
				claveunidad = 'H87'
				unitsnombre = 'Pieza'
			c = {
				'claveprodserv':claveprodserv,
				'claveunidad':claveunidad,
				'cantidad':str(concepto.quantity),
				'unidad': unitsnombre,
				'noidentificacion':'01',
				'descripcion':concepto.product.alias,
				'valorunitario':'{0:.2f}'.format(importe/concepto.quantity),
				'importe':'{0:.2f}'.format(importe),
				'impuestos_transladados':[],
				'descuento':'0.00'
			}
			if concepto.iva:
				c['impuestos_transladados'].append({
					'base':'{0:.2f}'.format(importe),
					'impuesto':'002',
					'tipofactor':'Tasa',
					'tasaocuota':'0.160000',
					'importe':'{0:.2f}'.format(concepto.iva),
				})
			if concepto.ieps:
				c['impuestos_transladados'].append({
					'base':'{0:.2f}'.format(importe),
					'impuesto':'003',
					'tipofactor':'Tasa',
					'tasaocuota':'0.080000',
					'importe':'{0:.2f}'.format(concepto.ieps),
				})
			conceptos.append(c)
		return Invoice().make_cfdi( locals() )

	@property
	def visible_username(self):
		return self.user.visible_username

	@property
	def _company(self):
		return self.sellpoint.company.name

	def get_status(self, user, sellpoint):
		if sellpoint.auto_cash:
			return 'COB'
		else:
			return 'PEN'

	def update_ticket(self, ticket, sellpoint, user):
		_ticket = Ticket.objects.filter(secret_code = ticket['secret_code'], format_date = ticket['format_date'], format_time = ticket['format_time']).first()
		if not _ticket:
			_ticket = Ticket().new_ticket(ticket, user, sellpoint, False)
		return _ticket

	def new_ticket(self, ticket, user, sellpoint, live):
		if live:
			self.barcode = sellpoint.get_new_barcode(ticket['barcode'])
		else:
			self.barcode = ticket['barcode']
		try:
			if ticket['order_name']:
				self.is_order=True
				self.order_name= ticket['order_name']
				self.order_notes= ticket['order_notes']
				self.order_phone= ticket['order_phone']
				self.order_datetime = datetime.datetime.strptime(ticket['order_datetime'], '%d-%m-%Y %H:%M')
				self.order_tocount = ticket['order_tocount']
		except:
			pass
		self.sellpoint = sellpoint
		self.user = user
		self.user_string = ticket['user_string']
		self.status = ticket['status']
		self.cut = sellpoint.assign_cut()
		self.secret_code = ticket['secret_code']
		self.total = ticket['total']
		self.iva = ticket['iva']
		self.ieps = ticket['ieps']
		self.format_time = ticket['format_time']
		self.format_date = ticket['format_date']
		self.userid = ticket['userid']
		self.sellpointid = ticket['sellpointid']
		self.live = live
		self.save()
		self.assign_products(ticket, self)
		return self

	def assign_products(self, ticket, t):
		for line in ticket['ticket_lines']:
			ticket_line = Ticket_lines()
			ticket_line.ticket = t
			ticket_line.product = Product_attributes.objects.get(id=line['product']['id'])
			ticket_line.text_product = line['text_product']
			ticket_line.alias = line['product']['alias']
			ticket_line.quantity = line['quantity']
			ticket_line.price = line['price']
			ticket_line.total = line['total']
			ticket_line.iva = line['iva']
			ticket_line.ieps = line['ieps']
			ticket_line.save()
		return True

	def get_ticket_lines(self):
		return Ticket_lines.objects.filter(ticket=self)

class Ticket_lines(models.Model):
	ticket = models.ForeignKey('Ticket', on_delete=models.CASCADE)
	product = models.ForeignKey('Product_attributes', null=True, on_delete=models.SET_NULL)
	text_product = models.CharField(max_length=250)
	alias = models.CharField(max_length=250)
	quantity = models.FloatField()
	price = models.FloatField()
	total = models.FloatField()
	iva = models.FloatField()
	ieps = models.FloatField()

	class Meta:
		default_permissions = ()

class Cut(models.Model):
	sellpoint = models.ForeignKey('Sellpoint', null=True, blank=True, on_delete=models.SET_NULL)
	user = models.ForeignKey('project.User', null=True, blank=True, on_delete=models.SET_NULL)
	init_time = models.DateTimeField(auto_now_add=True)
	final_time = models.DateTimeField(null=True, blank=True,)
	invoice = models.ForeignKey('invoice.Invoice', null=True, blank=True, on_delete=models.SET_NULL)
	ras = models.PositiveIntegerField(default=100)
	serial = models.IntegerField(default=1, null=True, blank=True)
	show = models.BooleanField(default=True)
	ocult = models.BooleanField(default=False)
	class Meta:
		default_permissions = ()
		verbose_name = "Corte"
		verbose_name_plural = "Cortes"
		permissions = (
			('can_view_cut','Ve lista de cortes'),
			('can_edit_cut','Modifica cortes'),
			('can_create_cut','Crea cortes'),
			('can_delete_cut','Elimina cortes'),
			('admin_cut','Administra cortes'),
		)
		ordering = ['-id']

	def __str__(self):
		return '{0}'.format(str(self.pk).rjust(6, '0'),)

	def get_absolute_url_list(self):
		try:
			return reverse('sellpoint:cut-list')
		except:
			return reverse('project:dashboard')

	def get_absolute_url_detail(self):
		return reverse('sellpoint:cut-detail',kwargs={'pk': self.pk})

	def have_actions(self):
		return True
	
	def len_cuts(self):
		return len(Cut.objects.filter(sellpoint=self.sellpoint))

	def make_cut_cfdi(self):
		serie = 'PV'

		fecha = str(datetime.datetime.now().isoformat())[:19]
		nocertificado = self.sellpoint.rfc_cuts.nocertificado
		subtotal = '{0:.2f}'.format(self.total - self.iva - self.ieps)
		moneda = 'MXN'
		total = '{0:.2f}'.format(self.total)
		formapago = '01'
		tipodecomprobante = 'I'
		condicionesdepago = 'CONDICIONES'
		metodopago = 'PUE'
		tipocambio = '1'
		lugarexpedicion = self.sellpoint.rfc_cuts.codigo_postal

		emisor_rfc =  self.sellpoint.rfc_cuts.rfc
		emisor_nombre = self.sellpoint.rfc_cuts.razon_social
		emisor_regimenfiscal = '601'

		receptor_rfc = 'XAXX010101000'
		receptor_nombre = 'Ventas a Publico en General'
		receptor_usocfdi = 'P01'

		footer = 'PASTELERIAS LA ESTRELLA :: NUESTRO SECRETO ES LA CALIDAD'

		conceptos = []
		for k, concepto in self.cut_lines.items():
			importe = concepto.total
			if concepto.iva:
				importe = importe - concepto.iva
			if concepto.ieps:
				importe = importe - concepto.ieps
			c = {
				'claveprodserv':concepto.product.product.code.codigo,
				'claveunidad':concepto.product.product.units.codigo,
				'cantidad':str(concepto.quantity),
				'unidad':concepto.product.product.units.nombre,
				'noidentificacion':'01',
				'descripcion':concepto.product.alias,
				'valorunitario':'{0:.2f}'.format(importe/concepto.quantity),
				'importe':'{0:.2f}'.format(importe),
				'impuestos_transladados':[],
				'descuento':'0.00'
			}
			if concepto.iva:
				c['impuestos_transladados'].append({
					'base':'{0:.2f}'.format(importe),
					'impuesto':'002',
					'tipofactor':'Tasa',
					'tasaocuota':'0.160000',
					'importe':'{0:.2f}'.format(concepto.iva),
				})
			if concepto.ieps:
				c['impuestos_transladados'].append({
					'base':'{0:.2f}'.format(importe),
					'impuesto':'003',
					'tipofactor':'Tasa',
					'tasaocuota':'0.080000',
					'importe':'{0:.2f}'.format(concepto.ieps),
				})
			conceptos.append(c)
		return Invoice().make_cfdi( locals() )

	@property
	def total(self):
		total = 0
		for ticket in Ticket.objects.filter(cut=self, status='COB', hide_ras=False):
			total += ticket.total
		return total
	
	@property
	def totalfaltante(self):
		total = 0
		for ticket in Ticket.objects.filter(cut=self, hide_ras=False, status='PEN'):
			total += ticket.total
		return total

	@property
	def iva(self):
		iva = 0
		for ticket in Ticket.objects.filter(cut=self, status='COB', hide_ras=False):
			iva += ticket.iva
		return iva

	@property
	def ieps(self):
		ieps = 0
		for ticket in Ticket.objects.filter(cut=self, status='COB', hide_ras=False):
			ieps += ticket.ieps
		return ieps

	@property
	def code(self):
		return str(self.pk).rjust(8, '0')

	@property
	def customers(self):
		return len(Ticket.objects.filter(cut=self))

	@property
	def pen(self):
		return len(Ticket.objects.filter(cut=self, status='PEN', hide_ras=False))
	
	@property
	def pen_total(self):
		total = 0
		for ticket in Ticket.objects.filter(cut=self, status='PEN', hide_ras=False):
			total += ticket.total
		return total

	@property
	def cob(self):
		return len(Ticket.objects.filter(cut=self, status='COB', hide_ras=False))

	@property
	def can(self):
		return len(Ticket.objects.filter(cut=self, status='CAN', hide_ras=False))

	@property
	def fac(self):
		return len(Ticket.objects.filter(cut=self, status='FAC', hide_ras=False))

	@property
	def inv(self):
		return len(Ticket.objects.filter(cut=self, invoice=True, hide_ras=False))

	@property
	def format_init_time(self):
		return timezone.localtime(self.init_time).strftime("%d/%m/%y %I:%M:%S %p")

	@property
	def format_final_time(self):
		if self.final_time:
			return timezone.localtime(self.final_time).strftime("%d/%m/%y %I:%M:%S %p")
		else:
			return '-'

	@property
	def cut_lines_without_pulls(self):
		lines = {}
		total, iva, ieps = 0, 0, 0
		ticket_lines = Ticket_lines.objects.filter(ticket__cut = self, ticket__status = 'COB', product__product__menu__pull_cut=False, ticket__hide_ras=False).order_by('product__product__menu__name','product__product__name','price')
		for ticket_line in ticket_lines:
			iva += ticket_line.iva
			ieps += ticket_line.ieps
			total += ticket_line.total
			if not (ticket_line.text_product + str(ticket_line.price)) in lines:
				lines.update({(ticket_line.text_product + str(ticket_line.price)):ticket_line})
			else:
				lines[(ticket_line.text_product + str(ticket_line.price))].quantity += ticket_line.quantity
				lines[(ticket_line.text_product + str(ticket_line.price))].iva += ticket_line.iva
				lines[(ticket_line.text_product + str(ticket_line.price))].ieps += ticket_line.ieps
				lines[(ticket_line.text_product + str(ticket_line.price))].total += ticket_line.total
		return {
			'lines':lines,
			'total':total,
			'iva':iva,
			'ieps':ieps,
			'subtotal':total - iva - ieps,
		}
	
	def cut_lines_without_pulls_json(self):
		lines = {}
		devoluciones = 0
		num_devoluciones = 0
		pedidos = 0
		num_pedidos = 0
		gastos = 0
		num_gastos = 0
		descuentos = 0
		num_descuentos = 0
		total, iva, ieps = 0, 0, 0
		subtotal = 0
		if self.sellpoint.pk == 22:
			ticket_lines = Ticket_lines.objects.filter(ticket__cut = self).distinct().order_by('product__product__name','price')
			for ticket_line in ticket_lines:
				iva += ticket_line.iva
				ieps += ticket_line.ieps
				total += ticket_line.total
				if 'GASTO' in ticket_line.text_product:
					gastos += ticket_line.total
					num_gastos += 1
				if 'PEDIDO' in ticket_line.text_product:
					pedidos += ticket_line.total
					num_pedidos += 1
				if '%' in ticket_line.text_product:
					num_descuentos += 1
					if re.findall('-[0-9]+%', ticket_line.text_product):
						#disccount = (float(re.findall(r'\d+', re.findall(r'-[0-9]+%', ticket_line.text_product)[0])[0])/100) + 1
						#descuentos += (ticket_line.total * disccount) - ticket_line.total
						#subtotal += (ticket_line.total * disccount)
						descuentos += ticket_line.total
				if not 'PEDIDO' in ticket_line.text_product and not 'GASTO' in ticket_line.text_product:
					subtotal += ticket_line.total
				text = re.sub('-[0-9]+%', '', ticket_line.text_product)
				if not (text) in lines:
					lines.update({(text):ticket_line})
				else:
					lines[(text)].quantity += ticket_line.quantity
					lines[(text)].iva += ticket_line.iva
					lines[(text)].ieps += ticket_line.ieps
					lines[(text)].total += ticket_line.total
			line_t = ''
			for key, value in lines.items():
				if value.quantity.is_integer():	
					line_t += str(key) + '\n' + (str(int(value.quantity)) + ' ').ljust(15) +'$' + str(format(value.total, ',.2f')) + '\n'
				else:
					line_t += str(key) + '\n' + (str(value.quantity) + ' ').ljust(15) +'$' + str(format(value.total, ',.2f')) + '\n'
			line_t += "-------------- \n"
			line_t += "SUBTOTAL             $" + str(format(subtotal, ',.2f')) + "\n"
			line_t += "-------------- \n"
			line_t += "GASTOS (" + str(int(num_gastos)) + ")\n"
			line_t += "TOTAL GASTOS         $" + str(format(gastos,',.2f')) + "\n"
			line_t += "PEDIDOS (" + str(int(num_pedidos)) + ")\n"
			line_t += "TOTAL PEDIDOS        $" + str(format(pedidos,',.2f')) + "\n"
			line_t += "DESCUENTOS (" + str(int(num_descuentos)) + ")\n"
			line_t += "TOTAL DESCUENTOS     $" + str(format(descuentos,',.2f')) + "\n"
			line_t += "-------------- \n"
			line_t += "TOTAL SIN IMPUESTOS  $" + str(total) + "\n"
			return line_t
		else:
			ticket_lines = Ticket_lines.objects.filter(ticket__cut = self, ticket__status = 'COB', ticket__hide_ras=False).order_by('product__product__menu__name','product__product__name','price')
			for ticket_line in ticket_lines:
				iva += ticket_line.iva
				ieps += ticket_line.ieps
				total += ticket_line.total
				if ticket_line.text_product == 'GASTO':
					gastos += ticket_line.total
					num_gastos += 1
				if ticket_line.text_product == 'PEDIDO':
					pedidos += ticket_line.total
					num_pedidos += 1
				if not (ticket_line.text_product + str(ticket_line.price)) in lines:
					lines.update({(ticket_line.text_product + str(ticket_line.price)):ticket_line})
				else:
					lines[(ticket_line.text_product + str(ticket_line.price))].quantity += ticket_line.quantity
					lines[(ticket_line.text_product + str(ticket_line.price))].iva += ticket_line.iva
					lines[(ticket_line.text_product + str(ticket_line.price))].ieps += ticket_line.ieps
					lines[(ticket_line.text_product + str(ticket_line.price))].total += ticket_line.total
				if '%' in ticket_line.text_product:
					descuentos += ticket_line.total
					num_descuentos += 1
			line_t = ''
			for key, value in lines.items():
				line_t += str(value.quantity) + ' ' + str(key) + ' ' + str(format(value.total, ',.2f')) + '\n'
			line_t += "-------------- \n"
			line_t += "# GASTOS       $" + str(format(gastos, ',.2f')) + "\n"
			line_t += "TOTAL GASTOS       " + str(num_gastos) + "\n"
			line_t += "# PEDIDOS       $" + str(format(pedidos, ',.2f')) + "\n"
			line_t += "TOTAL PEDIDOS       " + str(num_pedidos) + "\n"
			line_t += "# DESCUENTOS       $" + str(format(descuentos, ',.2f')) + "\n"
			line_t += "TOTAL DESCUENTOS       " + str(num_descuentos) + "\n"
			return line_t

	@property
	def cut_lines_with_pulls(self):
		lines = {}
		total, iva, ieps = 0, 0, 0
		menus_with_pulls = []
		ticket_lines = Ticket_lines.objects.filter(ticket__cut = self, ticket__status = 'COB', product__product__menu__pull_cut=True, ticket__hide_ras=False).order_by('product__product__menu__name','product__product__name','price')
		for ticket_line in ticket_lines:
			iva += ticket_line.iva
			ieps += ticket_line.ieps
			total += ticket_line.total
			if not (ticket_line.text_product + str(ticket_line.price)) in lines:
				lines.update({(ticket_line.text_product + str(ticket_line.price)):ticket_line})
			else:
				lines[(ticket_line.text_product + str(ticket_line.price))].quantity += ticket_line.quantity
				lines[(ticket_line.text_product + str(ticket_line.price))].iva += ticket_line.iva
				lines[(ticket_line.text_product + str(ticket_line.price))].ieps += ticket_line.ieps
				lines[(ticket_line.text_product + str(ticket_line.price))].total += ticket_line.total
		return {
			'lines':lines,
			'total':total,
			'iva':iva,
			'ieps':ieps,
			'subtotal':total - iva - ieps,
			'menus_with_pulls':menus_with_pulls
		}

	def cut_lines_ras(self):
		lines = {}
		total, iva, ieps = 0, 0, 0
		menus_with_pulls = []
		tickets = Ticket.objects.filter(cut = self, status = 'COB')
		tickets.update(hide_ras=False)
		total = 0
		for ticket in tickets:
			total += ticket.total
		rastotal = total * self.ras / 100
		rastickets = []
		total = 0
		if Ticket.objects.filter(cut=self, status='COB', total__gt=2000):
			tickets = Ticket.objects.filter(cut = self, status = 'COB').order_by('-total')
		else:
			tickets = Ticket.objects.filter(cut = self, status = 'COB').order_by('total')
		for ticket in tickets:
			total += ticket.total
			rastickets.append(ticket.id)
			if total > rastotal:
				break
		tickets.exclude(pk__in = rastickets).update(hide_ras=True)
		rastickets = Ticket.objects.filter(pk__in = rastickets)
		ticket_lines = Ticket_lines.objects.filter(ticket__in = rastickets.all())
		lines = {}
		total, iva, ieps, blanco, blanco_pz, dulce, dulce_pz, otros, otros_pz = 0, 0, 0, 0, 0, 0, 0,0,0
		for ticket_line in ticket_lines:
			if ticket_line.iva:
				otros += ticket_line.total
				otros_pz += ticket_line.quantity
			elif ticket_line.ieps:
				dulce += ticket_line.total
				dulce_pz += ticket_line.quantity
			else:
				blanco += ticket_line.total
				blanco_pz += ticket_line.quantity
			iva += ticket_line.iva
			ieps += ticket_line.ieps
			total += ticket_line.total
			if not (ticket_line.text_product + str(ticket_line.price)) in lines:
				lines.update({(ticket_line.text_product + str(ticket_line.price)):ticket_line})
			else:
				lines[(ticket_line.text_product + str(ticket_line.price))].quantity += ticket_line.quantity
				lines[(ticket_line.text_product + str(ticket_line.price))].iva += ticket_line.iva
				lines[(ticket_line.text_product + str(ticket_line.price))].ieps += ticket_line.ieps
				lines[(ticket_line.text_product + str(ticket_line.price))].total += ticket_line.total
		return {
			'lines':lines,
			'total':total,
			'iva':iva,
			'ieps':ieps,
			'subtotal':total - iva - ieps,
			'blanco':blanco,
			'blanco_pz': blanco_pz,
			'dulce': dulce,
            'dulce_pz': dulce_pz,
			'otros': otros,
			'otros_pz': otros_pz,
		}

	@property
	def _company(self):
		try:
			return self.sellpoint.company
		except:
			return '---'
