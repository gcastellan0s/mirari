from rest_framework import serializers
from .models import *

class Product_attributesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Product_attributes
		fields = ('id', 'product_pk', 'name', 'str_price', 'units', 'quantity', 'alias', 'inventory', 'price', 'iva', 'ieps', 'is_dynamic', 'menu_pk', 'color', 'is_favorite')

class TicketSerializer(serializers.ModelSerializer):
	class Meta:
		model = Ticket
		fields = ('pk','barcode','status','cut','total','iva','ieps','secret_code','visible_username', 'date', 'format_date', 'format_time', 'sellpoint', 'invoice', 'user_string', 'sellpointid', 'userid', 'live', 'is_order','order_name','order_notes','order_phone','order_datetime','order_tocount',)

class Ticket_linesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Ticket_lines
		fields = ('text_product','quantity','price','total','iva','ieps')

class SellpointSerializer(serializers.ModelSerializer):
	class Meta:
		model = Sellpoint
		fields = ('name','header_line_black_1','header_line_black_2','header_line_black_3','header_line_1','header_line_2','header_line_3','header_line_4','header_line_5','footer_line_1','footer_line_2','footer_line_3','auto_cash','number_tickets')

class CutsSerializer(serializers.ModelSerializer):
	class Meta:
		model = Cut
		fields = ('pk','code','format_init_time','format_final_time','total','pen','cob')