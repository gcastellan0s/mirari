# -*- encoding: utf-8 -*-
from django import forms
from .models import *
from django.forms import widgets
from project.forms import MaterialCheckboxWidget, MaterialDateWidget, MaterialTextWidget, SelectWidget, SelectMultipleWidget, MaterialColorWidget

class SellpointForm(forms.ModelForm):
	class Meta:
		model = Sellpoint
		fields = ('name','alias','serial','is_active','auto_cash','have_orders','color','invoice_in_point','sellpoint_type','cut_in_point','number_tickets','rfc_cuts','rfc_tickets','header_line_black_1','header_line_black_2','header_line_black_3','header_line_1','header_line_2','header_line_3','header_line_4','header_line_5','footer_line_1','footer_line_2','footer_line_3',)
		widgets={
			'is_active':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'auto_cash':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'have_orders': MaterialCheckboxWidget(attrs={'class': 'toggle-switch__checkbox', 'style': 'display:none;'}),
			'invoice_in_point':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'cut_in_point':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'sellpoint_type': SelectWidget(attrs={'class': 'select2', }),
			'serial':SelectWidget(attrs={'class':'select2',}),
			'rfc_cuts':SelectWidget(attrs={'class':'select2',}),
			'rfc_tickets':SelectWidget(attrs={'class':'select2',}),
		}
	def __init__(self, *args, **kwargs):
		serials = kwargs.pop('serials')
		fiscaldata = kwargs.pop('fiscaldata')
		super(SellpointForm, self).__init__(*args, **kwargs)
		self.fields['serial'].queryset = serials
		if fiscaldata:
			self.fields['rfc_cuts'].queryset = fiscaldata
			self.fields['rfc_tickets'].queryset = fiscaldata
		else:
			self.fields.pop('rfc_cuts')
			self.fields.pop('rfc_tickets')

class MenuForm(forms.ModelForm):
	class Meta:
		model = Menu
		fields = ('name', 'parent', 'color', 'pull_cut', 'is_active')
		widgets={
			'parent':SelectWidget(attrs={'class':'select2',}),
			'color':MaterialColorWidget(attrs={'class':'form-control colorPicker', 'v-model':'vcolor'}),
			'pull_cut':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'is_active':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
		}

	def __init__(self, *args, **kwargs):
		parents = kwargs.pop('parents')
		super(MenuForm, self).__init__(*args, **kwargs)
		if parents:
			self.fields['parent'].queryset = parents
		else:
			self.fields.pop('parent')

class ProductForm(forms.ModelForm):
	class Meta:
		model = Product
		fields = ('name','code','units','quantity','sellpoints','menu','is_active')
		widgets={
			'menu':SelectWidget(attrs={'class':'select2',}),
			'sellpoints':SelectMultipleWidget(attrs={'class':'select2', 'multiple':'multiple',}),
			'is_active':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
		}

	def __init__(self, *args, **kwargs):
		sellpoints = kwargs.pop('sellpoints')
		menus = kwargs.pop('menus')
		super(ProductForm, self).__init__(*args, **kwargs)
		self.fields['sellpoints'].queryset = sellpoints
		self.fields['menu'].queryset = menus

class Product_attributesForm(forms.ModelForm):
	class Meta:
		model = Product_attributes
		fields = ('alias','price','bar_code','iva','ieps','is_dynamic','is_active','is_favorite')
		widgets={
			'iva':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'ieps':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'is_dynamic':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'is_active':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'is_favorite':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
		}

class TicketForm(forms.ModelForm):
	class Meta:
		model = Ticket
		fields = ('order_name','order_phone','order_tocount','order_notes')
		widgets={
			'order_name':MaterialTextWidget(attrs={'class':'form-control', 'required':'required', 'v-model':'ticket.order_name'}),
			'order_phone':MaterialTextWidget(attrs={'class':'form-control', 'required':'required', 'v-model':'ticket.order_phone'}),
			'order_tocount':MaterialTextWidget(attrs={'class':'form-control', 'required':'required', 'v-model':'ticket.order_tocount'}),
			'order_notes':MaterialTextWidget(attrs={'class':'form-control', 'v-model':'ticket.order_notes'}),
		}

