# Generated by Django 2.0.1 on 2018-01-19 06:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sellpoint', '0009_product_attributes_active'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product_attributes',
            options={'verbose_name': 'Atributos de producto', 'verbose_name_plural': 'Atributos de producto'},
        ),
        migrations.AlterField(
            model_name='product_attributes',
            name='alias',
            field=models.CharField(blank=True, help_text='Nombre con el que se imprime este producto', max_length=250),
        ),
        migrations.AlterField(
            model_name='product_attributes',
            name='bar_code',
            field=models.CharField(blank=True, max_length=250, null=True, verbose_name='Código de Barras'),
        ),
        migrations.AlterField(
            model_name='product_attributes',
            name='ieps',
            field=models.BooleanField(default=False, help_text='Graba IEPS?', verbose_name='IEPS.'),
        ),
        migrations.AlterField(
            model_name='product_attributes',
            name='is_dynamic',
            field=models.BooleanField(default=False, help_text='Este producto tiene precio variable?', verbose_name='Precio dinámico'),
        ),
        migrations.AlterField(
            model_name='product_attributes',
            name='iva',
            field=models.BooleanField(default=False, help_text='Graba IVA?', verbose_name='I.V.A.'),
        ),
        migrations.AlterField(
            model_name='product_attributes',
            name='price',
            field=models.FloatField(default=0, verbose_name='Precio en esta sucursal'),
        ),
    ]
