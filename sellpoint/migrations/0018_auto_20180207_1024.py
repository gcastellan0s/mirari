# Generated by Django 2.0.1 on 2018-02-07 16:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sellpoint', '0017_auto_20180131_1359'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sellpoint',
            old_name='auto_chash',
            new_name='auto_cash',
        ),
        migrations.AlterField(
            model_name='menu',
            name='color',
            field=models.CharField(default='#2b2b2b', help_text='Color del menú y los productos', max_length=100, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='status',
            field=models.CharField(choices=[('COB', 'COBRADO'), ('PEN', 'PENDIENTE'), ('CAN', 'CANCELADO'), ('RAS', 'RASURADO'), ('FAC', 'FACTURADO'), ('CAN', 'CANCELADO'), ('OCU', 'OCULTO')], default='Pendiente', max_length=50),
        ),
    ]
