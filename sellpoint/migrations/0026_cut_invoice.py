# Generated by Django 2.0.1 on 2018-03-09 04:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0004_invoice'),
        ('sellpoint', '0025_auto_20180307_1951'),
    ]

    operations = [
        migrations.AddField(
            model_name='cut',
            name='invoice',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='invoice.Invoice'),
        ),
    ]
