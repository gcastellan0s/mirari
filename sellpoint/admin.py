from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from mptt.admin import MPTTModelAdmin
from .models import *

class PassAdmin(admin.ModelAdmin):
    pass

#admin.site.register(Ticket_lines, PassAdmin)

@admin.register(Sellpoint)
class SellpointAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    search_fields = ('name',)
    list_filter = ('company',)

@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('barcode','sellpoint','user','status','date','cut','total','_company')
    search_fields = ('barcode',)
    list_filter = ('sellpoint','status')

@admin.register(Cut)
class CutAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('sellpoint','code','total','init_time','final_time', '_company', 'ocult')
    #search_fields = ('code',)  
    list_filter = ('sellpoint__company',)

@admin.register(Menu)
class MenuAdmin(MPTTModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('name','company','is_active',)
    search_fields = ('name',)
    list_filter = ('company',)

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('name','menu','_company')
    search_fields = ('name',)

@admin.register(Product_attributes)
class Product_attributesAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('product','product','price')
    search_fields = ('name',)


@admin.register(Ticket_lines)
class Ticket_linesAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('ticket', 'total')
    fields = ('text_product','alias','quantity','price','total','iva','ieps',)
    search_fields = ('ticket__barcode',)