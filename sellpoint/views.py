from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import SingleObjectMixin, DetailView
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.db.models import Q
from django.urls import reverse
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.utils.crypto import get_random_string
from django.contrib import messages
from .models import *
from .forms import *
from .serializers import *
from project.serializers import *
from project.models import *
from project.views import log_variables, variables, check_permissions
import json
import sys
from django.core.exceptions import PermissionDenied
from invoice.models import FiscalData
from django.contrib.auth.models import AbstractUser, Permission, Group
from invoice.models import Invoice
import xmltodict

def router_sellpoint(request, company):
    if not request.user.sellpoints.all():
        sellpoints = company.mysellpoints()
        if not sellpoints:
            raise Http404
    else:
        sellpoints = request.user.sellpoints.all()
    if not sellpoints:
        raise Http404
    if request.GET.get('sellpoint'):
        sellpoint = get_object_or_404(Sellpoint, pk=request.GET.get('sellpoint'), company=company)
        if sellpoint in sellpoints:
            request.session['sellpoint'] = sellpoint.id
            return sellpoint
    if 'sellpoint' in request.session:
        sellpoint = Sellpoint.objects.filter(pk=request.session["sellpoint"], company=company).first()
        if sellpoint:
            if sellpoint in sellpoints:
                return sellpoint
    request.session['sellpoint'] = sellpoints[0].id
    return sellpoints[0]

def get_printer(request):
    if request.GET.get('sellpoint_printer'):
        request.session['sellpoint_printer'] = request.GET.get('sellpoint_printer')
    elif request.session.get('sellpoint_printer'):
        sellpoint_printer = request.session.get('sellpoint_printer')
    else:
        request.session['sellpoint_printer'] = 0
        sellpoint_printer = 0
    return 8099 + sellpoint_printer

def sellpoint_vendor(request):
    VAR = log_variables(request)
    sellpoint = router_sellpoint(request, VAR['COMPANY'])
    if request.GET.get('sellpoint'):
        return HttpResponseRedirect(reverse_lazy('sellpoint:sellpoint_vendor'))
    nodes = sellpoint.get_menus()
    sellpoint_printer = get_printer(request)
    form = TicketForm()
    return render(request, 'sellpoint_vendor.html',locals())

def sellpoint_casher(request):
    VAR = log_variables(request)
    sellpoint = router_sellpoint(request, VAR['COMPANY'])
    if request.GET.get('sellpoint'):
        return HttpResponseRedirect(reverse_lazy('sellpoint:sellpoint_casher'))
    sellpoint_printer = get_printer(request)
    return render(request, 'sellpoint_casher.html',locals())

##############################################################
# SELLPOINT
##############################################################
######### LIST
class SellpointList(ListView):
    model, m = Sellpoint, 'sellpoint'
    template_name = m+'_list.html'
    paginate_by = 30

    def get_queryset(self):
        self.VAR, self.q = log_variables(self.request, module=self.request.resolver_match.app_name), self.request.GET.get('q')
        check_permissions(request=self.request, permission='can_view_'+self.m)
        if self.q:
            return Sellpoint.objects.filter(company=self.VAR['COMPANY'], active=True, name__icontains=self.q)
        return Sellpoint.objects.filter(company=self.VAR['COMPANY'], active=True)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
        return context

######### CREATE
class SellpointCreate(CreateView):
    model, m = Sellpoint, 'sellpoint'
    template_name = m+'_create.html'
    form_class = SellpointForm

    def get_form_kwargs(self):
        company = Company.objects.get(id=self.request.session.get('company'))
        kwargs = super(SellpointCreate, self).get_form_kwargs()
        kwargs['serials'] = Serial.objects.filter(Q (company = company, is_active=True, active=True) | Q (company__in = company.get_ancestors(include_self=False), is_global=True, is_active=True, active=True))
        if company.hasmodule('invoice'):
            kwargs['fiscaldata'] = FiscalData.objects.filter(company = company, active=True)
        else:
            kwargs['fiscaldata'] = False
        return kwargs

    def get_success_url(self):
        return reverse('sellpoint:'+self.m+'-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
        check_permissions(request=self.request, permission='can_create_'+self.m)
        return context

    def form_valid(self, form):
        form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
        return super().form_valid(form)

######### UPDATE
class SellpointUpdate(UpdateView):
    model, m = Sellpoint, 'sellpoint'
    template_name = m+'_create.html'
    form_class = SellpointForm

    def get_form_kwargs(self):
        company = Company.objects.get(id=self.request.session.get('company'))
        kwargs = super(SellpointUpdate, self).get_form_kwargs()
        kwargs['serials'] = Serial.objects.filter(Q (company = company, is_active=True, active=True) | Q (company__in = company.get_ancestors(include_self=False), is_global=True, is_active=True, active=True))
        if company.hasmodule('invoice'):
            kwargs['fiscaldata'] = FiscalData.objects.filter(company = company, active=True)
        else:
            kwargs['fiscaldata'] = False
        return kwargs

    def get_success_url(self):
        return reverse('sellpoint:'+self.m+'-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
        check_permissions(request=self.request, permission='can_edit_sellpoint', obj=self.object.company)
        return context

######### DELETE
class SellpointDelete(DeleteView):
    model, m = Sellpoint, 'sellpoint'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('sellpoint:'+self.m+'-list')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
        self.object.active = False
        self.object.save()
        messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
        return HttpResponseRedirect(self.get_success_url())




##############################################################
# MENU
##############################################################
######### LIST
class MenuList(ListView):
    model, m = Menu, 'menu'
    template_name = m+'_list.html'
    paginate_by = 30

    def get_queryset(self):
        self.VAR, self.q = log_variables(self.request), self.request.GET.get('q')
        check_permissions(request=self.request, permission='can_view_menu')
        if self.q:
            return Menu.objects.filter(company=self.VAR['COMPANY'], active=True, name__icontains=self.q)
        return Menu.objects.filter(company=self.VAR['COMPANY'], active=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
        return context

######### CREATE
class MenuCreate(CreateView):
    model, m = Menu, 'menu'
    template_name = m+'_create.html'
    form_class = MenuForm

    def get_form_kwargs(self):
        kwargs = super(MenuCreate, self).get_form_kwargs()
        kwargs['parents'] = Menu.objects.filter(company = Company.objects.get(id=self.request.session.get('company')), is_active='True', active='True')
        return kwargs

    def get_success_url(self):
        return reverse('sellpoint:'+self.m+'-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request), 'CreateView'
        check_permissions(request=self.request, permission='can_create_'+ self.m)
        return context

    def form_valid(self, form):
        form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
        return super().form_valid(form)

######### UPDATE
class MenuUpdate(UpdateView):
    model, m = Menu, 'menu'
    template_name = m+'_create.html'
    form_class = MenuForm

    def get_form_kwargs(self):
        kwargs = super(MenuUpdate, self).get_form_kwargs()
        kwargs['parents'] = Menu.objects.filter(company = Company.objects.get(id=self.request.session.get('company')), is_active='True', active='True')
        return kwargs

    def get_success_url(self):
        return reverse('sellpoint:'+self.m+'-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['class']= self.model, log_variables(self.request), 'UpdateView'
        check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
        return context

######### DELETE
class MenuDelete(DeleteView):
    model, m = Menu, 'menu'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('sellpoint:'+self.m+'-list')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
        self.object.active = False
        self.object.save()
        messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
        return HttpResponseRedirect(self.get_success_url())



##############################################################
# PRODUCT
##############################################################
######### LIST
class ProductList(ListView):
    model, m = Product, 'product'
    template_name = m+'_list.html'
    paginate_by = 50

    def get_queryset(self):
        self.VAR, self.q, self.q__= log_variables(self.request), self.request.GET.get('q'), False
        self.filters = []
        check_permissions(request=self.request, permission='can_view_'+self.m)
        query = Product.objects.filter(sellpoints__in = self.request.user.get_my_sellpoints(self.VAR['COMPANY']), active=True)
        menu=[]
        for product in query:
            if not product.menu in menu:
                menu.append(product.menu)
        if self.q:
            query = query.filter(name__icontains=self.q)
        self.filters.append({'label':'Punto de Venta', 'name':'q__sellpoint', 'objects':self.request.user.get_my_sellpoints(self.VAR['COMPANY']),'l': self.request.GET.get('lq__sellpoint', '-')})
        if self.request.GET.get('q__sellpoint'):
            query, self.q__ = query.filter(sellpoints=Sellpoint.objects.get(pk=self.request.GET.get('q__sellpoint'))), True
        self.filters.append({'label':'Menu', 'name':'q__menu', 'objects':menu,'l': self.request.GET.get('lq__menu', '-')})
        if self.request.GET.get('q__menu'):
            query, self.q__ = query.filter(menu=Menu.objects.get(pk=self.request.GET.get('q__menu'))), True
        self.filters.append({'label':'IVA', 'name':'q__iva', 'objects':{True,False},'l': self.request.GET.get('lq__iva', '-')})
        if self.request.GET.get('q__iva'):
            products = []
            product_attributes = Product_attributes.objects.filter(iva=self.request.GET.get('q__iva'))
            for p in product_attributes:
                if not p.product.pk in products:
                    products.append(p.product.pk)
            query, self.q__ = query.filter(pk__in = products), True
        self.filters.append({'label':'IEPS', 'name':'q__ieps', 'objects':{True,False},'l': self.request.GET.get('lq__ieps', '-')})
        if self.request.GET.get('q__ieps'):
            products = []
            product_attributes = Product_attributes.objects.filter(iva=self.request.GET.get('q__ieps'))
            for p in product_attributes:
                if not p.product.pk in products:
                    products.append(p.product.pk)
            query, self.q__ = query.filter(pk__in = products), True
        return query

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['q'], context['filters'], context['q__'] = self.model, self.VAR, self.q, self.filters, self.q__
        return context

######### PERMISSION
def permission_product(my_sellpoint, product_sellpoints):
    for sellpoint in product_sellpoints:
        if sellpoint in my_sellpoint:
            return True
    raise PermissionDenied
    return False

######### DETAIL
class ProductDetail(DetailView):
    model, m = Product, 'product'
    template_name = m+'_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'] = self.model, log_variables(self.request)
        permission_product(self.request.user.get_my_sellpoints(context['VAR']['COMPANY']), self.object.sellpoints.all())
        check_permissions(request=self.request, permission='can_view_'+self.m)
        return context

######### CREATE
class ProductCreate(CreateView):
    model, m = Product, 'product'
    template_name = m+'_create.html'
    form_class = ProductForm

    def get_form_kwargs(self):
        kwargs = super(ProductCreate, self).get_form_kwargs()
        self.VAR = log_variables(self.request)
        company = Company.objects.get(id=self.request.session.get('company'))
        kwargs['sellpoints'] = Sellpoint.objects.filter(pk__in=self.request.user.get_my_sellpoints(self.VAR['COMPANY']), is_active=True, active=True)
        kwargs['menus'] = Menu.objects.filter(company=company, is_active=True, active=True)
        return kwargs

    def get_success_url(self):
        return reverse('sellpoint:'+self.m+'-detail', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['class'] = self.model, self.VAR, 'CreateView'
        check_permissions(request=self.request, permission='can_create_'+self.m)
        return context

    def form_valid(self, form):
        form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
        return super().form_valid(form)

######### UPDATE
class ProductUpdate(UpdateView):
    model, m = Product, 'product'
    template_name = m+'_create.html'
    form_class = ProductForm

    def get_form_kwargs(self):
        kwargs = super(ProductUpdate, self).get_form_kwargs()
        company = Company.objects.get(id=self.request.session.get('company'))
        kwargs['sellpoints'] = Sellpoint.objects.filter(company=company, is_active=True, active=True)
        kwargs['menus'] = Menu.objects.filter(company=company, is_active=True, active=True)
        return kwargs

    def get_success_url(self):
        return reverse('sellpoint:'+self.m+'-detail', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request), 'UpdateView'
        permission_product(self.request.user.get_my_sellpoints(context['VAR']['COMPANY']), self.object.sellpoints.all())
        check_permissions(request=self.request, permission='can_edit_'+self.m)
        return context

######### DELETE
class ProductDelete(DeleteView):
    model, m = Product, 'product'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('sellpoint:'+self.m+'-list')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        VAR = log_variables(self.request)
        permission_product(self.request.user.get_my_sellpoints(VAR['COMPANY']), self.object.sellpoints.all())
        check_permissions(request=self.request, permission='can_delete_'+self.m)
        self.object.active = False
        self.object.save()
        messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
        return HttpResponseRedirect(self.get_success_url())



##############################################################
# PRODUCT ATTR
##############################################################
######### UPDATE
class Product_attributesUpdate(UpdateView):
    model, m = Product_attributes, 'product_attributes'
    template_name = m+'_create.html'
    form_class = Product_attributesForm

    def get_form_kwargs(self):
        kwargs = super(Product_attributesUpdate, self).get_form_kwargs()
        return kwargs

    def get_success_url(self):
        return reverse('sellpoint:product-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['class']= self.model, log_variables(self.request), 'UpdateView'
        permission_product(self.request.user.get_my_sellpoints(context['VAR']['COMPANY']), self.object.product.sellpoints.all())
        check_permissions(request=self.request, permission='can_edit_'+self.m)
        return context

    def form_valid(self, form):
        return super().form_valid(form)



##############################################################
# TICKETS
##############################################################
######### LIST
class TicketList(ListView):
    model, m = Ticket, 'ticket'
    template_name = m+'_list.html'
    paginate_by = 100

    def get_queryset(self):
        self.VAR, self.q, self.q__, self.q__cut= log_variables(self.request), self.request.GET.get('q'), False, False
        self.filters = []
        check_permissions(request=self.request, permission='can_view_'+self.m)
        query = self.model.objects.filter(sellpoint__in = self.request.user.get_my_sellpoints(self.VAR['COMPANY']))
        if self.q:
            query, self.q__ = query.filter(Q(barcode__icontains=self.q)|Q(sellpoint__name__icontains=self.q)), True
        if self.request.GET.get('q__cut'):
            cut=get_object_or_404(Cut, pk=self.request.GET.get('q__cut'))
            query, self.q__cut = query.filter(  cut=cut ), cut
        if not self.q__cut:
            self.filters.append({'label':'Puntos de venta', 'name':'q__sellpoints', 'objects':self.request.user.get_my_sellpoints(self.VAR['COMPANY']),'l': self.request.GET.get('lq__sellpoints', '-')})
            if self.request.GET.get('q__sellpoints'):
                query, self.q__ = query.filter(sellpoint__pk=self.request.GET.get('q__sellpoints')), True
        if not self.request.user.has_perm('sellpoint.admin_ticket'):
            query = query.filter(cut__final_time__isnull = True)
        #if self.request.user.has_perm('sellpoint.admin_ticket'):
            #query = query.filter(date__gte=timezone.now()-datetime.timedelta(days=5))
        self.filters.append({'label':'Estatus', 'name':'q__status', 'objects':self.model.FILTER_STATUS_TICKET,'l': self.request.GET.get('lq__status', '-')})
        if self.request.GET.get('q__status'):
            query, self.q__ = query.filter(status=self.request.GET.get('q__status')[0:3]), True
        query = query.exclude(status='CAN')
        show_orders = self.request.GET.get('show_orders')
        if show_orders:
            query = query.filter(order_tocount__gt=0)
        return query

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['q'], context['filters'], context['q__'], context['q__cut'] = self.model, self.VAR, self.q, self.filters, self.q__, self.q__cut
        return context

##############################################################
# CUTS
##############################################################
######### LIST
class CutList(ListView):
    model, m = Cut, 'cut'
    model = Cut
    template_name = m+'_list.html'
    paginate_by = 35
    def get_queryset(self):
        self.VAR, self.q, self.q__= log_variables(self.request), self.request.GET.get('q'), False
        self.filters = []
        check_permissions(request=self.request, permission='can_view_cut')
        query = self.model.objects.filter(sellpoint__in = self.request.user.get_my_sellpoints(self.VAR['COMPANY'])).exclude(ocult=True)
        if self.q:
            return query.filter(serial=int(self.q))
        self.filters.append({'label':'Puntos de venta', 'name':'q__sellpoints', 'objects':self.request.user.get_my_sellpoints(self.VAR['COMPANY']),'l': self.request.GET.get('lq__sellpoints', '-')})
        if self.request.GET.get('q__sellpoints'):
            query, self.q__ = query.filter(sellpoint=self.request.GET.get('q__sellpoints')), True
        if self.request.user.is_staff or self.request.user.is_superuser:
            return query.order_by('-init_time')
        else:
            query = query.filter(Q(init_time__gte=timezone.now()-datetime.timedelta(days=5))|Q(final_time__isnull=True))
        return query.order_by('-init_time')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['q'], context['filters'], context['q__'] = self.model, self.VAR, self.q, self.filters, self.q__
        return context

class CutDetail(DetailView):
    model, m = Cut, 'cut'
    template_name = m+'_detail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'] = self.model, log_variables(self.request)
        check_permissions(request=self.request, permission='can_view_'+self.m, obj=self.object.sellpoint.company)
        return context

class CutDetail_(DetailView):
    model, m = Cut, 'cut'
    template_name = m+'_detail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'] = self.model, log_variables(self.request)
        check_permissions(request=self.request, permission='can_view_'+self.m, obj=self.object.sellpoint.company)
        return context

class CutDetail_porciento(DetailView):
    model, m = Cut, 'cut'
    template_name = m+'_detail_.html'
    def get_context_data(self, **kwargs):
        self.object.ras = self.kwargs['cant']
        self.object.sellpoint.save()
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'] = self.model, log_variables(self.request)
        check_permissions(request=self.request, permission='can_view_'+self.m, obj=self.object.sellpoint.company)
        return context


class CutDetail_porciento_(DetailView):
    model, m = Cut, 'cut'
    template_name = m+'_detail__.html'
    def get_context_data(self, **kwargs):
        self.object.ras = self.kwargs['cant']
        self.object.sellpoint.save()
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'] = self.model, log_variables(self.request)
        context['cuts'] = Cut.objects.filter(Q(sellpoint__pk=22, id__gt=9331, id__lt=10493)).order_by('sellpoint__pk','pk')
        context['print'] = self.request.GET.get('print')
        check_permissions(request=self.request, permission='can_view_'+self.m, obj=self.object.sellpoint.company)
        return context


##############################################################
# INVOICES
##############################################################
######### LIST
class InvoiceList(ListView):
    model, m = Invoice, 'invoice'
    template_name = m+'_list.html'
    paginate_by = 8
    def get_queryset(self):
        self.VAR, self.q, self.q__ = log_variables(self.request), self.request.GET.get('q'), False
        self.filters = []
        query = Invoice.objects.all()
        #if self.q:
            #return query.filter(serial=int(self.q))
        #self.filters.append({'label':'Puntos de venta', 'name':'q__sellpoints', 'objects':self.request.user.get_my_sellpoints(self.VAR['COMPANY']),'l': self.request.GET.get('lq__sellpoints', '-')})
        #if self.request.GET.get('q__sellpoints'):
            #query, self.q__ = query.filter(sellpoint=self.request.GET.get('q__sellpoints')), True
        #if self.request.user.is_staff or self.request.user.is_superuser:
            #return query.order_by('-init_time')
        #else:
            #query = query.filter(Q(init_time__gte=timezone.now()-datetime.timedelta(days=5))|Q(final_time__isnull=True))
        return query.order_by('-id')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'], context['q'], context['filters'], context['q__'] = self.model, self.VAR, self.q, self.filters, self.q__
        return context

class InvoiceDetail(DetailView):
    model, m = Invoice, 'invoice'
    template_name = m+'_detail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['model'], context['VAR'] = self.model, log_variables(self.request)
        context['xml'] = xmltodict.parse(self.object.xml.encode('utf-8'))
        fiscaldata = FiscalData.objects.filter(company=self.object.company).first()
        context['data'] = {
            'emisor_calle': fiscaldata.calle,
            'emisor_numero_exterior': fiscaldata.numero_exterior,
            'emisor_numero_interior': fiscaldata.numero_interior,
            'emisor_colonia': fiscaldata.colonia,
            'emisor_localidad': fiscaldata.localidad,
            'emisor_codigo_postal': fiscaldata.codigo_postal,
            'emisor_municipio_delegacion': fiscaldata.municipio_delegacion,
            'emisor_estado': fiscaldata.estado,
            'emisor_pais': fiscaldata.pais,
        }
        context['invoice'] = None
        return context

@csrf_exempt
def sellpoint_api(request):
    method = request.GET.get('method')
    if method == 'get_products':
        products = Product_attributes.objects.filter( is_active = True, sellpoint__pk = request.GET.get('sellpoint'), product__is_active=True, product__active=True )
        if request.GET.get('sellpoint') == '22':
            products = products.order_by('product__name','price')
        return JsonResponse({
            'products': Product_attributesSerializer(products, many=True).data,
            'sellpoint': SellpointSerializer( Sellpoint.objects.get(pk=request.GET.get('sellpoint')) ).data,
            'api' : True,
        })
    elif method == 'ras_cut':
        cut = Cut.objects.get(id=request.GET.get('cut'))
        cut.ras = int(request.GET.get('ras'))
        cut.save()
        lines = {}
        total, iva, ieps = 0, 0, 0
        menus_with_pulls = []
        tickets = Ticket.objects.filter(cut=cut, status = 'COB')
        tickets.update(hide_ras=False)
        total = 0
        for ticket in tickets:
            total += ticket.total
        rastotal = total*cut.ras / 100
        rastickets = []
        total = 0
        if Ticket.objects.filter(cut=cut, status='COB', total__gt=2000):
            tickets = Ticket.objects.filter(cut = cut, status = 'COB').order_by('-total')
        else:
            tickets = Ticket.objects.filter(cut = cut, status = 'COB').order_by('total')
        for ticket in tickets:
            total += ticket.total
            rastickets.append(ticket.id)
            if total > rastotal:
                break
        tickets.exclude(pk__in = rastickets).update(hide_ras=True)
        rastickets = Ticket.objects.filter(pk__in = rastickets)
        ticket_lines = Ticket_lines.objects.filter(ticket__in = rastickets.all())
        lines = {}
        total, iva, ieps, blanco, blanco_pz, dulce, dulce_pz, otros, otros_pz = 0, 0, 0, 0, 0, 0, 0,0,0
        for ticket_line in ticket_lines:
            if ticket_line.iva:
                otros += ticket_line.total
                otros_pz += ticket_line.quantity
            elif ticket_line.ieps:
                dulce += ticket_line.total
                dulce_pz += ticket_line.quantity
            else:
                blanco += ticket_line.total
                blanco_pz += ticket_line.quantity
            iva += ticket_line.iva
            ieps += ticket_line.ieps
            total += ticket_line.total
            if not (ticket_line.text_product + str(ticket_line.price)) in lines:
                lines.update({(ticket_line.text_product + str(ticket_line.price)):ticket_line})
            else:
                lines[(ticket_line.text_product + str(ticket_line.price))].quantity += ticket_line.quantity
                lines[(ticket_line.text_product + str(ticket_line.price))].iva += ticket_line.iva
                lines[(ticket_line.text_product + str(ticket_line.price))].ieps += ticket_line.ieps
                lines[(ticket_line.text_product + str(ticket_line.price))].total += ticket_line.total
        return JsonResponse({
            'api': True,
        })
    elif method == 'ocult_form':
        cut = Cut.objects.get(id=request.GET.get('cut'))
        cut.ocult = True
        cut.save()
        return JsonResponse({
            'api': True,
        })
    elif method == 'print_ticket':
        sellpoint = Sellpoint.objects.get(id=request.GET.get('sellpoint'))
        ticket = json.loads(request.POST.get('ticket'))
        serializer = TicketSerializer( Ticket().new_ticket(ticket, request.user, sellpoint, True) )
        return JsonResponse({
            'api' : True,
            'data' : serializer.data,
        })
    elif method == 'update_tickets':
        sellpoint = Sellpoint.objects.get(id=request.GET.get('sellpoint'))
        ticket = Ticket().update_ticket(json.loads(request.POST.get('ticket')), sellpoint, request.user)
        serializer = TicketSerializer(ticket)
        return JsonResponse({
            'api' : True,
            'data' : serializer.data,
        })
    elif method == 'get_cut_tickets':
        sellpoint = Sellpoint.objects.get(id=request.GET.get('sellpoint'))
        cut = sellpoint.assign_cut()
        if sellpoint.id == 20:
            serializer = TicketSerializer( Ticket.objects.filter( sellpoint__company = sellpoint.company, cut__final_time__isnull = True ).order_by('-id'), many=True )
        else:
            serializer = TicketSerializer( Ticket.objects.filter( sellpoint = sellpoint, cut = cut ).order_by('-id'), many=True )
        return JsonResponse({
            'api' : True,
            'data' : serializer.data,
        })
    elif method == 'ticket_payment':
        elements_barcode = request.POST.get('barcode').split(',')
        sellpoint = Sellpoint.objects.get(id=request.GET.get('sellpoint'))
        cut = sellpoint.assign_cut()
        ticket = None
        if sellpoint.id == 20:
            sellp = [20,21]
            ticket = Ticket.objects.filter( sellpoint__pk__in=sellp, barcode=elements_barcode[0], cut__final_time__isnull=True ).first()
        else:
            try:
                ticket = Ticket.objects.filter( sellpoint=sellpoint, barcode=elements_barcode[0], cut=cut, secret_code=elements_barcode[2]).first()
            except:
                pass
            if not ticket:
                ticket = Ticket.objects.filter( sellpoint=sellpoint, barcode=elements_barcode[0], cut=cut ).first()
        if not ticket:
            ticket = Ticket.objects.filter( total=elements_barcode[1], secret_code=elements_barcode[2] ).first()
            if not ticket and len(elements_barcode)>=6:
                if elements_barcode[6] == 'false':
                    data = {'status':4,'title':"EXITO!...",'message':'Ticket cobrado con éxito', 'color':'inverse', 'time': 1000}
                else:
                    data = {'status':5,'title':"ERROR!...",'message':'No se encontró este ticket.', 'color':'danger', 'time': 2500}
        if ticket:
            if ticket.cut.final_time:
                data = {'status':3,'title':"ERROR!...",'message':'Este ticket ha caducado.', 'color':'danger', 'time': 2500}
            elif ticket.status == 'PEN':
                ticket.status = 'COB'
                ticket.save()
                data = {'status':1,'title':"EXITO!...",'message':'Ticket cobrado con éxito.', 'color':'inverse', 'time': 1000}
            elif ticket.status == 'COB':
                data = {'status':2,'title':"ERROR!...",'message':'Este ticket ya fue cobrado antes.', 'color':'danger', 'time': 2500}
        return JsonResponse({
            'api' : True,
            'data' : data,
        })
    elif method == 'get_last_cuts':
        sellpoint = Sellpoint.objects.get(id=request.GET.get('sellpoint'))
        page = int(request.GET.get('page'))
        items = int(request.GET.get('items'))
        serializer = CutsSerializer( Cut.objects.filter( sellpoint = sellpoint ).order_by('-id')[page:page+items], many=True )
        return JsonResponse({
            'api' : True,
            'data' : serializer.data,
        })
    elif method == 'make_cut':
        sellpoint = Sellpoint.objects.get(id=request.GET.get('sellpoint'))
        if sellpoint.pk == 22:
            cut = Cut.objects.filter(sellpoint=sellpoint, final_time=None).first()
        else:
            cut = Cut.objects.filter(sellpoint=sellpoint, final_time=None).first()
        cut.final_time = timezone.now()
        cut.serial = len(Cut.objects.filter(sellpoint=sellpoint))
        ras = cut.cut_lines_ras()
        serializer = CutsSerializer(cut)
        l = cut.cut_lines_without_pulls_json()
        cut.save()
        return JsonResponse({
            'data' : serializer.data,
            'num': str(cut.serial),
            'pen': cut.pen_total,
            'l': l,
            'iva': str(ras['iva']),
            'ieps': str(ras['ieps']),
            'blanco': str(ras['blanco']),
            'blanco_pz': str(ras['blanco_pz']),
            'dulce': str(ras['dulce']),
            'dulce_pz': str(ras['dulce_pz']),
            'otros': str(ras['otros']),
            'otros_pz': str(ras['otros_pz']),
        })
    elif method == 'cancel_cut':
        sellpoint = Sellpoint.objects.get(id=request.GET.get('sellpoint'))
        cut = Cut.objects.filter(sellpoint=sellpoint, final_time=None).first()
        cut.show = False
        cut.save()
        serializer = CutsSerializer(cut)
        return JsonResponse({
            'data': serializer.data,
        })
    elif method == 'generateTicket':
        sellpoint = Sellpoint.objects.get(id=request.GET.get('sellpoint'))
        cut = Cut.objects.get(id=request.GET.get('cut'))
        ticket = Ticket()
        ticket.barcode = "0000000000000"
        ticket.sellpoint = sellpoint
        ticket.user = User.objects.all().last()
        ticket.user_string = ''
        ticket.status = "COB"
        ticket.cut = cut
        ticket.total = float(request.GET.get('total'))
        ticket.secret_code = 'xxxyyyxxxyyy'
        ticket.save()

        ticket_lines =  Ticket_lines()
        ticket_lines.ticket = ticket
        ticket_lines.product = Product_attributes.objects.get(id=5853)
        ticket_lines.text_product = 'OTRO'
        ticket_lines.alias = 'OTRO'
        ticket_lines.quantity = 1
        ticket_lines.price = float(request.GET.get('total'))
        ticket_lines.total = float(request.GET.get('total'))
        ticket_lines.iva = 0
        ticket_lines.ieps = 0
        ticket_lines.save()

        serializer = CutsSerializer(cut)
        return JsonResponse({
            'data': serializer.data,
        })
    elif method == 'get_ticket_info':
        ticket = Ticket.objects.get(pk=request.GET.get('pk'))
        return JsonResponse({
            'api' : True,
            'ticket' : TicketSerializer( ticket ).data,
            'ticket_lines' : Ticket_linesSerializer( Ticket_lines.objects.filter(ticket=ticket), many=True ).data,
            'sellpoint' : SellpointSerializer( ticket.sellpoint ).data,
        })
    elif method == 'get_randomticket':
        ticket = Ticket.objects.order_by('?').first()
        return JsonResponse({
            'api' : True,
            'ticket' : TicketSerializer( ticket ).data,
            'ticket_lines' : Ticket_linesSerializer( Ticket_lines.objects.filter(ticket=ticket), many=True ).data,
            'sellpoint' : SellpointSerializer( ticket.sellpoint ).data,
        })
    elif method == 'change_status_ticket':
        ticket = Ticket.objects.get(pk=request.GET.get('pk'))
        ticket.status = request.GET.get('status')
        ticket.save()
        return JsonResponse({
            'api' : True,
        })
    elif method == 'reset_invoice':
        ticket = Ticket.objects.get(pk=request.GET.get('pk'))
        ticket.invoice = False
        ticket.save()
        return JsonResponse({
            'api' : True,
        })
    elif method == 'get_codes':
        #serializer = Clave_Productos_ServiciosSerializer( Clave_Productos_Servicios.objects.filter(Q(codigo__icontains=request.GET.get('search'))|Q(nombre__icontains=request.GET.get('search')))[0:200] , many=True )
        serializer = Clave_Productos_ServiciosSerializer( Clave_Productos_Servicios.objects.all()[0:200], many=True )
        return JsonResponse({
            'items' : serializer.data,
        })
    elif method == 'get_units':
        #serializer = Clave_Unidades_MedidaSerializer( Clave_Unidades_Medida.objects.filter(Q(codigo__icontains=request.GET.get('search'))|Q(nombre__icontains=request.GET.get('search')))[0:200] , many=True )
        serializer = Clave_Productos_ServiciosSerializer( Clave_Unidades_Medida.objects.all()[0:200], many=True )
        return JsonResponse({
            'items' : serializer.data,
        })
    elif method == 'get_actual_cut':
        sellpoint = Sellpoint.objects.get(id=request.GET.get('sellpoint'))
        serializer = CutsSerializer( sellpoint.assign_cut() )
        return JsonResponse({
            'data' : serializer.data,
        })
    elif method == 'make_cut_cfdi':
        cut = Cut.objects.get(id=request.GET.get('cut'))
        cut.make_cut_cfdi()
        serializer = CutsSerializer( cut )
        return JsonResponse({
            'data' : serializer.data,
        })
    else:
        return render(request, 'viewcut.html', locals())
    try:
        pass
    except:
        exc_type, exc_value, exc_traceback = exc_tuple = sys.exc_info()
        return JsonResponse({
            'message': 'Por favor contacte al administrador del sistema con este error: ' + str(exc_value) + ' in: ' + str(exc_traceback.tb_lineno) + ' type: ' + str(exc_type.__name__),
            'api' : False,
        })
