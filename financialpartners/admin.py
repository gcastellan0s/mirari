from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *

class PassAdmin(admin.ModelAdmin):
    pass


@admin.register(Financialpartners)
class FinancialpartnersAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
