from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import SingleObjectMixin, DetailView
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.db.models import Q
from django.urls import reverse
from django.contrib import messages
from project.views import log_variables, variables, check_permissions
from project.models import *
from project.forms import *
from invoice.forms import FiscalDataForm
from django.core.exceptions import PermissionDenied
from .models import *
from .forms import *
from .serializers import *
import json
from django.conf import settings
import requests
import xmltodict


from sellpoint.models import Sellpoint

##############################################################
# Financialpartners
##############################################################
######### LIST
class FinancialpartnersList(ListView):
	model = Financialpartners
	m = model.MODEL
	template_name = m+'_list.html'
	paginate_by = 30

	def get_queryset(self):
		self.VAR, self.q = log_variables(self.request, module=self.request.resolver_match.app_name), self.request.GET.get('q')
		check_permissions(request=self.request, permission='can_view_'+self.m)
		query = self.model.objects.filter(company=self.VAR['COMPANY'], active=True)
		if self.q:
			query.objects.filter( Q(email__icontains=self.q),Q(username__icontains=self.q),Q(first_name__icontains=self.q),Q(last_name__icontains=self.q) )
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		return context

######### DETAIL
class FinancialpartnersDetail(DetailView):
	model= Financialpartners
	m = model.MODEL
	template_name = m+'_detail.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name)
		check_permissions(request=self.request, permission='can_view_'+self.m)
		return context

######### CREATE
class FinancialpartnersCreate(CreateView):
	model = Financialpartners
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = FinancialpartnersForm

	def get_success_url(self):
		return reverse( self.model.APP+':'+self.m+'-detail', kwargs={'pk': self.object.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
		form.instance.set_password(settings.VAR_PRODUCTION['VAR_SECRET_KEY'])
		form.instance.username = self.request.user.company.get_root().code.upper() + '_'
		form.instance.visible_username = form.instance.username
		ret = super().form_valid(form)
		fp = form.save()
		if not fp.reference_number:
			fp.reference_number = 'cyp' + str(fp.pk)
		fp.username = self.request.user.company.get_root().code.upper() + '_cyp00' + str(fp.pk)
		fp.visible_username = 'cyp00' + str(fp.pk)
		fp.save()

		history = History()
		history.user = self.request.user
		history.financialpartners = fp
		history.text = 'Información del socio creada'
		history.action = 'Create'
		history.save()

		return ret
		

######### UPDATE
class FinancialpartnersUpdate(UpdateView):
	model, m = Financialpartners, 'financialpartners'
	template_name = m+'_create.html'
	form_class = FinancialpartnersForm

	def get_form_kwargs(self):
		company = Company.objects.get(id=self.request.session.get('company'))
		kwargs = super(FinancialpartnersUpdate, self).get_form_kwargs()
		return kwargs

	def get_success_url(self):
		return reverse('financialpartners:'+self.m+'-detail', kwargs={'pk': self.object.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_sellpoint', obj=self.object.company)

		history = History()
		history.user = self.request.user
		history.financialpartners = self.object
		history.text = 'Información del socio actualizada'
		history.action = 'Update'
		history.save()

		return context

######### DELETE
class FinancialpartnersDelete(DeleteView):
	model, m = Financialpartners, 'financialpartners'

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('financialpartners:'+self.m+'-list')
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()

		history = History()
		history.user = self.request.user
		history.financialpartners = fp
		history.text = 'Información del socio eliminada'
		history.action = 'Delete'
		history.save()

		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())



##############################################################
# FiscalData
##############################################################
######### CREATE
class FiscalDataCreate(CreateView):
	model = FiscalData
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = FiscalDataForm

	def get_success_url(self):
		return reverse( 'financialpartners:financialpartners-detail', kwargs={'pk': self.kwargs['pk']})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)

		history = History()
		history.user = self.request.user
		history.financialpartners = fp
		history.text = 'Información fiscal del socio creada'
		history.action = 'Create'
		history.save()

		return context

	def form_valid(self, form):
		form.instance.company = User.objects.get(pk=self.kwargs['pk']).company
		form.instance.user = User.objects.get(pk=self.kwargs['pk'])
		return super().form_valid(form)

######### UPDATE
class FiscalDataUpdate(UpdateView):
	model = FiscalData
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = FiscalDataForm

	def get_form_kwargs(self):
		kwargs = super(FinancialpartnersUpdate, self).get_form_kwargs()
		return kwargs

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
		return context

######### DELETE
class FiscalDataDelete(DeleteView):
	model = FiscalData
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.delete()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())


##############################################################
# Address
##############################################################
######### CREATE
class AddressCreate(CreateView):
	model = Address
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = AddressForm

	def get_success_url(self):
		return reverse( 'financialpartners:financialpartners-detail', kwargs={'pk': self.kwargs['pk']})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.user = User.objects.get(pk=self.kwargs['pk'])
		return super().form_valid(form)

######### UPDATE
class AddressUpdate(UpdateView):
	model = Address
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = AddressForm

	def get_form_kwargs(self):
		kwargs = super(FinancialpartnersUpdate, self).get_form_kwargs()
		return kwargs

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
		return context

######### DELETE
class AddressDelete(DeleteView):
	model = Address
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())



##############################################################
# Beneficiaries
##############################################################
######### CREATE
class Financialpartners_beneficiarieCreate(CreateView):
	model = Financialpartners_beneficiarie
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = Financialpartners_beneficiarieForm

	def get_success_url(self):
		return reverse( 'financialpartners:financialpartners-detail', kwargs={'pk': self.kwargs['pk']})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.user = Financialpartners.objects.get(pk=self.kwargs['pk'])
		return super().form_valid(form)

######### UPDATE
class Financialpartners_beneficiarieUpdate(UpdateView):
	model = Financialpartners_beneficiarie
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = Financialpartners_beneficiarieForm

	def get_form_kwargs(self):
		kwargs = super(FinancialpartnersUpdate, self).get_form_kwargs()
		return kwargs

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
		return context

######### DELETE
class Financialpartners_beneficiarieDelete(DeleteView):
	model = Financialpartners_beneficiarie
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())

##############################################################
# Bank
##############################################################
######### CREATE
class BankCreate(CreateView):
	model = Bank
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = BankForm

	def get_success_url(self):
		return reverse( 'financialpartners:financialpartners-detail', kwargs={'pk': self.kwargs['pk']})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.user = Financialpartners.objects.get(pk=self.kwargs['pk'])
		return super().form_valid(form)

######### UPDATE
class BankUpdate(UpdateView):
	model = Financialpartners_beneficiarie
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = Financialpartners_beneficiarieForm

	def get_form_kwargs(self):
		kwargs = super(FinancialpartnersUpdate, self).get_form_kwargs()
		return kwargs

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
		return context

######### DELETE
class BankDelete(DeleteView):
	model = Financialpartners_beneficiarie
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())


##############################################################
# Bank
##############################################################
######### CREATE
class File_userCreate(CreateView):
	model = File_user
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = FileForm

	def get_success_url(self):
		return reverse( 'financialpartners:financialpartners-detail', kwargs={'pk': self.kwargs['pk']})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.user = Financialpartners.objects.get(pk=self.kwargs['pk'])
		form.instance.module = Module.objects.get(code='financialpartners')
		return super().form_valid(form)

######### UPDATE
class File_userUpdate(UpdateView):
	model = File_user
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = FileForm

	def get_form_kwargs(self):
		kwargs = super(FinancialpartnersUpdate, self).get_form_kwargs()
		return kwargs

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
		return context

######### DELETE
class File_userDelete(DeleteView):
	model = File_user
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())


##############################################################
# Bank
##############################################################
######### CREATE
class InvestmentCreate(CreateView):
	model = Investment
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = InvestmentForm

	def get_success_url(self):
		return reverse( 'financialpartners:financialpartners-detail', kwargs={'pk': self.kwargs['pk']})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.user = Financialpartners.objects.get(pk=self.kwargs['pk'])
		return super().form_valid(form)

######### UPDATE
class InvestmentUpdate(UpdateView):
	model = Investment
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = InvestmentForm

	def get_form_kwargs(self):
		kwargs = super(FinancialpartnersUpdate, self).get_form_kwargs()
		return kwargs

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
		return context

######### DELETE
class InvestmentDelete(DeleteView):
	model = Investment
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('financialpartners:financialpartners-detail', kwargs={'pk': self.object.user.pk})
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())


##############################################################
# QEQ
##############################################################
class QEQCreate(CreateView):
	model = QEQ
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = QEQForm

	def get_success_url(self):
		return reverse( 'financialpartners:qeq-add' )

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_view_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.user = Financialpartners.objects.get(pk=self.kwargs['pk'])
		return super().form_valid(form)


@csrf_exempt
def financialpartners_api(request):
	method = request.GET.get('method')
	if method == 'get_clave_actividad_economica':
		serializer = Clave_Actividad_EconomicaSerializer( Clave_Actividad_Economica.objects.filter(Q(codigo__icontains=request.GET.get('search'))|Q(nombre__icontains=request.GET.get('search')))[0:200] , many=True )
		return JsonResponse({
			'items' : serializer.data,
		})
	if method == 'qeq':
		session = requests.Session()
		response = session.get('https://qeq.mx/datos/qws/access?var1=pgaray@gimpsa.com.mx&var2=qeq634')
		if request.GET.get('razon') :
			url = 'https://qeq.mx/datos/qws/pepse?'
			url += 'rfc='+request.GET.get('rfc')+'&'
			url += 'razonsoc='+request.GET.get('razon')+'&'
		else:
			url = 'https://qeq.mx/datos/qws/pepsp?'
			url += 'nombre='+request.GET.get('name')+'&'
			url += 'paterno='+request.GET.get('first_name')+'&'
			url += 'materno='+request.GET.get('last_name')+'&'
			url += 'curp='+request.GET.get('curp')+'&'
			url += 'rfc='+request.GET.get('rfc')+'&'
			url += 'issste='+request.GET.get('issste')+'&'
			url += 'imss='+request.GET.get('imss')+'&'
		r = session.get(url,cookies=session.cookies.get_dict())
		jsonxml = xmltodict.parse(r.text)
		return JsonResponse({
			'data' : jsonxml,
			'url' : url,
		})
	try:
		pass
	except:
		exc_type, exc_value, exc_traceback = exc_tuple = sys.exc_info()
		return JsonResponse({
			'message': 'Por favor contacte al administrador del sistema con este error: ' + str(exc_value) + ' in: ' + str(exc_traceback.tb_lineno) + ' type: ' + str(exc_type.__name__),
			'api' : False,
		})


