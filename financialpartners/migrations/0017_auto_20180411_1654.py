# Generated by Django 2.0.1 on 2018-04-11 21:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('financialpartners', '0016_auto_20180411_1511'),
    ]

    operations = [
        migrations.AlterField(
            model_name='financialpartners',
            name='reference_number',
            field=models.CharField(blank=True, help_text='Si no cuenta con uno el sistema le asiganará un número', max_length=250, null=True, verbose_name='Identificación del socio'),
        ),
    ]
