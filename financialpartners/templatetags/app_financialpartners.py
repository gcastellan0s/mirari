from datetime import datetime
from django import template
import requests
import xmltodict

register = template.Library()

@register.filter
def qeq_rfc(obj):
	session = requests.Session()
	response = session.get('https://qeq.mx/datos/qws/access?var1=pgaray@gimpsa.com.mx&var2=qeq634')
	if obj.persona == 'Física':
		url = 'https://qeq.mx/datos/qws/pepsp?'
	else:
		url = 'https://qeq.mx/datos/qws/pepse?'
	url += 'rfc=' + obj.rfc
	r = session.get(url,cookies=session.cookies.get_dict())
	jsonxml = xmltodict.parse(r.text)
	return jsonxml['xml']