from django.urls import path
from .views import *

app_name = "financialpartners"

urlpatterns = [
	path('socios-prestamistas/', FinancialpartnersList.as_view(), name='financialpartners-list'),
	path('socio-prestamista/agregar/', FinancialpartnersCreate.as_view(), name='financialpartners-add'),
	path('socio-prestamista/ver/<int:pk>/', FinancialpartnersDetail.as_view(), name='financialpartners-detail'),
	path('socio-prestamista/actualizar/<int:pk>/', FinancialpartnersUpdate.as_view(), name='financialpartners-update'),
	path('socio-prestamista/eliminar/<int:pk>/', FinancialpartnersDelete.as_view(), name='financialpartners-delete'),

	path('socio-direccion/<int:pk>/agregar/', AddressCreate.as_view(), name='address-add'),
	path('socio-direccion/actualizar/<int:pk>/', AddressUpdate.as_view(), name='address-update'),
	path('socio-direccion/eliminar/<int:pk>/', AddressDelete.as_view(), name='address-delete'),

	path('socio-dato-fiscal/<int:pk>/agregar/', FiscalDataCreate.as_view(), name='fiscaldata-add'),
	path('socio-dato-fiscal/actualizar/<int:pk>/', FiscalDataUpdate.as_view(), name='fiscaldata-update'),
	path('socio-dato-fiscal/eliminar/<int:pk>/', FiscalDataDelete.as_view(), name='fiscaldata-delete'),

	path('socio-beneficiario/<int:pk>/agregar/', Financialpartners_beneficiarieCreate.as_view(), name='financialpartners_beneficiarie-add'),
	path('socio-beneficiario/actualizar/<int:pk>/', Financialpartners_beneficiarieUpdate.as_view(), name='financialpartners_beneficiarie-update'),
	path('socio-beneficiario/eliminar/<int:pk>/', Financialpartners_beneficiarieDelete.as_view(), name='financialpartners_beneficiarie-delete'),

	path('socio-banco/<int:pk>/agregar/', BankCreate.as_view(), name='bank-add'),
	path('socio-banco/actualizar/<int:pk>/', BankUpdate.as_view(), name='bank-update'),
	path('socio-banco/eliminar/<int:pk>/', BankDelete.as_view(), name='bank-delete'),

	path('socio-archivo/<int:pk>/agregar/', File_userCreate.as_view(), name='file_user-add'),
	path('socio-archivo/actualizar/<int:pk>/', File_userUpdate.as_view(), name='file_user-update'),
	path('socio-archivo/eliminar/<int:pk>/', File_userDelete.as_view(), name='file_user-delete'),

	path('socio-inversion/<int:pk>/agregar/', InvestmentCreate.as_view(), name='investment-add'),
	path('socio-inversion/actualizar/<int:pk>/', InvestmentUpdate.as_view(), name='investment-update'),
	path('socio-inversion/eliminar/<int:pk>/', InvestmentDelete.as_view(), name='investment-delete'),

	path('consulta/qeq/', QEQCreate.as_view(), name='qeq-add'),

	path('financialpartners-api/', financialpartners_api, name='financialpartners_api'),
]