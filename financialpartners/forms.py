# -*- encoding: utf-8 -*-
from django import forms
from django.forms import widgets
from project.forms import MaterialCheckboxWidget, MaterialDateWidget, MaterialTextWidget, SelectWidget, SelectMultipleWidget, MaterialColorWidget, BaseUserForm, MaterialDateWidget
from project.models import User, Address, Bank, File_user
from invoice.models import FiscalData
from .models import *

class FinancialpartnersForm(forms.ModelForm):
	class Meta:
		model = Financialpartners
		fields = 'first_name', 'last_name', 'email', 'actividad', 'birthday', 'gender', 'country', 'reference_number'
		labels = {
			'first_name':'Nombre(s) del socio',
			'last_name':'Apellidos del socio',
			'birthday':'Fecha de nacimiento',
			'gender':'',
			'country':'',
			'reference_number':'Numero de referencia del socio',
		}
		help_texts = {
			'gender':'Genero',
			'country':'Pais de origen',
		}
		widgets={
			'actividad':SelectWidget(attrs={'class':'select2',}),
			'country':SelectWidget(attrs={'class':'select2',}),
			'gender':SelectWidget(attrs={'class':'select2',}),
		}


class AddressForm(forms.ModelForm):
	class Meta:
		model = Address
		fields = 'name', 'calle','numero_exterior','numero_interior','colonia','municipio_delegacion','estado','codigo_postal','pais','notes'
		widgets={
			'estado':SelectWidget(attrs={'class':'select2',}),
			'pais':SelectWidget(attrs={'class':'select2',}),
		}


class FiscalDataForm(forms.ModelForm):
	class Meta:
		model = FiscalData
		fields = 'persona','rfc','razon_social','curp','telefono','contacto','email_contacto','calle','numero_exterior','numero_interior','colonia','localidad','municipio_delegacion','estado','codigo_postal','pais',
		widgets={
			'persona':SelectWidget(attrs={'v-model':'persona'}),
			'estado':SelectWidget(attrs={'class':'select2',}),
		}

class Financialpartners_beneficiarieForm(forms.ModelForm):
	class Meta:
		model = Financialpartners_beneficiarie
		fields = 'name','relationship','percentage',

class BankForm(forms.ModelForm):
	class Meta:
		model = Bank
		fields = 'banco','no_cuenta','sucursal','clabe','no_cuenta_habiente'

class FileForm(forms.ModelForm):
	class Meta:
		model = File_user
		fields = 'code','file','name'

class InvestmentForm(forms.ModelForm):
	class Meta:
		model = Investment
		fields = 'tipo','importe','moneda','tasa','tiie','plazo','periodicidad','estatus','renovacion','fechaven','fechavencon','fechafirmacon','fechapagocap'
		widgets={
			'fechaven':MaterialDateWidget(attrs={'class':'date-picker','data-date-format':'d/m/Y'}),
			'fechavencon':MaterialDateWidget(attrs={'class':'date-picker','data-date-format':'d/m/Y'}),
			'fechafirmacon':MaterialDateWidget(attrs={'class':'date-picker','data-date-format':'d/m/Y'}),
			'fechapagocap':MaterialDateWidget(attrs={'class':'date-picker','data-date-format':'d/m/Y'}),
		}

class QEQForm(forms.ModelForm):
	class Meta:
		model = QEQ
		fields = 'tipo','rfc','name','first_name','last_name','curp','issste','imss','razon'
		widgets={
			'tipo':SelectWidget(attrs={'v-model':'tipo'}), #se quito el select2 porque no hace el cambio en vue
			'rfc':MaterialTextWidget(attrs={'v-model':'rfc'}),
			'name':MaterialTextWidget(attrs={'v-model':'name'}),
			'first_name':MaterialTextWidget(attrs={'v-model':'first_name'}),
			'last_name':MaterialTextWidget(attrs={'v-model':'last_name'}),
			'curp':MaterialTextWidget(attrs={'v-model':'curp'}),
			'issste':MaterialTextWidget(attrs={'v-model':'issste'}),
			'imss':MaterialTextWidget(attrs={'v-model':'imss'}),
			'razon':MaterialTextWidget(attrs={'v-model':'razon'}),
		}





