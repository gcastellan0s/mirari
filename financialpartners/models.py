# -*- encoding: utf-8 -*-
from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from config.var_local import VAR_LOCAL, VAR_GLOBAL
from mptt.models import MPTTModel, TreeForeignKey
from django.urls import reverse_lazy, reverse
from localflavor.mx.models import MXRFCField, MXZipCodeField, MXCURPField
from django.db.models.signals import m2m_changed
from django.utils import timezone
from invoice.models import FiscalData
from project.models import User, Address, Bank, File_user, History
import datetime
from django.utils.safestring import mark_safe

APP = "financialpartners"

class Clave_Actividad_Economica(models.Model):
	
	codigo = models.CharField('Código de producto o servicio',max_length=120)	
	nombre = models.CharField('Nombre de producto o servicio',max_length=255)

	class Meta:
		default_permissions = ()

	def __str__(self):
		return '[ {0} ] {1}'.format(self.codigo, self.nombre)

	@property
	def string(self):
		return '[ {0} ] {1}'.format(self.codigo, self.nombre)

VERBOSE_NAME = "Socio prestamista"
VERBOSE_NAME_PLURAL = "Socios prestamistas"
MODEL = "financialpartners"
GENDER_NEW = "un nuevo"
GENDER_THIS = "este"

class Financialpartners(User):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	actividad = models.ForeignKey('Clave_Actividad_Economica', on_delete=models.CASCADE, blank=True, null=True, help_text="Actividad económica del socio", verbose_name="")
	reference_number = models.CharField('Identificación del socio', max_length=250, blank=True, null=True, help_text="Si no cuenta con uno el sistema le asiganará un número")

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.visible_username

	def add_text(self):
		return "NUEVO "+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')+'?module=financialpartners'

	def get_absolute_url_add(self):
		return reverse(APP+':'+self.MODEL.lower()+'-add')+'?module=financialpartners'

	def get_absolute_url_detail(self):
		return reverse(APP+':'+self.MODEL.lower()+'-detail',kwargs={'pk': self.pk})+'?module=financialpartners'

	def get_absolute_url_update(self):
		return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})+'?module=financialpartners'

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})+'?module=financialpartners'

	def rfcs(self):
		p = ''
		for obj in self.my_fiscaldata().all():
			p += obj.rfc + ', '
		return p[0:len(p)-2]

	def string(self):
		return mark_safe("""
				{0} {1}<br > 
				Email:{2}<br /> 
				# Referencia: <strong>{7}</strong><br /> 
				Actividad comercial: {6} <br />
				Nacionalidad:{3}<br /> 
				Fecha de nacimiento: {4}<br /> 
				Género: {5} <br />  
			""".format(
				self.first_name, 
				self.last_name,
				self.email,
				self.country,
				self.birthday,
				self.gender,
				self.actividad,
				self.reference_number,
			)
		) 

	def my_address(self):
		return Address.objects.filter(user = self)

	def my_fiscaldata(self):
		return FiscalData.objects.filter(user = self)

	def my_financialpartners_beneficiaries(self):
		return Financialpartners_beneficiarie.objects.filter(user = self)

	def my_banks(self):
		return Bank.objects.filter(user = self)

	def my_files(self):
		return File_user.objects.filter(user = self, module__code='financialpartners')

	def my_investments(self):
		return Investment.objects.filter(user = self)

VERBOSE_NAME = "Beneficiario"
VERBOSE_NAME_PLURAL = "Beneficiarios"
MODEL = "Financialpartners_beneficiarie"
GENDER_NEW = "un nuevo"
GENDER_THIS = "este"

class Financialpartners_beneficiarie(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	user = models.ForeignKey('Financialpartners', on_delete=models.CASCADE)
	name = models.CharField('Nombre completo del beneficiario',max_length=250)
	relationship = models.CharField('Parentesco con el socio',max_length=250)
	percentage = models.IntegerField('Porcentaje %')

	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.name

	def string(self):
		return mark_safe('({0}) <strong>{1}</strong> con el {2}%'.format(self.relationship, self.name, self.percentage))


VERBOSE_NAME = "Inversion"
VERBOSE_NAME_PLURAL = "Inversiones"
MODEL = "investment"
GENDER_NEW = "una nueva"
GENDER_THIS = "esta"

class Investment(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	user = models.ForeignKey('Financialpartners', on_delete=models.CASCADE)
	creation_date = models.DateTimeField(auto_now_add=True)

	tipo = models.CharField('Tipo de inversión', max_length=250)
	importe = models.FloatField('Importe de la inversión')
	moneda = models.CharField('Tipo de cambio', max_length=250, default="MXN")
	tasa = models.FloatField('Tasa %')
	tiie = models.FloatField('Tiie %')
	plazo = models.IntegerField('Plazo de la inversión')
	fechaven = models.DateField('Fecha de vencimiento', blank=True, null=True)    
	fechavencon = models.DateField('Fecha de vencimiento del contrato', blank=True, null=True)   
	fechafirmacon = models.DateField('Fecha de confirmación del crédito', blank=True, null=True)   
	periodicidad = models.IntegerField('Peridicidad en meses')
	fechapagocap = models.DateField('Fecha pago a capital', blank=True, null=True)
	estatus = models.CharField('Estatus de la inversión', max_length=250, default="MXN")
	renovacion = models.DateField('Renovación', blank=True, null=True)

	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.name

	def string(self):
		return mark_safe('{0}'.format(self.pk,))



QEQ_PERSONA = (
	('Persona','Persona'),
	('Empresa','Empresa'),
)

VERBOSE_NAME = "Consulta QEQ"
VERBOSE_NAME_PLURAL = "Consultas QEQ"
MODEL = "qeq"
GENDER_NEW = "una nueva"
GENDER_THIS = "esta"

class QEQ(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	tipo = models.CharField(choices=QEQ_PERSONA, max_length=100, default='Persona', verbose_name='', help_text='Tipo de persona')
	rfc = MXRFCField(verbose_name="RFC", blank=True, null=True)
	name = models.CharField('Nombre de persona', max_length=100, blank=True, null=True)
	first_name = models.CharField('Apellido paterno', max_length=100, blank=True, null=True)
	last_name = models.CharField('Apellido materno', max_length=100, blank=True, null=True)
	curp = MXCURPField('C.U.R.P.', blank=True, null=True)
	issste = models.CharField('ISSSTE', max_length=100, blank=True, null=True)
	imss = models.CharField('IMSS', max_length=100, blank=True, null=True)
	razon = models.CharField('Razon social', max_length=100, blank=True, null=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Realiza consultas de '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.pk


class History_Financialpartners(History):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	financialpartners = models.ForeignKey('Financialpartners', on_delete=models.CASCADE)

	def __str__(self):
		return self.financialpartners.first_name


