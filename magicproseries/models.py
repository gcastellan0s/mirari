# -*- encoding: utf-8 -*-
from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from config.var_local import VAR_LOCAL, VAR_GLOBAL
from mptt.models import MPTTModel, TreeForeignKey
from django.urls import reverse_lazy, reverse
from django.db.models.signals import m2m_changed
from django.utils.safestring import mark_safe
from django.utils import timezone
from project.models import User, Company
import datetime
from django.utils.text import slugify

APP = "magicproseries"

VERBOSE_NAME = "Tienda"
VERBOSE_NAME_PLURAL = "Tiendas"
MODEL = "store"
NEW = "NUEVA"
GENDER_NEW = "una nueva"
GENDER_THIS = "esta"

class Store(Company):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	NEW = NEW
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	can_delete = models.BooleanField('Se puede borrar', default=True, help_text='Esta tienda se puede eliminar?')
	history = models.TextField('Historia de la tienda', blank=True, null=True)
	description = models.TextField('Descripción', blank=True, null=True)

	schedule_monday = models.CharField('Horario Lunes:',max_length=250, blank=True, null=True, help_text="ejemplo: 10:00 AM - 18:00 PM")
	schedule_thursday = models.CharField('Horario Martes:',max_length=250, blank=True, null=True, help_text="ejemplo: 10:00 AM - 18:00 PM")
	schedule_wednesday = models.CharField('Horario Miercoles:',max_length=250, blank=True, null=True, help_text="ejemplo: 10:00 AM - 18:00 PM")
	schedule_tuesday = models.CharField('Horario Jueves:',max_length=250, blank=True, null=True, help_text="ejemplo: 10:00 AM - 18:00 PM")
	schedule_friday = models.CharField('Horario Viernes:',max_length=250, blank=True, null=True, help_text="ejemplo: 10:00 AM - 18:00 PM")
	schedule_saturday = models.CharField('Horario Säbado:',max_length=250, blank=True, null=True, help_text="ejemplo: 10:00 AM - 18:00 PM")
	schedule_sunday = models.CharField('Horario Domingo:',max_length=250, blank=True, null=True, help_text="ejemplo: 10:00 AM - 18:00 PM")

	phone = models.CharField('Teléfono a 10 dígitos', max_length=250, blank=True, null=True, help_text="ejemplo: (55) 5555 5555")

	address = models.CharField('',max_length=255, blank=True, null=True, help_text="Dirección")
	lat = models.CharField('', max_length=50, blank=True, null=True)
	lng = models.CharField('', max_length=50, blank=True, null=True)

	indications = models.CharField('Otras indicaciones', max_length=255, blank=True, null=True)
	facebook = models.CharField('Página de Facebook', max_length=255, blank=True, null=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.name

	def add_text(self):
		return NEW + ' '+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')

	def get_absolute_url_add(self):
		return reverse(APP+':'+self.MODEL.lower()+'-add')

	def get_absolute_url_detail(self):
		return reverse(APP+':'+self.MODEL.lower()+'-detail',kwargs={'pk': self.pk})

	def get_absolute_url_update(self):
		return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})

	def get_photos(self):
		return Event_gallery.objects.filter(event__store = self)



VERBOSE_NAME = "Usuario"
VERBOSE_NAME_PLURAL = "Usuarios"
MODEL = "mps_user"
NEW = "NUEVO"
GENDER_NEW = "un nuevo"
GENDER_THIS = "este"

class MPS_Player(User):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	NEW = NEW
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	dci = models.CharField('DCI',max_length=250, blank=True, null=True)
	mps_country = models.CharField('Country', max_length=250, blank=True, null=True)
	points = models.IntegerField(blank=True, null=True)
	general_points = models.IntegerField(blank=True, null=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.username

	def add_text(self):
		return NEW + ' '+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')

	def get_absolute_url_add(self):
		return reverse(APP+':'+self.MODEL.lower()+'-add')

	def get_absolute_url_detail(self):
		return reverse(APP+':'+self.MODEL.lower()+'-detail',kwargs={'pk': self.pk})

	def get_absolute_url_update(self):
		return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})


VERBOSE_NAME = "Temporadas"
VERBOSE_NAME_PLURAL = "Temporada"
MODEL = "mps_user"
NEW = "NUEVO"
GENDER_NEW = "un nuevo"
GENDER_THIS = "este"

class Seasson(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	NEW = NEW
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	name = models.CharField('Nombre',max_length=250)
	code = models.CharField('Codigo',max_length=250)
	datetime = models.DateTimeField(auto_now_add=True)
	is_active = models.BooleanField(default=True)

	active = models.BooleanField(default=True)


	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.name

	def add_text(self):
		return NEW + ' '+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')

	def get_absolute_url_add(self):
		return reverse(APP+':'+self.MODEL.lower()+'-add')

	def get_absolute_url_detail(self):
		return reverse(APP+':'+self.MODEL.lower()+'-detail',kwargs={'pk': self.pk})

	def get_absolute_url_update(self):
		return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})

	def get_seasson(self):
		return Seasson.objects.filter(is_active = True).first()



VERBOSE_NAME = "Evento"
VERBOSE_NAME_PLURAL = "Eventos"
MODEL = "event"
NEW = "NUEVO"
GENDER_NEW = "un nuevo"
GENDER_THIS = "este"

EVENT_TYPE_MPS = (
	('T2','Tipo 2'),
	('MD','Moderno'),
	('DR','Draft'),
	('CM','Commander'),
)

def path_cover(self, filename):
	date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
	upload_to = "companys/%s_%s/events/%s__%s/%s__%s" % (self.store.id, self.store.slug, self.id, self.slug, date, filename)
	return upload_to

def path_cer(self, filename):
	date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
	upload_to = "companys/%s_%s/events/%s__%s/%s__%s" % (self.store.id, self.store.slug, self.id, self.slug, date, filename)
	return upload_to

class Event(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	NEW = NEW
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	store = models.ForeignKey('Store', on_delete=models.CASCADE, related_name='+',)
	user = models.ForeignKey('project.User', on_delete=models.CASCADE, related_name='+',)
	seasson = models.ForeignKey('Seasson', on_delete=models.CASCADE, related_name='+',)

	slug = models.SlugField()
	event_type = models.CharField('Formato del evento', choices=EVENT_TYPE_MPS, max_length=250, default='T2')
	cover = ProcessedImageField(blank=True, null=True, upload_to=path_cover, processors=[ResizeToFill(820, 312)],format='JPEG',options={'quality': 60}, verbose_name="", help_text="Portada de la tienda 820x312")
	name = models.CharField('Nombre del evento', max_length=50)
	address = models.CharField('', max_length=255, blank=True, null=True, help_text="Dirección del evento, <strong>Dejala vacia si es la misma que esta tienda.</strong>")
	lat = models.CharField('', max_length=50, blank=True, null=True)
	lng = models.CharField('', max_length=50, blank=True, null=True)
	create = models.DateField(auto_now_add=True)
	date = models.DateField('Fecha de inicio')
	time = models.TimeField('Hora de inicio')
	prizes = models.TextField('', help_text="Indica los premios para este evento", default="El ganador acumula puntos para calificar a la final de temporada por un premio de $ 50,000.00 MXN en efectivo.")
	price = models.IntegerField('Precio del evento', default=0)
	description = models.TextField('', blank=True, null=True, help_text="Descripción del evento")
	views = models.IntegerField(default=0)

	wer = models.FileField(blank=True, null=True, upload_to=path_cer)
	wertext = models.TextField(blank=True, null=True)

	facebook = models.CharField('Ingresa la URL a tu evento en facebook', max_length=250, blank=True, null=True)

	is_active = models.BooleanField('Esta activo?', default=True, help_text='Desactivar producto?')
	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super().save()

	def add_text(self):
		return NEW + ' '+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')

	def get_absolute_url_add(self):
		return reverse(APP+':'+self.MODEL.lower()+'-add')

	def get_absolute_url_detail(self):
		return reverse(APP+':'+self.MODEL.lower()+'-detail',kwargs={'pk': self.pk, 'slug':self.slug})

	def get_absolute_url_update(self):
		return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})

	def get_photos(self):
		return Event_gallery.objects.filter(event = self)

	def event_type_(self):
		if self.event_type == 'T2':
			return 'ESTANDAR'
		elif self.event_type == 'MD':
			return 'MODERNO'
		elif self.event_type == 'DR':
			return 'DARFT'
		elif self.event_type == 'CM':
			return 'COMMANDER'

	def address_(self):
		if self.address:
			return self.address
		else:
			return self.store.address

	def lat_(self):
		if self.lat:
			return self.lat
		else:
			return self.store.lat

	def lng_(self):
		if self.lng:
			return self.lng
		else:
			return self.store.lng

	def players(self):
		return len(Positions.objects.filter(event=self))


VERBOSE_NAME = "Foto de evento"
VERBOSE_NAME_PLURAL = "Fotos de evento"
MODEL = "event_gallery"
NEW = "SUBIR"
GENDER_NEW = "subir nueva"
GENDER_THIS = "este"


def path_event_gallery(self, filename):
	date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
	upload_to = "companys/%s_%s/events/%s__%s/%s__%s" % (self.event.store.id, self.event.store.slug, self.event.id, self.event.slug, date, filename)
	return upload_to

class Event_gallery(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	NEW = NEW
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	event = models.ForeignKey('Event', on_delete=models.CASCADE, related_name='+',)
	photo = ProcessedImageField(blank=True, null=True, upload_to=path_event_gallery, processors=[ResizeToFill(800, 800)], format='JPEG',options={'quality': 60}, verbose_name="")

	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return str(self.id)

	def add_text(self):
		return NEW + ' '+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})



VERBOSE_NAME = "Posicion"
VERBOSE_NAME_PLURAL = "Posiciones"
MODEL = "positions"
NEW = "NUEVAS"
GENDER_NEW = "una nueva"
GENDER_THIS = "esta"

class Positions(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	NEW = NEW
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	event = models.ForeignKey('Event', on_delete=models.CASCADE, related_name='+',)
	player = models.ForeignKey('MPS_Player', on_delete=models.CASCADE, related_name='+')
	position = models.IntegerField(default=0)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.player.dci

	def add_text(self):
		return NEW + ' '+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')

	def get_absolute_url_add(self):
		return reverse(APP+':'+self.MODEL.lower()+'-add')

	def get_absolute_url_detail(self):
		return reverse(APP+':'+self.MODEL.lower()+'-detail',kwargs={'pk': self.pk})

	def get_absolute_url_update(self):
		return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})

	def color(self):
		if self.position == 1:
			return 'gold'
		elif self.position == 2:
			return 'silver'
		elif self.position == 3:
			return 'bronce'
		else:
			return ''

	def points(self):
		if self.position == 1:
			return '4'
		elif self.position == 2:
			return '3'
		elif self.position == 3:
			return '2'
		else:
			return '1'


#trash functions
def path_logo_store(self, filename):
	return True

def path_logo_cover(self, filename):
	return True
