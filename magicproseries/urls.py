from django.urls import path
from .views import *

app_name = "magicproseries"

urlpatterns = [

	path('mps/', index, name='index'),

	path('tiendas/', StoreList.as_view(), name='store-list'),
	path('tienda/agregar/', StoreCreate.as_view(), name='store-add'),
	path('tienda/actualizar/<int:pk>/', StoreUpdate.as_view(), name='store-update'),
	path('tienda/eliminar/<int:pk>/', StoreDelete.as_view(), name='store-delete'),
	path('tienda/<int:pk>/', StoreDetail.as_view(), name='store-detail'),

	path('eventos/', EventList.as_view(), name='event-list'),
	path('evento/agregar/', EventCreate.as_view(), name='event-add'),
	path('evento/actualizar/<int:pk>/', EventUpdate.as_view(), name='event-update'),
	path('evento/eliminar/<int:pk>/', EventDelete.as_view(), name='event-delete'),
	path('evento/<int:pk>/<slug:slug>/', EventDetail.as_view(), name='event-detail'),

	path('evento/fotos/', Event_galleryList.as_view(), name='event_gallery-list'),
	path('evento/foto/eliminar/<int:pk>/', Event_galleryDelete.as_view(), name='event_gallery-delete'),

	path('mps-api/', mps_api, name='mps_api'),

]
