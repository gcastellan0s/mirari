# -*- encoding: utf-8 -*-
from django import forms
from django.forms import widgets
from project.forms import MaterialCheckboxWidget, MaterialDateWidget, MaterialTextWidget, SelectWidget, SelectMultipleWidget, MaterialColorWidget, BaseUserForm, MaterialDateWidget, WYSIWYGWidget
from .models import *

class StoreForm(forms.ModelForm):
	class Meta:
		model = Store
		fields = ('name','address','lat','lng','indications','phone','default_mail','contact_mail','facebook','schedule_monday','schedule_thursday','schedule_wednesday','schedule_tuesday','schedule_friday','schedule_saturday','schedule_sunday','description','history',)
		labels = {
			'name':'Nombre de la tienda',
			'default_mail':'Correo de notificaciones',
			'contact_mail':'Correo de contacto para clientes',
		}
		widgets={
			'name':MaterialTextWidget(attrs={'class':'form-control', 'required':'required'}),
			'address':MaterialTextWidget(attrs={'class':'form-control','required':'required'}),
			'lat':MaterialTextWidget(attrs={'class':'form-control','v-model':'lat'}),
			'lng':MaterialTextWidget(attrs={'class':'form-control','v-model':'lng'}),
		}

class Store_logo_Form(forms.ModelForm):
	class Meta:
		model = Store
		fields = ('logo',)

class Store_cover_Form(forms.ModelForm):
	class Meta:
		model = Store
		fields = ('cover',)

class EventForm(forms.ModelForm):
	class Meta:
		model = Event
		fields = ('name','address','lat','lng','date','time','event_type','price','facebook','description','prizes')
		widgets={
			'name':MaterialTextWidget(attrs={'class':'form-control', 'required':'required'}),
			'address':MaterialTextWidget(attrs={'class':'form-control','required':'required'}),
			'lat':MaterialTextWidget(attrs={'class':'form-control','v-model':'lat'}),
			'lng':MaterialTextWidget(attrs={'class':'form-control','v-model':'lng'}),
			'description':WYSIWYGWidget(attrs={'style':'display:none'}),
			'event_type':SelectWidget(attrs={'class':'select2',}),
		}

class EventFileWERForm(forms.ModelForm):
	class Meta:
		model = Event
		fields = ('wer',)

class Event_galleryForm(forms.ModelForm):
	class Meta:
		model = Event_gallery
		fields = ('photo',)
