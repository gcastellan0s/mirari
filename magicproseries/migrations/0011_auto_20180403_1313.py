# Generated by Django 2.0.1 on 2018-04-03 18:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('magicproseries', '0010_event_prizes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='prizes',
            field=models.TextField(default='El ganador acumula puntos para calificar a la final de temporada por un premio de $ 50,000.00 MXN en efectivo.', help_text='Indica los premios para este evento', verbose_name=''),
        ),
    ]
