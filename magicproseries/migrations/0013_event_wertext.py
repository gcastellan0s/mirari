# Generated by Django 2.0.1 on 2018-04-05 05:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('magicproseries', '0012_auto_20180403_1501'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='wertext',
            field=models.TextField(blank=True, null=True),
        ),
    ]
