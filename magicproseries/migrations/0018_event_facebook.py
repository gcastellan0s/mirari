# Generated by Django 2.0.1 on 2018-04-05 20:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('magicproseries', '0017_auto_20180405_1455'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='facebook',
            field=models.CharField(blank=True, max_length=250, null=True, verbose_name='Ingresa la URL a tu evento en facebook'),
        ),
    ]
