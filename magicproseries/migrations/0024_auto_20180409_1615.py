# Generated by Django 2.0.1 on 2018-04-09 21:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('magicproseries', '0023_auto_20180409_1505'),
    ]

    operations = [
        migrations.AddField(
            model_name='mps_player',
            name='general_points',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='mps_player',
            name='points',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
