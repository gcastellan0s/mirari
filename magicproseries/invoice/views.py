from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import SingleObjectMixin, DetailView
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.db.models import Q
from django.urls import reverse
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.contrib import messages
from project.views import log_variables, variables, check_permissions
from django.core.exceptions import PermissionDenied
from datetime import datetime
import json
from django.conf import settings
from project.models import *

from .models import *
from .forms import *
from .serializers import *

#############################################################	
# FiscalData
##############################################################
######### LIST
class FiscalDataList(ListView):
	model, m = FiscalData, 'fiscaldata'
	template_name = m+'_list.html'
	paginate_by = 30

	def get_queryset(self):
		self.VAR, self.q = log_variables(self.request, module=self.request.resolver_match.app_name), self.request.GET.get('q')
		check_permissions(request=self.request, permission='can_view_'+self.m)
		query = self.model.objects.filter(company=self.VAR['COMPANY'], active=True)
		if self.q:
			query.objects.filter(rfc__icontains=self.q)
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		return context

######### DETAIL
class FiscalDataDetail(DetailView):
	model, m = FiscalData, 'fiscaldata'
	template_name = m+'_detail.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name)
		check_permissions(request=self.request, permission='can_view_'+self.m)
		return context

def get_nocertificado(path_cer):
	nocertificado = os.popen('openssl x509 -inform DER -in '+path_cer+' -noout -serial').readlines()[0].replace("serial=","").replace("\n","")
	result = "" 
	for i in range(len(nocertificado)):
		if i % 2 != 0:
			result = result + nocertificado[i]
	return result

######### CREATE
class FiscalDataCreate(CreateView):
	model, m = FiscalData, 'fiscaldata'
	template_name = m+'_create.html'
	form_class = FiscalDataForm

	def get_form_kwargs(self):
		company = Company.objects.get(id=self.request.session.get('company'))
		kwargs = super(FiscalDataCreate, self).get_form_kwargs()
		return kwargs

	def get_success_url(self):
		return reverse('invoice:'+self.m+'-detail', kwargs={'pk': self.object.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
		ctx = super().form_valid(form)
		self.object.save_certs('add')
		return ctx

######### UPDATE
class FiscalDataUpdate(UpdateView):
	model, m = FiscalData, 'fiscaldata'
	template_name = m+'_create.html'
	form_class = FiscalDataForm

	def get_form_kwargs(self):
		kwargs = super(FiscalDataUpdate, self).get_form_kwargs()
		return kwargs

	def get_success_url(self):
		return reverse('invoice:'+self.m+'-detail', kwargs={'pk': self.object.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
		return context

	def form_valid(self, form):
		ctx = super().form_valid(form)
		self.object.save_certs('update')
		return ctx

######### DELETE
class FiscalDataDelete(DeleteView):
	model, m = FiscalData, 'fiscaldata'

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('invoice:'+self.m+'-list')
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())

def invoice_ticket(request):
	from sellpoint.models import Ticket
	VAR = variables(request)
	if request.method == "POST":
		api = False
		if 'folio' in request.POST:
			ticket = Ticket.objects.filter(barcode= request.POST.get('folio'), secret_code= request.POST.get('key'), cut__invoice__isnull=True, status='COB', invoice=False).first()
			if not ticket:
				message = 'No encontramos este ticket en el sistema!'
			else:
				response = ticket.make_cfdi(email=request.POST.get('email'),  rfc=request.POST.get('rfc'), name=request.POST.get('name'))
				api = {'xml':response['xml'],'uuid':response['uuid']}
				message = 'Tu descarga comenzará en breve.'
		return JsonResponse({
					'message':message,
					'api' : api,
				})
	return render(request, 'invoice_ticket.html',locals())

def cfdi2(request):
	return render(request, 'cfdi.html',locals())

