# -*- encoding: utf-8 -*-
from django import forms
from django.forms import widgets
from project.forms import MaterialCheckboxWidget, MaterialDateWidget, MaterialTextWidget, SelectWidget, SelectMultipleWidget, MaterialColorWidget
from .models import *

class FiscalDataForm(forms.ModelForm):
	class Meta:
		model = FiscalData
		fields = ('rfc','razon_social','persona','password','cer','key','curp','telefono','contacto','email_contacto','calle','numero_exterior','numero_interior','colonia','localidad','municipio_delegacion','estado','codigo_postal',)
		widgets={
			'estado':SelectWidget(attrs={'class':'select2',}),
			'persona':SelectWidget(attrs={'class':'select2','data-minimum-results-for-search':'Infinity'}),
			'password':MaterialTextWidget(attrs={'class':'form-control','required':'required','type':'password'}),
		}
