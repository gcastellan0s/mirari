from django.contrib.auth.admin import UserAdmin
from django.contrib import admin
from .models import *

class PassAdmin(admin.ModelAdmin):
	pass

@admin.register(MPS_Player)
class MPS_PlayerAdmin(admin.ModelAdmin):
	actions_on_top = True
	actions_on_bottom = False
	list_display = ('username', 'dci','points','general_points')
	search_fields = ('username','dci')

@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
	actions_on_top = True
	actions_on_bottom = False
	list_display = ('name', 'parent')
	search_fields = ('name','code')

@admin.register(Seasson)
class SeassonAdmin(admin.ModelAdmin):
	actions_on_top = True
	actions_on_bottom = False
	list_display = ('name', 'code', 'datetime','active')
	search_fields = ('name','code')

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
	actions_on_top = True
	actions_on_bottom = False

@admin.register(Event_gallery)
class Event_galleryAdmin(admin.ModelAdmin):
	actions_on_top = True
	actions_on_bottom = False

@admin.register(Positions)
class PositionsAdmin(admin.ModelAdmin):
	list_display = ('event', 'player', 'position')
	actions_on_top = True
	actions_on_bottom = False
