from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
from django.views.generic.detail import SingleObjectMixin, DetailView
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.shortcuts import render
from django.db.models import Q
from django.urls import reverse
from django.contrib import messages
from project.views import log_variables, variables, check_permissions
from project.models import *
from project.forms import *
from itertools import chain
import json
import sys

from io import StringIO, BytesIO
from lxml import etree
import xmltodict

from .models import *
from .forms import *
from .serializers import *

def index(request):
	VAR = log_variables(request)
	return render(request, 'index.html',locals())



##############################################################
# MAGICPROSERIES
##############################################################
######### LIST
class StoreList(ListView):
	model = Store
	m = model.MODEL
	template_name = m+'_list.html'
	paginate_by = 30

	def get_queryset(self):
		self.VAR, self.q = log_variables(self.request, module=self.request.resolver_match.app_name), self.request.GET.get('q')
		check_permissions(request=self.request, permission='can_view_'+self.m)
		query = self.request.user.company.get_descendants(include_self=True).filter(active=True)
		if self.q:
			query = query.filter(Q(name__icontains=self.q)|Q(default_mail__icontains=self.q)|Q(contact_mail__icontains=self.q))
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		context['form_logo'] = Store_logo_Form()
		context['form_cover'] = Store_cover_Form()
		return context

######### CREATE
class StoreCreate(CreateView):
	model = Store
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = StoreForm

	def get_success_url(self):
		return reverse(self.model.APP+':'+self.m+'-list')

	def form_valid(self, form):
		company = get_object_or_404(Company, pk=self.request.session.get('company'))
		master = company.get_root()
		form.instance.parent = company
		form.instance.slogan = master.slogan
		form.instance.web = master.web
		form.instance.domain = master.domain
		form.instance.payment_date = master.payment_date
		form.instance.ip = master.ip
		obj = form.save(commit=False)
		obj.save()
		obj.code = str( obj.pk ) +'_'+ master.code
		obj.save()
		for module in company.modules.all():
			obj.modules.add(module)
		return super().form_valid(form)

	def get_context_data(self, **kwargs):
		self.VAR = log_variables(self.request)
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, self.VAR
		context['class'] = 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

######### UPDATE
class StoreUpdate(UpdateView):
	model = Store
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = StoreForm

	def get_success_url(self):
		return reverse(self.model.APP+':'+self.m+'-list')

	def get_context_data(self, **kwargs):
		self.VAR = log_variables(self.request)
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, self.VAR
		context['class'] = 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object)
		return context

######### DETAIL
class StoreDetail(DetailView):
	model = Store
	m = model.MODEL
	template_name = m + '_detail.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, variables(self.request)
		query_positions = Positions.objects.filter(event__store=self.object).order_by('position')
		dict_positions = {}
		for position in query_positions:
			if position.player.dci in dict_positions:
				dict_positions[position.player.dci]['points'] += int(position.points())
			else:
				dict_positions.update({
					position.player.dci:{
						'player':position.player,
						'points':int(position.points()),
					}
				})
		context['positions'] = sorted(dict_positions.items(), key = lambda x: x[1]['points'], reverse = True)
		return context

######### DELETE
class StoreDelete(DeleteView):
	model = Store
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse(self.model.APP+':'+self.m+'-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())



##############################################################
# EVENT
##############################################################
######### LIST
class EventList(ListView):
	model = Event
	m = model.MODEL
	template_name = m+'_list.html'
	paginate_by = 30

	def get_queryset(self):
		self.VAR, self.q = log_variables(self.request, module=self.request.resolver_match.app_name), self.request.GET.get('q')
		check_permissions(request=self.request, permission='can_view_'+self.m)
		company = get_object_or_404(Company, pk=self.request.session.get('company'))
		query = self.model.objects.filter(active=True, store=company).order_by('-date')
		if self.q:
			query = query.filter(Q(name__icontains=self.q))
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		context['form_wer'] = EventFileWERForm()
		return context

######### CREATE
class EventCreate(CreateView):
	model = Event
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = EventForm

	def get_success_url(self):
		return reverse(self.model.APP+':'+self.m+'-list')

	def form_valid(self, form):
		form.instance.store = Store.objects.get(id=self.request.session.get('company'))
		form.instance.user = self.request.user
		form.instance.seasson = Seasson().get_seasson()
		return super().form_valid(form)

	def get_context_data(self, **kwargs):
		self.VAR = log_variables(self.request)
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, self.VAR
		context['class'] = 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

######### UPDATE
class EventUpdate(UpdateView):
	model = Event
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = EventForm

	def get_success_url(self):
		return reverse(self.model.APP+':'+self.m+'-list')

	def get_context_data(self, **kwargs):
		self.VAR = log_variables(self.request)
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, self.VAR
		context['class'] = 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object)
		return context

######### DETAIL
class EventDetail(DetailView):
	model = Event
	m = model.MODEL
	template_name = m + '_detail.html'

	def get_context_data(self, **kwargs):
		self.object.views = self.object.views + 1
		self.object.save()
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, variables(self.request)
		context['form'] = EventFileWERForm()
		context['positions'] = Positions.objects.filter(event=self.object).order_by('position')
		return context

######### DELETE
class EventDelete(DeleteView):
	model = Event
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse(self.model.APP+':'+self.m+'-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object)
		self.object.save()
		messages.error(self.request, 'Aun no se pueden eliminar las organizaciones en el sistema')
		return HttpResponseRedirect(self.get_success_url())



##############################################################
# Event_gallery
##############################################################
######### LIST
class Event_galleryList(ListView):
	model = Event_gallery
	m = model.MODEL
	template_name = m+'_list.html'
	paginate_by = 30

	def get_queryset(self):
		self.VAR, self.q = log_variables(self.request, module=self.request.resolver_match.app_name), self.request.GET.get('q')
		check_permissions(request=self.request, permission='can_view_'+self.m)
		self.event = get_object_or_404(Event, pk=self.request.GET.get('id'), slug=self.request.GET.get('slug'))
		query = self.model.objects.filter(event=self.event, active = True).order_by('-id')
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		context['form_photo'] = Event_galleryForm()
		context['event'] = self.event
		context['posturl'] = 'id='+str(self.event.id)+'&slug='+str(self.event.slug)
		return context

######### DELETE
class Event_galleryDelete(DeleteView):
	model = Event_gallery
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		self.event = get_object_or_404(Event, pk=self.request.GET.get('id'), slug=self.request.GET.get('slug'))
		return reverse(self.model.APP+':'+self.m+'-list')+'?id='+str(self.event.id)+'&slug='+str(self.event.slug)

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())

@csrf_exempt
def mps_api(request):
	method = request.GET.get('method')
	api = False
	message = ''
	try:
		if method == 'form_wer':
			event = get_object_or_404(Event, pk=request.GET.get('event'))
			form = EventFileWERForm(instance=event, data=request.POST, files=request.FILES)
			if form.is_valid():
				jsonString = json.dumps(xmltodict.parse(request.FILES['wer'].read()), indent=4)
				event = form.save(commit=False)
				event.wertext = jsonString.replace('@','')
				event.save()
				json_wer = json.loads(event.wertext)
				if 'eventupload' in json_wer:
					json_wer = json_wer['eventupload']
				for player in json_wer['event']['participation']['person']:
					p = MPS_Player.objects.filter(dci = player['id']).first()
					company = request.user.company.get_root()
					if not p:
						p = MPS_Player()
						p.company = company
						p.dci = player['id']
						p.first_name = player['first']
						p.last_name = player['last']
						p.mps_country = player['country']
						p.username = (company.code.upper() + '_' + player['id'])
						p.visible_username = (company.code.upper() + '_' + player['id'])
						p.set_password(player['id'])
						p.save()
				Positions.objects.filter(event = event).delete()
				for player in json_wer['event']['participation']['role'][0]['ref']:
					positions = Positions()
					positions.event = event
					positions.player = MPS_Player.objects.get(dci = player['person'])
					positions.position = int(player['seq'])
					positions.save()
				players = MPS_Player.objects.all()
				for player in players:
					t = 0
					for position in Positions.objects.filter(player = player).all():
						if position.position == 1:
							t+=4
						elif position.position == 2:
							t+=3
						elif position.position == 3:
							t+=2
						else:
							t+=1
					player.points = t
					player.general_points = t
					player.save()
				api = True
			return JsonResponse({
				'api' : api,
				'message' : message,
			})
		if method == 'form_photo':
			event = get_object_or_404(Event, pk=request.GET.get('event'))
			form = Event_galleryForm(data=request.POST, files=request.FILES)
			if form.is_valid():
				event_gallery = form.save(commit=False)
				event_gallery.event = event
				event_gallery.save()
				api = True
			return JsonResponse({
				'api' : api,
				'message' : message,
			})
		if method == 'form_logo':
			store = get_object_or_404(Store, pk=request.GET.get('store'))
			form = Store_logo_Form(instance=store, data=request.POST, files=request.FILES)
			if form.is_valid():
				store = form.save()
				api = store.logo.url
			return JsonResponse({
				'api' : api,
				'message' : message,
			})
		if method == 'form_cover':
			store = get_object_or_404(Store, pk=request.GET.get('store'))
			form = Store_cover_Form(instance=store, data=request.POST, files=request.FILES)
			if form.is_valid():
				store = form.save()
				api = store.logo.url
			return JsonResponse({
				'api' : api,
				'message' : message,
			})
	except:
		exc_type, exc_value, exc_traceback = exc_tuple = sys.exc_info()
		return JsonResponse({
			'message': 'Por favor contacte al administrador del sistema con este error: ' + str(exc_value) + ' in: ' + str(exc_traceback.tb_lineno) + ' type: ' + str(exc_type.__name__),
			'api' : False,
		})
