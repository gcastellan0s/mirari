# -*- encoding: utf-8 -*-
from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from config.var_local import VAR_LOCAL, VAR_GLOBAL
from mptt.models import MPTTModel, TreeForeignKey
from django.urls import reverse_lazy, reverse
from localflavor.mx.models import MXRFCField, MXZipCodeField, MXCURPField
import datetime
from django.db.models.signals import m2m_changed
from djmoney.money import Money
from django.utils import timezone
from django.conf import settings
import OpenSSL.crypto
from lxml.etree import Element, SubElement, QName, tostring, XML, XMLParser, fromstring
import xmltodict
from django.template.loader import render_to_string
from numbertoletters import number_to_letters
from django.utils.safestring import mark_safe
import os
import boto3
import zeep
import pdfkit
import json
from django.core.mail import EmailMultiAlternatives
from django.template import Context, Template
from django.template.loader import get_template

APP = "invoice"

PERSONA = (
    ('Física','Física'),
    ('Moral','Moral'),
)

STATE_CHOICES = (
    ('Aguascalientes', ('Aguascalientes')),
    ('Baja California', ('Baja California')),
    ('Baja California Sur', ('Baja California Sur')),
    ('Campeche', ('Campeche')),
    ('Chihuahua', ('Chihuahua')),
    ('Chiapas', ('Chiapas')),
    ('Coahuila', ('Coahuila')),
    ('Colima', ('Colima')),
    ('CDMX', ('CDMX')),
    ('Durango', ('Durango')),
    ('Guerrero', ('Guerrero')),
    ('Guanajuato', ('Guanajuato')),
    ('Hidalgo', ('Hidalgo')),
    ('Jalisco', ('Jalisco')),
    ('Estado de México', ('Estado de México')),
    ('Michoacán', ('Michoacán')),
    ('Morelos', ('Morelos')),
    ('Nayarit', ('Nayarit')),
    ('Nuevo León', ('Nuevo León')),
    ('Oaxaca', ('Oaxaca')),
    ('Puebla', ('Puebla')),
    ('Querétaro', ('Querétaro')),
    ('Quintana Roo', ('Quintana Roo')),
    ('Sinaloa', ('Sinaloa')),
    ('San Luis Potosí', ('San Luis Potosí')),
    ('Sonora', ('Sonora')),
    ('Tabasco', ('Tabasco')),
    ('Tamaulipas', ('Tamaulipas')),
    ('Tlaxcala', ('Tlaxcala')),
    ('Veracruz', ('Veracruz')),
    ('Yucatán', ('Yucatán')),
    ('Zacatecas', ('Zacatecas')),
)

FiscalData_estatus = (
    ('success','Bien'),
    ('danger','Error'),
    ('warning','Advertencia'),
)

FormaPago = (
    ('01','Efectivo'),
    ('02','Cheque nominativo'),
    ('03','Transferencia electrónica de fondos'),
)

RegimenFiscal = (
    ('601','General de Ley Personas Morales'),
    ('603','Personas Morales con Fines no Lucrativos'),
    ('605','Sueldos y Salarios e Ingresos Asimilados'),
)

def path_cfdi_company(self, filename):
    date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    file_name = filename.split(".")
    if settings.VAR_PRODUCTION['PRODUCTION']:
        upload_to = "companys/%s_%s/certificados/%s/%s.%s" % (self.company.id, self.company.name, self.rfc, self.rfc, file_name[len(file_name)-1])
    else:
        upload_to = settings.MEDIA_URL+"companys/%s_%s/certificados/%s/%s.%s" % (self.company.id, self.company.name, self.rfc, self.rfc, file_name[len(file_name)-1])
    return upload_to

VERBOSE_NAME = "Información fiscal"
VERBOSE_NAME_PLURAL = "Datos fiscales"
MODEL = "FiscalData"
GENDER_NEW = "nueva"
GENDER_THIS = "este"

class FiscalData(models.Model):

    VERBOSE_NAME = VERBOSE_NAME
    VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
    MODEL = MODEL
    GENDER_NEW = GENDER_NEW
    GENDER_THIS = GENDER_THIS
    APP = APP

    company = models.ForeignKey('project.Company', related_name='+', on_delete=models.CASCADE, verbose_name="")
    user = models.ForeignKey('project.User', related_name='+', on_delete=models.CASCADE, verbose_name="", blank=True, null=True)

    rfc = MXRFCField(verbose_name="RFC", blank=True, null=True)
    razon_social = models.CharField(verbose_name='', max_length=255, blank=True, null=True, help_text="Razon social de persona Física o Moral")
    persona = models.CharField(choices=PERSONA, max_length=100, blank=True, null=True, verbose_name='', help_text='Tipo de persona', default='Física')
    curp = MXCURPField('C.U.R.P.', blank=True, null=True)
    telefono = models.CharField('Teléfono de contacto', max_length=100, blank=True, null=True)
    contacto = models.CharField('Nombre completo del contacto', max_length=255, blank=True, null=True)
    email_contacto = models.EmailField('Email del contacto', max_length=100, blank=True, null=True)
    calle = models.CharField(max_length=255, blank=True, null=True)
    numero_exterior = models.CharField(max_length=150, blank=True, null=True)
    numero_interior = models.CharField(max_length=150, blank=True, null=True)
    colonia = models.CharField(max_length=255, blank=True, null=True)
    localidad = models.CharField(max_length=255, blank=True, null=True)
    municipio_delegacion = models.CharField('Municipio o Delegación', max_length=150, blank=True, null=True)
    estado = models.CharField(choices=STATE_CHOICES, max_length=100, blank=True, null=True, verbose_name='', help_text='Estado')
    codigo_postal = MXZipCodeField('Código Postal', blank=True, null=True)
    pais = models.CharField(max_length=100, default='México')

    cer = models.FileField('',blank=True, null=True, help_text='.cer certificado generado atravez del CSD', upload_to=path_cfdi_company)
    key = models.FileField('',blank=True, null=True, help_text='.key llave generada atravez del CSD', upload_to=path_cfdi_company)
    password = models.CharField('', blank=True, null=True, max_length=50, help_text="Contraseña de la llave privada")

    nocertificado = models.CharField('', blank=True, null=True, max_length=50, help_text="Numero de certificado")

    estatus = models.CharField(choices=FiscalData_estatus, max_length=255, blank=True, null=True)

    active = models.BooleanField(default=True)

    class Meta:
        default_permissions = ()
        verbose_name = VERBOSE_NAME
        verbose_name_plural = VERBOSE_NAME_PLURAL
        permissions = (
            ('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
            ('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
            ('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
            ('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
        )

    def __str__(self):
        return self.rfc

    def string(self):
        return mark_safe("""
            {0} {1} <br />
            <strong>Persona {2}</strong> Estatus: 
            """.format(self.rfc, self.razon_social, self.persona))

    def save_certs(self, action):
        if settings.VAR_PRODUCTION['PRODUCTION']:
            s3 = boto3.resource('s3', aws_access_key_id=settings.VAR_PRODUCTION['VAR_DJANGO_AWS_ACCESS_KEY_ID'], aws_secret_access_key=settings.VAR_PRODUCTION['VAR_AWS_SECRET_ACCESS_KEY'])
            bucket = s3.Bucket(settings.VAR_PRODUCTION['VAR_AWS_STORAGE_BUCKET_NAME'])
            directory = "invoice/certificados/%s_%s/%s/" % (self.company.id, self.company.name, self.rfc)
            if not os.path.exists(directory):
                os.makedirs(directory)
            bucket.download_file('media/'+self.cer.name, directory+self.rfc+'.cer')
            bucket.download_file('media/'+self.key.name, directory+self.rfc+'.key')
        else:
            directory = "media/companys/%s_%s/certificados/%s/" % (self.company.id, self.company.name, self.rfc)
        client = zeep.Client(wsdl='https://facturacion.finkok.com/servicios/soap/registration.wsdl')
        if action == 'add':
            client.service.add( 
                reseller_username = settings.VAR_PRODUCTION['INVOICE_USER'], 
                reseller_password = settings.VAR_PRODUCTION['INVOICE_PASSWORD'], 
                taxpayer_id = self.rfc, 
                type_user = 'O', 
                added = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'),
                cer = open(directory+self.rfc+'.cer', "rb").read(),
                key = open(directory+self.rfc+'.key', "rb").read(),
                passphrase = self.password,
            )
        else:
            client.service.edit(
                reseller_username = settings.VAR_PRODUCTION['INVOICE_USER'],
                reseller_password = settings.VAR_PRODUCTION['INVOICE_PASSWORD'],
                taxpayer_id = self.rfc, 
                cer = open(directory+self.rfc+'.cer', "rb").read(),
                key = open(directory+self.rfc+'.key', "rb").read(),
                passphrase = self.password,
                status = 'A',
            )
        respuesta = directory.replace(" ","\ ")+self.rfc+'.cer'
        nocertificado = os.popen('openssl x509 -inform DER -in '+respuesta+' -noout -serial').readlines()[0].replace("serial=","").replace("\n","")
        self.nocertificado = "" 
        for i in range(len(nocertificado)):
            if i % 2 != 0:
                self.nocertificado = self.nocertificado + nocertificado[i]
        self.save()
        return True

    def add_text(self):
        return "NUEVA "+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Agrega "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

    def edit_text(self):
        return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

    def delete_text(self):
        return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

    def get_absolute_url_list(self):
        return reverse(APP+':'+self.MODEL.lower()+'-list')

    def get_absolute_url_add(self):
        return reverse(APP+':'+self.MODEL.lower()+'-add')

    def get_absolute_url_detail(self):
        return reverse(APP+':'+self.MODEL.lower()+'-detail',kwargs={'pk': self.pk})

    def get_absolute_url_update(self):
        return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})

    def get_absolute_url_delete(self):
        return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})


VERBOSE_NAME = "Factura"
VERBOSE_NAME_PLURAL = "Facturas"
MODEL = "Invoice"
GENDER_NEW = "nueva"
GENDER_THIS = "esta"

class Invoice(models.Model):

    VERBOSE_NAME = VERBOSE_NAME
    VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
    MODEL = MODEL
    GENDER_NEW = GENDER_NEW
    GENDER_THIS = GENDER_THIS
    APP = APP

    company = models.ForeignKey('project.Company', related_name='+', on_delete=models.CASCADE, verbose_name="")
    xml = models.TextField()
    is_cancel = models.BooleanField(default=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    uuid = models.CharField(max_length=250, blank=True, null=True)
    serial = models.CharField(max_length=50, blank=True, null=True)
    folio = models.CharField(max_length=50, blank=True, null=True)
    nocertificadosat = models.CharField(max_length=250, blank=True, null=True)
    satseal = models.TextField(blank=True, null=True)
    nocertificadosat = models.CharField(max_length=250, blank=True, null=True)
    fecha = models.CharField(max_length=250, blank=True, null=True)
    active = models.BooleanField(default=True)

    class Meta:
        default_permissions = ()
        verbose_name = VERBOSE_NAME
        verbose_name_plural = VERBOSE_NAME_PLURAL
        permissions = (
            ('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
            ('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
            ('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
            ('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
        )

    def __str__(self):
        return str(self.id)
    
    def date(self):
        jsonxml = xmltodict.parse(self.xml.encode('utf-8'))
        return jsonxml['cfdi:Comprobante']['@Total']

    def ticket(self):
        from sellpoint.models import Ticket
        jsonxml = xmltodict.parse(self.xml.encode('utf-8'))
        t = Ticket.objects.filter(total=self.total(), invoice=True, date__lt=self.creation_date, sellpoint__company=self.company).first()
        if t:
            return t
        else:
            return '-'

    def rfc(self):
        jsonxml = xmltodict.parse(self.xml.encode('utf-8'))
        return jsonxml['cfdi:Comprobante']['cfdi:Receptor']['@Rfc']
    
    def name(self):
        jsonxml = xmltodict.parse(self.xml.encode('utf-8'))
        return jsonxml['cfdi:Comprobante']['cfdi:Receptor']['@Nombre']

    def subtotal(self):
        jsonxml = xmltodict.parse(self.xml.encode('utf-8'))
        return jsonxml['cfdi:Comprobante']['@SubTotal']
    subtotal.allow_tags = True

    def total(self):
        jsonxml = xmltodict.parse(self.xml.encode('utf-8'))
        return jsonxml['cfdi:Comprobante']['@Total']
    total.allow_tags = True

    def make_cfdi(self, data):
        #xml_header = '<?xml version="1.0" encoding="ascii"?>'
        nscfdi = 'http://www.sat.gob.mx/cfd/3'
        nsxsi = 'http://www.w3.org/2001/XMLSchema-instance'
        nsmap = {
            'cfdi': nscfdi,
            'xsi': nsxsi,
        }
        print("HOLA MUNDO")
        Comprobante = Element('{%s}Comprobante' % nscfdi, nsmap=nsmap, attrib={"{%s}schemaLocation" % nsxsi: "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd"},
            Version='3.3',
            Serie=data['serie'],
            Folio=data['folio'],
            Fecha=data['fecha'],
            Sello='',
            NoCertificado=data['nocertificado'],
            Certificado='',
            SubTotal=data['subtotal'],
            Moneda=data['moneda'],
            Total=data['total'],
            TipoDeComprobante=data['tipodecomprobante'],
            CondicionesDePago=data['condicionesdepago'],
            LugarExpedicion=data['lugarexpedicion'],
            MetodoPago=data['metodopago'],
            FormaPago=data['formapago'],
        )
        CfdiEmisor = SubElement(Comprobante, '{%s}Emisor' % nscfdi, 
            Rfc=data['emisor_rfc'],
            Nombre=data['emisor_nombre'],
            RegimenFiscal=data['emisor_regimenfiscal'],
        )
        CfdiReceptor = SubElement(Comprobante, '{%s}Receptor' % nscfdi, 
            Rfc=data['receptor_rfc'],
            Nombre=data['receptor_nombre'],
            UsoCFDI=data['receptor_usocfdi'],
        )
        CfdiConceptos = SubElement(Comprobante, '{%s}Conceptos' % nscfdi,
        )
        transladados = {}
        total_transladados = 0
        for concepto in data['conceptos']:
            CfdiConcepto = SubElement(CfdiConceptos, '{%s}Concepto' % nscfdi,
                ClaveProdServ = concepto['claveprodserv'],
                ClaveUnidad = concepto['claveunidad'],
                Unidad = concepto['unidad'],
                Cantidad = concepto['cantidad'],
                NoIdentificacion = concepto['noidentificacion'],
                Descripcion = concepto['descripcion'],
                ValorUnitario = concepto['valorunitario'],
                Importe = concepto['importe'],
                #Descuento = concepto['descuento'],
            )
            if concepto['impuestos_transladados']:
                CfdiImpuestos = SubElement(CfdiConcepto, '{%s}Impuestos' % nscfdi,
                )
                CfdiTraslados = SubElement(CfdiImpuestos, '{%s}Traslados' % nscfdi,
                )
                for impuesto in concepto['impuestos_transladados']:
                    CfdiTraslado = SubElement(CfdiTraslados, '{%s}Traslado' % nscfdi,
                        Base = impuesto['base'],
                        Impuesto = impuesto['impuesto'],
                        TipoFactor = impuesto['tipofactor'],
                        TasaOCuota = impuesto['tasaocuota'],
                        Importe = impuesto['importe'],
                    )
                    if not impuesto['impuesto'] in transladados:
                        transladados.update({
                            impuesto['impuesto']:{
                                'Impuesto': impuesto['impuesto'],
                                'TipoFactor': impuesto['tipofactor'],
                                'TasaOCuota': impuesto['tasaocuota'],
                                'Importe': '0.00',
                            }
                        })
                    transladados[impuesto['impuesto']]['Importe'] = float(transladados[impuesto['impuesto']]['Importe']) + float(impuesto['importe'])
                    total_transladados = total_transladados + float(impuesto['importe'])
        if transladados:
            Cfdi_impuestos_Impuestos = SubElement(Comprobante, '{%s}Impuestos' % nscfdi,
                TotalImpuestosTrasladados = '{0:.2f}'.format(total_transladados)
            )
            Cfdi_impuestos_Traslados = SubElement(Cfdi_impuestos_Impuestos, '{%s}Traslados' % nscfdi,
            )
            for k, transladado in transladados.items():
                Cfdi_impuestos_Traslado = SubElement(Cfdi_impuestos_Traslados, '{%s}Traslado' % nscfdi,
                    Impuesto = transladado['Impuesto'],
                    TipoFactor = transladado['TipoFactor'],
                    TasaOCuota = transladado['TasaOCuota'],
                    Importe = '{0:.2f}'.format(transladado['Importe']),
                )
        client = zeep.Client(wsdl='https://facturacion.finkok.com/servicios/soap/stamp.wsdl')
        response = client.service.sign_stamp( 
            username = settings.VAR_PRODUCTION['INVOICE_USER'], 
            password = settings.VAR_PRODUCTION['INVOICE_PASSWORD'], 
            xml = tostring(Comprobante), 
        )
        #return response, Comprobante
        print(response)
        print("#######")
        self.company = data['company']
        self.xml = response['xml']
        self.uuid = response['UUID']
        self.serial = data['serie']
        self.satseal = response['SatSeal']
        self.folio = data['folio']
        self.nocertificadosat = response['NoCertificadoSAT']
        self.fecha = response['Fecha']
        self.save()
        invoice = self
        self.make_pdf(xml = invoice.xml, data = data, invoice = invoice)
        html_content = get_template('email.html').render()
        plaintext = get_template('email.txt').render()
        msg = EmailMultiAlternatives(
            subject="Facturación Estrella",
            body=plaintext,
            from_email='pasteleriaslaestrella@gmail.com',
            to=(data['email'],),
        )
        msg.attach_file('cfdi.xml')
        msg.attach_file('cfdi.pdf')
        msg.attach_alternative(html_content, "text/html")
        msg.send(False)

        html_content = get_template('email.html').render()
        plaintext = get_template('email.txt').render()
        msg = EmailMultiAlternatives(
            subject="Facturación Estrella",
            body=plaintext,
            from_email='pasteleriaslaestrella@gmail.com',
            to=('contabilidad.laestrella@gmail.com',),
        )
        msg.attach_file('cfdi.xml')
        msg.attach_file('cfdi.pdf')
        msg.attach_alternative(html_content, "text/html")
        msg.send(False)
        return {'xml': self.xml, 'uuid': self.uuid, 'pdf': 'response_pdf'}
        

    def make_pdf(self, xml = False, data = False, invoice = False):
        jsonxml = xmltodict.parse(xml.encode('utf-8'))
        #print(jsonxml['cfdi:Comprobante'])
        #print({'xml':jsonxml, 'data':data, 'invoice':invoice})
        html = render_to_string('cfdi/cfdi.html', {'xml':jsonxml, 'data':data, 'invoice':invoice})
        #print (json.dumps(jsonxml, indent=4))
        options = {
            'page-size': 'Letter',
            'encoding': "UTF-8",
            'dpi': 300,
        }
        f = open("cfdi.xml", "w")
        f.write(xml)
        f.close()
        df = pdfkit.from_string(html, 'cfdi.pdf', options, css='invoice/templates/cfdi/app.min.css')
        return True










