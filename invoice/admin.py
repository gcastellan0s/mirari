from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *

class PassAdmin(admin.ModelAdmin):
    pass

@admin.register(FiscalData)
class FiscalDataAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('company', 'rfc', 'razon_social', 'persona', 'nocertificado')

@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('company', 'creation_date', 'uuid', 'serial', 'folio', 'total', 'subtotal')
