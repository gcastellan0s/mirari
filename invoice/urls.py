from django.urls import path
from .views import *
from django_pdfkit import PDFView

app_name = "invoice"

urlpatterns = [

   path('datos-fiscales/', FiscalDataList.as_view(), name='fiscaldata-list'),
   path('datos-fiscales/agregar/', FiscalDataCreate.as_view(), name='fiscaldata-add'),
   path('datos-fiscales/ver/<int:pk>/', FiscalDataDetail.as_view(), name='fiscaldata-detail'),
   path('datos-fiscales/actualizar/<int:pk>/', FiscalDataUpdate.as_view(), name='fiscaldata-update'),
   path('datos-fiscales/eliminar/<int:pk>/', FiscalDataDelete.as_view(), name='fiscaldata-delete'),

   path('facturar-ticket-de-compra/', invoice_ticket, name='invoice_ticket'),

   path('cfdi/', PDFView.as_view(template_name='cfdi.html'), name='cfdi'),
   path('cfdi2/', cfdi2, name='cfdi2'),

]