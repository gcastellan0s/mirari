const electron = require('electron')
var path = require('path')
const {app, BrowserWindow} = electron

let mainWindow;

app.on('ready', function() {
	const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize
	let mainWindow = new BrowserWindow({
		width: width, 
		height: height,
    	frame: false,
    	//kiosk: true,
		icon: path.join(__dirname, '/assets/img/ico.png'),
	})
	//mainWindow.setFullScreen(true)
	mainWindow.loadURL('file://' + __dirname + '/browser.html');
  	//mainWindow.openDevTools();
  	//mainWindow.webContents.print({silent: false});
});