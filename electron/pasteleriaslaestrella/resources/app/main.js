const electron = require('electron')
var path = require('path')
const {app, BrowserWindow} = electron
let mainWindow;

app.on('ready', function() {
  	const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize
	mainWindow = new BrowserWindow({
		width: width, 
		height: height,
		//frame: false,
    	frame: false,
		icon: path.join(__dirname, '/assets/img/ico.png')
	})
	//mainWindow.setFullScreen(true)
  	mainWindow.webContents.print({silent: true});
	mainWindow.loadURL('file://' + __dirname + '/browser.html');
  	//mainWindow.openDevTools();
});