from django import template
from magicproseries.models import *
from datetime import datetime
register = template.Library()

@register.filter
def mps_events(var):
	seasson = Seasson().get_seasson()
	date = datetime.today()
	return Event.objects.filter(seasson=seasson, date__gte=date).order_by('-date')

@register.filter
def mps_all_photos(var):
	seasson = Seasson().get_seasson()
	return Event_gallery.objects.filter(event__seasson = seasson).order_by('?')[0:50]

@register.filter
def mps_players_standings(var):
	return MPS_Player.objects.all().order_by('-points')[0:80]

@register.filter
def all_stores(var):
	return Store.objects.all()

@register.filter
def events_in_store(store):
	seasson = Seasson().get_seasson()
	return Event.objects.filter(store=store, seasson=seasson).order_by('-date')