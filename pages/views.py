from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.shortcuts import render
from project.views import variables

def landing(request):
	VAR = variables(request)
	css = ['sites/' + VAR['VAR_LOCAL']['PAGES']['LANDING']+'landing_css.html',]
	js = ['sites/' + VAR['VAR_LOCAL']['PAGES']['LANDING']+'landing_js.html',]
	return render(request, 'sites/' + VAR['VAR_LOCAL']['PAGES']['LANDING']+'landing.html',locals())