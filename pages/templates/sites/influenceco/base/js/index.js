$(function() {

	causeRepaintsOn = $(".head_txt h1, .head_txt h4, .head_txt p");

	$(window).resize(function() {
		causeRepaintsOn.css("z-index", 1);
	});

});
function myCallback(el,id){
	if($(id).css("opacity")<1){return;}
	var span=$(id).find("span");
	clearTimeout(timeout);
	span.addClass("on");
	var timeout=setTimeout(function(){span.removeClass("on")},350);
}
var windowWidthMob = window.innerWidth;
$(document).ready(function(){
	//console.log('document'+windowWidthMob);
	if(windowWidthMob >= 1024){
		var screen_height = window.innerHeight;
		//$("body").css('height',screen_height+'px');
		$("body").css({'height':screen_height+'px',"overflow-y":"visible"});
		$("body").mCustomScrollbar({
			theme:"minimal",
			mouseWheel : {
				scrollInertia:400,
				scrollAmount:280,
				 axis: "Y"
			},
			callbacks:{
				whileScrolling:function(){ 
					var bodyScrollTop = -(this.mcs.top);
					var top_head = $('.top_head').height();
					var sec_res_web = top_head + ($('#ui_ux_design').height()) - 20;
					var sec_branding = top_head + ($('#ui_ux_design').height())+($('#sec_res_web').height()) - 20;
					
					// console.log(bodyScrollTop+'__'+(top_head-40+($('#ui_ux_design').height())));
					if(bodyScrollTop+top_head-200 < top_head){
						$("section.showsImg .sec_bottom_img").hide();
					}
					if(bodyScrollTop > top_head-40){
						$(".ui-menu__line").css('background','#000');
					} else{
						$(".ui-menu__line").css('background','#fff');
					}
					if(bodyScrollTop > top_head-40){
						$("#ui_ux_design .sec_bottom_img").show();
					} 
					if(bodyScrollTop > sec_res_web){
						$("#sec_res_web .sec_bottom_img").show();
					}
					if(bodyScrollTop > sec_branding){
						$("#sec_branding .sec_bottom_img").show();
					} 
				},
				alwaysTriggerOffsets:false
			}
			
		});
		$(".menu-opener").on('click',function(){
			$("#menuScrollbar").mCustomScrollbar({
				theme:"minimal-dark"
			});
			
			$(".menu-opener, .menu-opener-inner, .menu").toggleClass("active");
			if ($(".menu").hasClass("active")) {
				$('body').mCustomScrollbar("disable");
			
			  $(".menu__overlay").css({'display': 'block', 'opacity': '0.7'});
			  $(".logo").css('opacity', '0.4');
			} else {
				$('body').mCustomScrollbar("update");
				$(".menu__overlay").css({'display': 'none', 'opacity': '0'});
				$(".logo").css('opacity', '1');
			}
		});
		$(".menu__overlay").on('click',function(){
			$(".menu-opener, .menu-opener-inner, .menu").removeClass("active");
			$('body').mCustomScrollbar("update");
			$(".logo").css('opacity', '1');
			 $(".menu__overlay").css({'display': 'none', 'opacity': '0'});
		});
	} else {
		$("body").css('height','auto');
		$(window).scroll(function(){
				var bodyScrollTop = $(this).scrollTop();
				var top_head = $('.top_head').height();
				var sec_res_web = top_head + ($('#ui_ux_design').height()) - 20;
				var sec_branding = top_head + ($('#ui_ux_design').height())+($('#sec_res_web').height()) - 20;
				//alert(bodyScrollTop);
				// console.log(bodyScrollTop+'__'+(top_head-40+($('#ui_ux_design').height())));
				if(bodyScrollTop+top_head-200 < top_head){
					$("section.showsImg .sec_bottom_img").hide();
				}
				if(bodyScrollTop > top_head-40){
					$(".ui-menu__line").css('background','#000');
				} else{
					$(".ui-menu__line").css('background','#fff');
				}
				if(bodyScrollTop > top_head-40){
					$("#ui_ux_design .sec_bottom_img").show();
				} 
				if(bodyScrollTop > sec_res_web){
					$("#sec_res_web .sec_bottom_img").show();
				}
				if(bodyScrollTop > sec_branding){
					$("#sec_branding .sec_bottom_img").show();
				} 
		});
		$(".menu-opener").on('click',function(){
			$("#menuScrollbar").mCustomScrollbar({
				theme:"minimal-dark"
			});
			
			$(".menu-opener, .menu-opener-inner, .menu").toggleClass("active");
			if ($(".menu").hasClass("active")) {
				$('body').css("overflow-y",'hidden');
			
			  $(".menu__overlay").css({'display': 'block', 'opacity': '0.7'});
			  $(".logo").css('opacity', '0.4');
			} else {
				$('body').css("overflow-y",'auto');
				$(".menu__overlay").css({'display': 'none', 'opacity': '0'});
				$(".logo").css('opacity', '1');
			}
		});
		$(".menu__overlay").on('click',function(){
			$(".menu-opener, .menu-opener-inner, .menu").removeClass("active");
			$('body').css("overflow-y",'auto');
			$(".logo").css('opacity', '1');
			$(".menu__overlay").css({'display': 'none', 'opacity': '0'});
		});
	}
});

$(window).resize(function(){
	var windowWidthMob = window.innerWidth;
	//console.log(windowWidthMob);
	if(windowWidthMob >= 1024){
		var screen_height = window.innerHeight;
		$("body").css({'height':screen_height+'px',"overflow-y":"visible"});
		$('body').mCustomScrollbar("update");
		$("body").mCustomScrollbar({
			theme:"minimal",
			mouseWheel : {
				scrollInertia:400,
				scrollAmount:280,
				 axis: "Y"
			},
			callbacks:{
				whileScrolling:function(){ 
					var bodyScrollTop = -(this.mcs.top);
					var top_head = $('.top_head').height();
					var sec_res_web = top_head + ($('#ui_ux_design').height()) - 20;
					var sec_branding = top_head + ($('#ui_ux_design').height())+($('#sec_res_web').height()) - 20;
					
					// console.log(bodyScrollTop+'__'+(top_head-40+($('#ui_ux_design').height())));
					if(bodyScrollTop+top_head-200 < top_head){
						$("section.showsImg .sec_bottom_img").hide();
					}
					if(bodyScrollTop > top_head-40){
						$(".ui-menu__line").css('background','#000');
					} else{
						$(".ui-menu__line").css('background','#fff');
					}
					if(bodyScrollTop > top_head-40){
						$("#ui_ux_design .sec_bottom_img").show();
					} 
					if(bodyScrollTop > sec_res_web){
						$("#sec_res_web .sec_bottom_img").show();
					}
					if(bodyScrollTop > sec_branding){
						$("#sec_branding .sec_bottom_img").show();
					} 
				},
				alwaysTriggerOffsets:false
			}
		});
		$(".menu-opener").on('click',function(){
			if ($(".menu").hasClass("active")) {
				$('body').mCustomScrollbar("disable");
			} else {
				$('body').mCustomScrollbar("update");
			}
		});
		$(".menu__overlay").on('click',function(){
			$('body').mCustomScrollbar("update");
		});
		
	} else {
		//alert('wel');
		$("body").css('height','auto');
		$('body').mCustomScrollbar("disable");
		$(window).scroll(function(){
				var bodyScrollTop = $(this).scrollTop();
				var top_head = $('.top_head').height();
				var sec_res_web = top_head + ($('#ui_ux_design').height());
				var sec_branding = top_head + ($('#ui_ux_design').height())+($('#sec_res_web').height()) - 20;
				//alert(bodyScrollTop);
				// console.log(bodyScrollTop+'__'+(top_head-40+($('#ui_ux_design').height())));
				if(bodyScrollTop+top_head-200 < top_head){
					$("section.showsImg .sec_bottom_img").hide();
				}
				if(bodyScrollTop > top_head-40){
					$(".ui-menu__line").css('background','#000');
				} else{
					$(".ui-menu__line").css('background','#fff');
				}
				if(bodyScrollTop > top_head-40){
					$("#ui_ux_design .sec_bottom_img").show();
				} 
				if(bodyScrollTop > sec_res_web){
					$("#sec_res_web .sec_bottom_img").show();
				}
				if(bodyScrollTop > sec_branding){
					$("#sec_branding .sec_bottom_img").show();
				} 
		});
		$(".menu-opener").on('click',function(){
			if ($(".menu").hasClass("active")) {
				$('body').css("overflow-y",'hidden');
			} else {
				$('body').css("overflow",'visible');
			}
		});
		$(".menu__overlay").on('click',function(){
			$('body').css("overflow",'visible');
		});
	}
});
$(document).ready(function(){
	var windowWidth = $(window).width();
	var windowHeight = $(this).height();
	var windowScrollTop = $(this).scrollTop();
	var screen_height = window.innerHeight;
	var screen_width = window.innerWidth;
	var ban_txt_ht = screen_height - $('.top_nav').height() - $('.head_mouse').height() - 60;
	var txt_ht = ban_txt_ht;
	var scrollbar_ht = screen_height-$('.menu_logo').height();
	var top_head = $('.top_head').height();	
	var top_head = $('.top_head').height();	
	var windowInnerWidth = window.innerWidth;
	$(".top_head,.menu").css('height',screen_height+'px');
	$("#menuScrollbar").css('height',scrollbar_ht+'px');
	$('.table_cell').css({'height':txt_ht+'px','width':screen_width+'px'});
	var head_mouse = screen_height - $('.top_nav').height() - $('.table_cell').height();
	var head_mouse_top= "";
	if(windowInnerWidth < 641){
		head_mouse_top = (head_mouse/2)-10;
	} else{
		head_mouse_top = (head_mouse/2)-30;
	}
	$('.head_mouse').css('margin-top',head_mouse_top+'px');
	
	$("#portfolio").click(function(){
		$(".port_menu").slideToggle();
	});
});


$(window).resize(function(){	
	var windowInnerWidth = window.innerWidth;
	var screen_height = window.innerHeight;
	var screen_width = window.innerWidth;
	var ban_txt_ht = screen_height - $('.top_nav').height() - $('.head_mouse').height() - 60;
	var txt_ht = ban_txt_ht;
	var scrollbar_ht = screen_height-$('.menu_logo').height();
	$(".top_head,.menu").css('height',screen_height+'px');
	$("#menuScrollbar").css('height',scrollbar_ht+'px');
	$('.table_cell').css({'height':txt_ht+'px','width':screen_width+'px'});
	
	var head_mouse = screen_height - $('.top_nav').height() - $('.table_cell').height();
	var head_mouse_top= "";
	if(windowInnerWidth < 641){
		head_mouse_top = (head_mouse/2)-10;
	} else{
		head_mouse_top = (head_mouse/2)-30;
	}
	$('.head_mouse').css('margin-top',head_mouse_top+'px');
	
});