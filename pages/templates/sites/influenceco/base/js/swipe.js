+function() {
    function vector(A, B) {
        return {
            x: B.x - A.x,
            y: B.y - A.y
        };
    }
    function module(vector) {
        return Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));
    }

    function Swipe(node, options) {
        var swipe = this;

        swipe.data = {};
        swipe.node = $(node);
        swipe.options = options || {};

        //  MOUSE EVENTS
        function mousePoint(event) {
            return {
                x: event.clientX,
                y: event.clientY
            };
        }
        swipe.node.on('mousedown', function(event) {
            swipe.start(event, mousePoint(event));
        });
        swipe.node.on('mousemove', function(event) {
            swipe.move(event, mousePoint(event));
        });
        $(window).on('mouseup', function(event) {
            swipe.end(event, mousePoint(event));
        });

        //  TOUCH EVENTS
        function touchPoint(event) {
            return {
                x: event.originalEvent.touches[0].clientX,
                y: event.originalEvent.touches[0].clientY
            };
        }
        swipe.node.on('touchstart', function(event) {
            swipe.start(event, touchPoint(event));
        });
        swipe.node.on('touchmove', function(event) {
            swipe.move(event, touchPoint(event));
        });
        $(window).on('touchend', function(event) {
            swipe.end(event);
        });
    }
    Swipe.prototype = {
        constructor: Swipe,

        swipeStart: false,
        swiping: false,
        startCondition: false,
        startConditionFlag: false,
        data: null,
        node: null,

        setCurrentData: function(event, position) {
            this.data.eventType = event.originalEvent instanceof window.MouseEvent ? 'mouse' : event.originalEvent instanceof window.TouchEvent ? 'touch' : null;

            this.data.current = position || this.data.current;
            this.data.currentTime = Date.now();

            var pointA = this.data.start;
            var pointB = this.data.current;

            var vectorA0 = vector(pointA, {
                x: this.data.start.x,
                y: 0
            });
            var vectorAB = vector(pointA, pointB);
            var vectorBA = vector(pointB, pointA);

            var scalarProduct = vectorA0.x * vectorAB.x + vectorA0.y * vectorAB.y;
            var moduleA0 = module(vectorA0);
            var moduleAB = module(vectorAB);

            var deg = Math.acos(scalarProduct / (moduleA0 * moduleAB)) * (180 / Math.PI);

            this.data.degree = pointA.x > pointB.x ? 360 - deg : deg;

            this.data.delta = vectorBA;
            this.data.length = module(vectorBA);
        },
        start: function(event, position) {
            this.swipeStart = true;
            this.data.start = position;
        },
        move: function(event, position) {
            if(!this.swipeStart || !this.data.start) {
                return;
            }

            this.setCurrentData(event, position);

            if(!this.swiping) {
                if($.isFunction(this.options.startWhen) ? this.options.startWhen.call(this.node[0], this.data) : true) {
                    if(!this.startConditionFlag) {
                        this.startCondition = $.isFunction(this.options.startIf) ? this.options.startIf.call(this.node[0], this.data) : true;
                    }
                    this.startConditionFlag = true;

                    if(!this.startCondition) {
                        return;
                    }

                    this.data.startTime = Date.now();
                    this.node.trigger('swipestart', this.data);
                } else {
                    return;
                }
            }
            this.swiping = true;

            this.node.trigger('swipe', this.data);
        },
        end: function(event) {
            if(this.swiping) {
                this.setCurrentData(event);
                this.node.trigger('swipeend', this.data);
            }

            this.swipeStart = false;
            this.swiping = false;
            this.startCondition = false;
            this.startConditionFlag = false;
            this.data = {};
        }
    };
    $.fn.swipe = function(options) {
        return this.each(function() {
            if(!($(this).data('swipe') instanceof Swipe)) {
                $(this).data('swipe', new Swipe(this, options));
            }
        });
    };
}();