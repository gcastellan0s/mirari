$(function() {

	causeRepaintsOn = $(".head_txt h1, .head_txt h4, .head_txt p");

	$(window).resize(function() {
		causeRepaintsOn.css("z-index", 1);
	});

});
var windowWidthMob = window.innerWidth;
$(document).ready(function(){
	//var windowWidthMob = window.innerWidth;
	//console.log('document_'+windowWidthMob);
	if(windowWidthMob >= 1024){
		var screen_height = window.innerHeight;
		//$("body").css('height',screen_height+'px');
		$("body").css({'height':screen_height+'px',"overflow-y":"visible"});
		$("body").mCustomScrollbar({
			theme:"minimal",
			mouseWheel : {
				scrollInertia:400,
				scrollAmount:280,
				 axis: "Y"
			},
			callbacks:{
				whileScrolling:function(){ 
					var bodyScrollTop = -(this.mcs.top);
					var top_head = $('.top_head').height();
					if(bodyScrollTop > top_head-40){
						$(".ui-menu__line").css('background','#000');
					} else{
						$(".ui-menu__line").css('background','#fff');
					}
				},
				alwaysTriggerOffsets:false
			}
		});
		$(".menu-opener").on('click',function(){
			$("#menuScrollbar").mCustomScrollbar({
				theme:"minimal-dark"
			});
			
			$(".menu-opener, .menu-opener-inner, .menu").toggleClass("active");
			if ($(".menu").hasClass("active")) {
				$('body').mCustomScrollbar("disable");
			
			  $(".menu__overlay").css({'display': 'block', 'opacity': '0.7'});
			  $(".logo").css('opacity', '0.4');
			} else {
				$('body').mCustomScrollbar("update");
				$(".menu__overlay").css({'display': 'none', 'opacity': '0'});
				$(".logo").css('opacity', '1');
			}
		});
		$(".menu__overlay").on('click',function(){
			$(".menu-opener, .menu-opener-inner, .menu").removeClass("active");
			$('body').mCustomScrollbar("update");
			$(".logo").css('opacity', '1');
			 $(".menu__overlay").css({'display': 'none', 'opacity': '0'});
		});
	} else {
		$("body").css('height','auto');
		$(window).scroll(function(){
			var bodyScrollTop = $(this).scrollTop();
			var top_head = $('.top_head').height();
			if(bodyScrollTop > top_head-40){
				$(".ui-menu__line").css('background','#000');
			} else{
				$(".ui-menu__line").css('background','#fff');
			}
		});
		$(".menu-opener").on('click',function(){
			$("#menuScrollbar").mCustomScrollbar({
				theme:"minimal-dark"
			});
			
			$(".menu-opener, .menu-opener-inner, .menu").toggleClass("active");
			if ($(".menu").hasClass("active")) {
				$('body').css("overflow-y",'hidden');
			
			  $(".menu__overlay").css({'display': 'block', 'opacity': '0.7'});
			  $(".logo").css('opacity', '0.4');
			} else {
				$('body').css("overflow-y",'auto');
				$(".menu__overlay").css({'display': 'none', 'opacity': '0'});
				$(".logo").css('opacity', '1');
			}
		});
		$(".menu__overlay").on('click',function(){
			$(".menu-opener, .menu-opener-inner, .menu").removeClass("active");
			$('body').css("overflow-y",'auto');
			$(".logo").css('opacity', '1');
			$(".menu__overlay").css({'display': 'none', 'opacity': '0'});
		});
	}
});

$(window).resize(function(){
	var windowWidthMob = window.innerWidth;
	//console.log('resize_'+windowWidthMob);
	if(windowWidthMob >= 1024){
		var screen_height = window.innerHeight;
		$("body").css({'height':screen_height+'px',"overflow-y":"visible"});
		$('body').mCustomScrollbar("update");
		$("body").mCustomScrollbar({
			theme:"minimal",
			mouseWheel : {
				scrollInertia:400,
				scrollAmount:280,
				 axis: "Y"
			},
			callbacks:{
				whileScrolling:function(){ 
					var bodyScrollTop = -(this.mcs.top);
					var top_head = $('.top_head').height();
					if(bodyScrollTop > top_head-40){
						$(".ui-menu__line").css('background','#000');
					} else{
						$(".ui-menu__line").css('background','#fff');
					}
				},
				alwaysTriggerOffsets:false
			}
		});
		$(".menu-opener").on('click',function(){
			if ($(".menu").hasClass("active")) {
				$('body').mCustomScrollbar("disable");
			} else {
				$('body').mCustomScrollbar("update");
			}
		});
		$(".menu__overlay").on('click',function(){
			$('body').mCustomScrollbar("update");
		});
		
	} else {
		//alert('wel');
		$("body").css('height','auto');
		$('body').mCustomScrollbar("disable");
		$(window).scroll(function(){
			var bodyScrollTop = $(this).scrollTop();
			var top_head = $('.top_head').height();
			if(bodyScrollTop > top_head-40){
				$(".ui-menu__line").css('background','#000');
			} else{
				$(".ui-menu__line").css('background','#fff');
			}
		});
		$(".menu-opener").on('click',function(){
			if ($(".menu").hasClass("active")) {
				$('body').css("overflow-y",'hidden');
			} else {
				$('body').css("overflow",'visible');
			}
		});
		$(".menu__overlay").on('click',function(){
			$('body').css("overflow",'visible');
		});
	}
});
$(document).ready(function(){
	var windowWidth = $(this).width();
	var windowHeight = $(this).height();
	var windowScrollTop = $(this).scrollTop();
	var screen_height = window.innerHeight;
	var screen_width = window.innerWidth;
	var abt_table_cell = $('#abt_us_sec').height();
	var ban_txt_ht = screen_height - $('.top_nav').height() - $('.head_mouse').height() - 60;
	var txt_ht = ban_txt_ht-25;
	var scrollbar_ht = screen_height-$('.menu_logo').height();
	var top_head = $('.top_head').height();	
	var top_head = $('.top_head').height();	
	var windowInnerWidth = window.innerWidth;
	$(".menu").css('height',screen_height+'px');
	$("#menuScrollbar").css('height',scrollbar_ht+'px');
	//$('.table_cell').css({'height':txt_ht+'px','width':screen_width+'px'});
	$('.table_cell').css({'width':screen_width+'px'});
	$('.abt_table_cell').css({'height':abt_table_cell+'px'});
	
	$("#portfolio").click(function(){
		$(".port_menu").slideToggle();
	});
});


$(window).resize(function(){
	var windowInnerWidth = window.innerWidth;
	var screen_height = window.innerHeight;
	var screen_width = window.innerWidth;
	var ban_txt_ht = screen_height - $('.top_nav').height() - $('.head_mouse').height() - 60;
	var txt_ht = ban_txt_ht-65;
	var scrollbar_ht = screen_height-$('.menu_logo').height();
	var abt_table_cell = $('#abt_us_sec').height();
	$(".menu").css('height',screen_height+'px');
	$("#menuScrollbar").css('height',scrollbar_ht+'px');
	//$('.table_cell').css({'height':txt_ht+'px','width':screen_width+'px'});
	$('.table_cell').css({'width':screen_width+'px'});
	$('.abt_table_cell').css({'height':abt_table_cell+'px'});
});