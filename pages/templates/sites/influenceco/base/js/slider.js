+function() {
    /*
    * .slider
    *       .slider-without-previous-direction
    *       .slider-without-next-direction
    * .swipe-mode
    *       .swipe-without-mouse
    *       .swipe-without-touch
    *       .swipe-without-carousel
    *       .swipe-without-previous-direction
    *       .swipe-without-next-direction
    * .fade-mode
    * .arrow-mode
    *       .arrow-without-previous-direction
    *       .arrow-without-next-direction
    * .tab-mode
    *       .tab-without-previous-direction
    *       .tab-without-next-direction
    * .button-mode
    *       .button-previous-direction
    *       .button-without-previous-direction
    *       .button-without-next-direction
    * [data-time] or [time]
    *       .timer-previous-direction
    *       .timer-without-previous-direction
    *       .timer-without-next-direction
    * */
    function Slider( node ) {
        this.wrapper = $( node );
        this.render();
    }
    Slider.prototype = {
        constructor: Slider,

        options: {
            maxSwipeLength: 10,
            minSwipeDuration: 100,
            maxSwipeDuration: 700,
            maxSwipeTime: 300
        },

        is: function(className) {
            return $(this.wrapper).hasClass(className);
        },

        __currentSlideIndex: 0,
        get currentSlideIndex() {
            return this.__currentSlideIndex;
        },
        set currentSlideIndex(value) {
            var oldValue = this.__currentSlideIndex;

            if(oldValue == value) {
                return;
            }

            this.__currentSlideIndex = value;

            var slides = this.getSlides();
            this.wrapper.trigger('slide_changed', {
                value: {
                    current: value,
                    last: oldValue
                },
                slides: slides
            });
        },

        __swipeDirection: 0,
        get swipeDirection() {
            return this.__swipeDirection;
        },
        set swipeDirection(value) {
            var oldValue = this.__swipeDirection;

            if(oldValue == value) {
                return;
            }

            this.__swipeDirection = value;

            if($.isFunction(this.swipeDirectionChanged)) {
                this.swipeDirectionChanged.call(this, value, oldValue);
            }
        },
        swipeDirectionChanged: function(newValue, oldValue) {
            this.oldNewSlide = oldValue > 0 ? this.getNextSlide() : oldValue < 0 ? this.getPreviousSlide() : null;
            this.newSlide = newValue > 0 ? this.getNextSlide() : newValue < 0 ? this.getPreviousSlide() : null;

            var slides = this.getSlides()
                .css('opacity', '')
                .css(this.transitionDuration(''))
                .css(this.transform(''))
                .removeClass('previous')
                .removeClass('next')
                .removeClass('animating');

            if(!oldValue) {
                this.sliderWidth = this.wrapper.width();
            }

            if(newValue) {
                this.newSlide.addClass(newValue < 0 ? 'previous' : newValue > 0 ? 'next' : '');
            } else {
                var lastCurrentSlide = this.getCurrentSlide();
                var currentSlide = this.oldNewSlide;
                var percents = Math.abs(this.swipeData.delta.x * 100 / this.sliderWidth);
                var duration = this.swipeData.currentTime - this.swipeData.startTime;

                if(duration < this.options.minSwipeDuration) {
                    duration = this.options.minSwipeDuration;
                }
                if(duration > this.options.maxSwipeDuration) {
                    duration = this.options.maxSwipeDuration;
                }

                lastCurrentSlide.addClass('animating');
                currentSlide.addClass('animating');

                if(this.isSwipeWithoutDirection(oldValue) || (percents < this.options.maxSwipeLength) || (duration > this.options.maxSwipeTime && percents < 50)) {
                    currentSlide.addClass(oldValue > 0 ? 'next' : oldValue < 0 ? 'previous' : '');
                    return;
                }

                lastCurrentSlide
                    .css(this.transitionDuration(duration + 'ms'))
                    .removeClass('current')
                    .addClass(oldValue > 0 ? 'previous' : oldValue < 0 ? 'next' : '');
                currentSlide
                    .css(this.transitionDuration(duration + 'ms'))
                    .addClass('current');

                var currentSlideIndex = slides.toArray().indexOf(currentSlide[0]);

                this.setCurrentTab(currentSlideIndex);

                this.currentSlideIndex = currentSlideIndex;

                this.setTimer();
            }
        },
        swipeMove: function(data) {
            if(this.isSwipeWithoutDirection()) {
                return;
            }

            var percents = -this.getSwipePercents();

            //if(this.wrapper.hasClass('fade-mode')) {
            //    this.getCurrentSlide().css('opacity', 1 - Math.abs(percents / 100));
            //    this.newSlide.css('opacity', Math.abs(percents / 100));
            //    return;
            //}

            this.getCurrentSlide().css(this.transform('translate3d(' + percents + '%,0,0)'));
            this.newSlide.css(this.transform('translate3d(' + ((percents < 0 ? 100 : -100) + percents) + '%,0,0)'));
        },
        isNotSwipeByType: function(type) {
            return (type === 'mouse' && this.is('swipe-without-mouse')) || (type === 'touch' && this.is('swipe-without-touch'));
        },
        isSwipeWithoutDirection: function(swipeDirection) {
            if(!arguments.length) {
                swipeDirection = this.swipeDirection;
            }
            return (this.is('swipe-without-carousel') && (((swipeDirection < 0) && (this.currentSlideIndex === 0)) || ((swipeDirection > 0) && (this.currentSlideIndex === this.getSlides().length - 1)))) ||
                ((swipeDirection < 0) && (this.is('swipe-without-previous-direction') || this.is('slider-without-previous-direction'))) ||
                ((swipeDirection > 0) && (this.is('swipe-without-next-direction') || this.is('slider-without-next-direction')));
        },
        transform: function(value) {
            return {
                '-webkit-transform': value,
                '-moz-transform': value,
                '-o-transform': value,
                'transform': value
            };
        },
        transition: function(value) {
            return {
                '-webkit-transition': value,
                '-moz-transition': value,
                '-o-transition': value,
                'transition': value
            };
        },
        transitionDuration: function(value) {
            return {
                '-webkit-transition-duration': value,
                '-moz-transition-duration': value,
                '-o-transition-duration': value,
                'transition-duration': value
            };
        },
        getSwipePercents: function() {
            return this.swipeData.delta.x * 100 / this.wrapper.width();
        },

        setCurrentSlideByIndexWithDirection: function(slideIndex, directionFlag) {
            if(this.is('swipe-mode') && this.is('slider-swiping')) {
                return;
            }

            var lastCurrentSlideIndex = this.currentSlideIndex;
            var currentSlideIndex = slideIndex;
            var slides = this.getSlides();
            var slidesLength = slides.length - 1;

            if(!slidesLength || lastCurrentSlideIndex == currentSlideIndex) {
                slides.eq(currentSlideIndex).addClass('current');
                return;
            }

            slides
                .css('opacity', '')
                .css(this.transitionDuration(''))
                .css(this.transform(''))
                .removeClass('previous')
                .removeClass('current')
                .removeClass('next')
                .removeClass('animating');

            directionFlag = typeof directionFlag == 'number' ? directionFlag > 0 : currentSlideIndex > lastCurrentSlideIndex;

            if(currentSlideIndex < 0) {
                currentSlideIndex = slidesLength;
            }
            if(currentSlideIndex > slidesLength) {
                currentSlideIndex = 0;
            }

            this.setCurrentTab(currentSlideIndex);

            this.currentSlideIndex = currentSlideIndex;

            var lastCurrentSlide = slides.eq(lastCurrentSlideIndex);
            var currentSlide = slides.eq(currentSlideIndex);

            lastCurrentSlide.addClass('current');
            currentSlide.addClass(directionFlag ? 'next' : 'previous');

            setTimeout(function() {
                lastCurrentSlide.removeClass('current').addClass(directionFlag ? 'previous' : 'next').addClass('animating');
                currentSlide.removeClass(directionFlag ? 'next' : 'previous').addClass('current').addClass('animating');
            }, 50);

            this.setTimer();
        },

        setCurrentTab: function(slideIndex) {
            if(!this.is('tab-mode')) {
                return;
            }

            var scope = this;

            var tabs = scope.wrapper.find('.tabs');

            var lastCurrentTab = $(scope.tabs[scope.currentSlideIndex]);
            var currentTab = $(scope.tabs[slideIndex]);

            if(!!lastCurrentTab[0] && !scope.__tabSize) {
                lastCurrentTab.css(scope.transition('none')).removeClass('current');
                scope.__tabSize = parseFloat(lastCurrentTab.css('margin-left')) + parseFloat(lastCurrentTab.css('margin-right')) + parseFloat(lastCurrentTab.css('width'));
                lastCurrentTab.addClass('current').css(scope.transition(''));
            }

            if(!!currentTab[0] && !scope.__currentTabSize) {
                currentTab.css(scope.transition('none')).addClass('current');
                scope.__currentTabSize = parseFloat(currentTab.css('margin-left')) + (parseFloat(currentTab.css('width'))) / 2;
                currentTab.removeClass('current').css(scope.transition(''));
            }

            //setTimeout(function() {
                lastCurrentTab.removeClass('current');
                currentTab.addClass('current');

                tabs.css(scope.transform(scope.is('button-mode') ? 'translate3d(-' + (scope.__tabSize * slideIndex + scope.__currentTabSize) + 'px,0,0)' : ''));
            //}, 500);
        },

        setTimer: function() {
            clearTimeout(this.timer);

            var scope = this;
            var time = parseFloat(scope.wrapper.data('time') || scope.wrapper.attr('time'));
            var timerPreviousDirection = scope.is('timer-previous-direction');

            if(!time || (timerPreviousDirection ? (scope.is('timer-without-previous-direction') || scope.is('slider-without-next-direction')) : (scope.is('timer-without-next-direction') || scope.is('slider-without-next-direction')))) {
                return;
            }

            scope.timer = setTimeout(function() {
                scope.setCurrentSlideByIndexWithDirection(scope.currentSlideIndex + (timerPreviousDirection ? -1 : 1));
            }, time);
        },

        render: function() {
            var scope = this;

            //  set swipe mode
            scope.wrapper
                .swipe({
                    startIf: function(data) {
                        return (scope.getSlides().length > 1) && ((20 <= data.degree && data.degree <= 160) || (200 <= data.degree && data.degree <= 340));
                    }
                })
                .on('touchmove', function(event) {
                    !!$(this).data('swipe').swiping && event.preventDefault();
                })
                .on('swipestart', function(event, data) {
                    if(scope.isNotSwipeByType(data.eventType) || !scope.wrapper.hasClass('swipe-mode')) {
                        return;
                    }

                    scope.wrapper.addClass('slider-swiping');
                })
                .on('swipe', function(event, data) {
                    if(scope.isNotSwipeByType(data.eventType) || !scope.wrapper.hasClass('swipe-mode')) {
                        return;
                    }

                    scope.swipeData = data;
                    scope.swipeDirection = data.delta.x < 0 ? -1 : data.delta.x > 0 ? 1 : 0;

                    if($.isFunction(scope.swipeMove)) {
                        scope.swipeMove.call(scope, data);
                    }
                })
                .on('swipeend', function(event, data) {
                    if(scope.isNotSwipeByType(data.eventType) || !scope.wrapper.hasClass('swipe-mode')) {
                        return;
                    }

                    scope.swipeDirection = 0;
                    scope.wrapper.removeClass('slider-swiping');
                });

            //  set arrow mode
            if(scope.wrapper.hasClass('arrow-mode')) {
                scope.arrowMode = {
                    prev: $('<button class="arrow arrow-prev"></button>'),
                    next: $('<button class="arrow arrow-next"></button>')
                };
                scope.wrapper
                    .append(scope.arrowMode.prev.on('click', function() {
                        if(scope.is('arrow-without-previous-direction') || scope.is('slider-without-previous-direction')) {
                            return;
                        }
                        scope.setCurrentSlideByIndexWithDirection(scope.currentSlideIndex - 1, -1);
                    } ) )
                    .append(scope.arrowMode.next.on('click', function() {
                        if(scope.is('arrow-without-next-direction') || scope.is('slider-without-next-direction')) {
                            return;
                        }
                        scope.setCurrentSlideByIndexWithDirection(scope.currentSlideIndex + 1, 1);
                    } ) );
            }

            //  set tab mode
            if(scope.wrapper.hasClass('tab-mode')) {
                var tabs = $('<ul class="tabs"></ul>');
                var tab = $('<li></li>');
                scope.tabs = [];
                scope.getSlides().toArray().forEach(function(slide, index) {
                    var newTab = tab.clone();
                    scope.tabs.push(newTab);
                    tabs.append(newTab.on('click', function() {
                        if(index < scope.currentSlideIndex ? (scope.is('tab-without-previous-direction') || scope.is('slider-without-previous-direction')) : (scope.is('tab-without-next-direction') || scope.is('slider-without-next-direction'))) {
                            return;
                        }
                        scope.setCurrentSlideByIndexWithDirection(index, index < scope.currentSlideIndex ? -1 : 1);
                    }));
                });
                scope.wrapper.append(tabs);
            }

            //  set button mode
            if(scope.wrapper.hasClass('button-mode')) {
                scope.wrapper.append($('<button class="button-control"></button>').on('click', function(event) {
                    var buttonPreviousDirection = scope.is('button-previous-direction');

                    if(!scope.wrapper.hasClass('button-mode') || (buttonPreviousDirection ? (scope.is('button-without-previous-direction') || scope.is('slider-without-next-direction')) : (scope.is('button-without-next-direction') || scope.is('slider-without-next-direction')))) {
                        return;
                    }

                    event.preventDefault();

                    scope.setCurrentSlideByIndexWithDirection(scope.currentSlideIndex + (buttonPreviousDirection ? -1 : 1));
                }));
            }

            //  set first slide
            scope.currentSlideIndex = 0;
            scope.getCurrentSlide().addClass('current');
            scope.setCurrentTab(scope.currentSlideIndex);
            scope.setTimer();
        },

        getSlides: function() {
            return this.wrapper.find('.slide');
        },
        getSlideIndex: function( slideIndex ) {
            var slidesLength = this.getSlides().length - 1;

            if( slideIndex < 0 ) {
                slideIndex = slidesLength;
            }
            if( slideIndex > slidesLength ) {
                slideIndex = 0;
            }

            return slideIndex;
        },
        getSlideByIndex: function( slideIndex ) {
            return this.getSlides().eq( this.getSlideIndex( slideIndex ) );
        },
        getCurrentSlide: function() {
            return this.getSlideByIndex( this.currentSlideIndex );
        },
        getPreviousSlide: function() {
            return this.getSlideByIndex( this.currentSlideIndex - 1 ).not( this.getCurrentSlide() );
        },
        getNextSlide: function() {
            return this.getSlideByIndex( this.currentSlideIndex + 1 ).not( this.getCurrentSlide() );
        }
    };

    $.fn.slider = function() {
        return this.each(function() {
            if(!($(this).data('slider') instanceof Slider)) {
                $(this).data('slider', new Slider(this));
            }
        });
    };

    $().ready( function() {
        $('.slider').slider();
        $('body').on('click', 'a', function(event) {
            var slide = $(this.hash);

            if(!slide.hasClass('slide')) {
                return;
            }

            slide.parents().each(function() {
                var slider = $(this);
                var klass = slider.data('slider');

                if(!(klass instanceof Slider)) {
                    return;
                }

                var slideIndex = klass.getSlides().toArray().indexOf(slide[0]);

                if(slideIndex < 0) {
                    return;
                }

                event.preventDefault();

                slider.data('slider').setCurrentSlideByIndexWithDirection(slideIndex);

                return false;
            });
        });
    } );
}();