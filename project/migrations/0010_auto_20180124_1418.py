# Generated by Django 2.0.1 on 2018-01-24 20:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
        ('project', '0009_auto_20180124_1141'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='id',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='name',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='user_permissions',
        ),
        migrations.AddField(
            model_name='profile',
            name='group',
            field=models.OneToOneField(default=1, on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='auth.Group'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='user',
            name='profile',
            field=models.ForeignKey(blank=True, help_text='Elige un perfil de lalista para este usuario', null=True, on_delete=django.db.models.deletion.SET_NULL, to='project.Profile', verbose_name=''),
        ),
    ]
