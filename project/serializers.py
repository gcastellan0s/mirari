from rest_framework import serializers
from .models import *

class Clave_Productos_ServiciosSerializer(serializers.ModelSerializer):
	class Meta:
		model = Clave_Productos_Servicios
		fields = ('id','codigo','nombre','string')

class Clave_Unidades_MedidaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Clave_Unidades_Medida
		fields = ('id','codigo','nombre','string')