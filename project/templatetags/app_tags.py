from datetime import datetime
from django import template
from numbertoletters import number_to_letters
#from project.models import Modules
register = template.Library()

@register.filter
def verbose_name(obj, query):
	if len(query) > 1:
		return obj._meta.verbose_name_plural
	else:
		return obj._meta.verbose_name

@register.filter
def verbose_name_singular(obj):
		return obj._meta.verbose_name

@register.filter
def verbose_name_plural(obj):
		return obj._meta.verbose_name_plural

@register.filter(name='rangeloop')
def rangeloop(max):
	return range(1,max)

@register.filter(name='rangecount')
def rangecount(max):
	return range(0,max)

@register.filter
def addaction(text, action):
	return text+'.'+action

@register.filter
def minus(val, cant):
	return val - cant

@register.filter
def addlowerclassname(text,model):
	return text+model.__class__.__name__.lower()

@register.filter
def hasmodule(company, module):
	return company.hasmodule(module)

@register.filter
def haspermission(permission, user):
	return user.has_perm(permission)

@register.filter
def get_my_sellpoints(user, company):
	return user.get_my_sellpoints(company)

@register.filter
def addposturl(text, symbol):
	if text:
		return symbol+text
	return ''

@register.filter
def addpostq(q):
	if q:
		return '&q='+q
	return ''

@register.filter
def rest(value1, value2):
	return value1 - value2

@register.filter
def plus(value1, value2):
	return value1 + value2

@register.filter
def k(dictionary, key):
	try:
		if key == 'cfdi:Concepto' or key == 'cfdi:Traslado':
			if not "<class 'list'>" == str(type(dictionary[key])):
				list_ = [dictionary[key]]
				return list_
		return dictionary[key]
	except:
		return []

@register.filter
def datestring(string):
	string = string.split('T')
	fecha = string[0].split('-')
	hora = string[1].split(':')
	return fecha[2]+'/'+fecha[1]+'/'+fecha[0][2:4]+' '+hora[0]+':'+hora[1]

@register.filter
def numbertostring(number):
	number = number.split('.')
	return number_to_letters(number[0]) + ' pesos. ' + number[1] + '/100'

@register.filter
def add_linebreaks(string, number):
	c = 0
	for s in string:
		if c % number == 0 and c != 0: 
			string = string[0:c] + '<br />' + string[c:]
		c+=1
	return string

@register.filter
def concat(string1, string2):
	print(string2)
	s = string1+'|'+string2
	return s

@register.filter
def replace_space(string, key):
	try:
		return string.replace(key, ' ')
	except:
		return 'Prueba'

