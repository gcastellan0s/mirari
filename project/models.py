# -*- encoding: utf-8 -*-
from django.urls import reverse
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from django.contrib.auth.models import AbstractUser, Permission, Group
from localflavor.mx.models import MXRFCField, MXZipCodeField, MXCURPField
from django.core.exceptions import PermissionDenied
from mptt.models import MPTTModel, TreeForeignKey
from django.db.models.signals import m2m_changed
from django_countries.fields import CountryField
from sellpoint.models import Sellpoint
from django.db import models
from django.conf import settings
from django.utils.safestring import mark_safe
import datetime
from django.utils.text import slugify

class Module(models.Model):
	name = models.CharField(max_length=100)
	code = models.CharField(max_length=100)

	class Meta:
		default_permissions = ()
		verbose_name = "Modulo"
		verbose_name_plural = "Modulos"

	def __str__(self):
		return self.name

def path_cfdi_company(self, filename):
	date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
	upload_to = "companys/%s_%s/certificados/%s__%s" % (self.id, self.slug, date, filename)
	return upload_to

def path_logo(self, filename):
	date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
	upload_to = "companys/%s_%s/logos/%s__%s" % (self.id, self.slug, date, filename)
	return upload_to

def path_cover(self, filename):
	date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
	upload_to = "companys/%s_%s/logos/%s__%s" % (self.id, self.slug, date, filename)
	return upload_to

class Company(MPTTModel):
	parent = TreeForeignKey('self', null=True, blank=True, related_name='+', db_index=True, on_delete=models.CASCADE)
	nivel = models.PositiveIntegerField(default=1)

	is_provider = models.BooleanField(default=False)
	have_provider = models.ManyToManyField('self', blank=True, related_name='+',)
	provider_modules = models.ManyToManyField('Module', blank=True, related_name='+',)

	name = models.CharField('Nombre de la sucursal', max_length=50)
	code = models.CharField(max_length=150, unique=True)

	color = models.CharField('Color', max_length=100, blank=True, null=True)

	slug = models.SlugField()

	logo = ProcessedImageField(blank=True, null=True, upload_to=path_logo, processors=[ResizeToFill(600, 600)],format='JPEG',options={'quality': 60}, verbose_name="", help_text="Logo de la tienda. PNG 600x600")
	cover = ProcessedImageField(blank=True, null=True, upload_to=path_cover, processors=[ResizeToFill(820, 312)],format='JPEG',options={'quality': 60}, verbose_name="", help_text="Portada de la tienda 820x312")

	slogan = models.CharField(max_length=255, blank=True, null=True)
	web = models.CharField(max_length=255, blank=True, null=True)
	domain = models.CharField(max_length=150)
	ip = models.CharField(max_length=20, blank=True, null=True)
	default_mail = models.EmailField('Email de la sucursal', blank=True, null=True)
	contact_mail = models.EmailField('Email de contacto para clientes', blank=True, null=True)
	creation_date = models.DateTimeField(auto_now_add=True)
	payment_date = models.DateField(auto_now_add=True)
	slug = models.CharField(max_length=150, blank=True, null=True)
	modules = models.ManyToManyField('Module', blank=True, related_name='+',)

	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = "Organizaión"
		verbose_name_plural = "Organizaciones"
		permissions = (
			('can_view_company','Ve lista de organizaciones'),
			('can_edit_company','Modifica organizaciones'),
			('can_create_company','Crea organizaciones'),
			('can_delete_company','Elimina organizaciones'),
		)

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.name = self.name.upper()
		self.slug = slugify(self.name)
		super().save()

	def get_descendants_me(self):
		return self.get_descendants(include_self=True)

	def add_text(self):
		return "NUEVA ORGANIZACIÓN","Agregar nueva organización","Crea un nueva organización en el sistema"

	def edit_text(self):
		return "EDITAR ORGANIZACIÓN","Editar esta organización","Edita los datos de esta organización en el sistema"

	def delete_text(self):
		return "ELIMINAR ORGANIZACIÓN","Eliminar esta organización","Elimina los datos de esta organización","Se eliminará esta organización y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse('project:company-list')

	def get_absolute_url_add(self):
		return reverse('project:company-add')

	def get_absolute_url_detail(self):
		return reverse('project:company-detail',kwargs={'pk': self.pk})

	def get_absolute_url_update(self):
		return reverse('project:company-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse('project:company-delete',kwargs={'pk': self.pk})

	def mysellpoints(self):
		return Sellpoint.objects.filter(company=self, active=True, is_active=True)

	def modulos(self):
		p = ''
		for module in self.modules.all():
			if not module.code == 'project':
				p += module.name + ', '
		return p[0:len(p)-2]

	def hasmodule(self, module):
		module = Module.objects.filter(code = module).first()
		if module:
			if module in self.modules.all():
				return True
		return False

CONTACT_TYPE = (
	('phone','Teléfono fijo'),
	('cell','Teléfono móvil'),
	('office_phone','Teléfono de oficina'),
	('home_phone','Teléfono de casa'),
	('email','correo electrónico'),
)

class Contact_info(models.Model):

	contact_type = models.CharField(choices=CONTACT_TYPE, max_length=250, blank=True, null=True, verbose_name='', help_text='Tipo')
	creation_date = models.DateTimeField(auto_now_add=True)
	info = models.CharField(choices=CONTACT_TYPE, max_length=250, blank=True, null=True, verbose_name='', help_text='Contacto')
	user = models.ForeignKey('User', blank=True, null=True, on_delete=models.PROTECT, related_name='+',)

	class Meta:
		default_permissions = ()
		verbose_name = "Forma de contacto"
		verbose_name_plural = "Formas de contacto"

STATE_CHOICES = (
	('Aguascalientes', ('Aguascalientes')),
	('Baja California', ('Baja California')),
	('Baja California Sur', ('Baja California Sur')),
	('Campeche', ('Campeche')),
	('Chihuahua', ('Chihuahua')),
	('Chiapas', ('Chiapas')),
	('Coahuila', ('Coahuila')),
	('Colima', ('Colima')),
	('CDMX', ('CDMX')),
	('Durango', ('Durango')),
	('Guerrero', ('Guerrero')),
	('Guanajuato', ('Guanajuato')),
	('Hidalgo', ('Hidalgo')),
	('Jalisco', ('Jalisco')),
	('Estado de México', ('Estado de México')),
	('Michoacán', ('Michoacán')),
	('Morelos', ('Morelos')),
	('Nayarit', ('Nayarit')),
	('Nuevo León', ('Nuevo León')),
	('Oaxaca', ('Oaxaca')),
	('Puebla', ('Puebla')),
	('Querétaro', ('Querétaro')),
	('Quintana Roo', ('Quintana Roo')),
	('Sinaloa', ('Sinaloa')),
	('San Luis Potosí', ('San Luis Potosí')),
	('Sonora', ('Sonora')),
	('Tabasco', ('Tabasco')),
	('Tamaulipas', ('Tamaulipas')),
	('Tlaxcala', ('Tlaxcala')),
	('Veracruz', ('Veracruz')),
	('Yucatán', ('Yucatán')),
	('Zacatecas', ('Zacatecas')),
)


GENDER = (
	('Hombre', 'Hombre'),
	('Mujer', 'Mujer'),
	('Otro', 'Otro'),
)


class User(AbstractUser):

	password1 = models.CharField('Contraseña', blank=True, null=True, max_length=50, help_text="")
	password2 = models.CharField('Contraseña de nuevo', blank=True, null=True, max_length=50, help_text="")
	visible_username = models.CharField('Usuario', max_length=50, help_text="Obligatorio. Longitud máxima 30 caracteres alfanuméricos (letras, dígitos y @/./+/-/_) solamente.")
	company = models.ForeignKey('Company', blank=True, null=True, on_delete=models.PROTECT, related_name='+',)
	can_change_password = models.BooleanField('Modifica su contraseña', default=True, help_text='Puede cambiar su propia contraseña')
	private_groups = models.ManyToManyField(Group, blank=True, related_name='+',)
	birthday = models.DateField('Fecha nacimiento', blank=True, null=True)
	gender = models.CharField(max_length=50, blank=True, null=True, choices=GENDER)
	country = CountryField(blank=True, null=True, default="MX")

	#sellpoint
	sellpoints = models.ManyToManyField('sellpoint.sellpoint', blank=True, related_name='+', verbose_name="", help_text="Asigna una sucursal, en blanco si estará asignado a todas")

	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = "Usuario"
		verbose_name_plural = "Usuarios"
		permissions = (
			('can_view_user','Ve lista de usuarios'),
			('can_edit_user','Modifica usuarios'),
			('can_create_user','Crea usuarios'),
			('can_delete_user','Elimina usuarios'),
		)

	def __str__(self):
		return self.visible_username

	def add_text(self):
		return "NUEVO USUSARIO","Agregar nuevo usuario","Crea un nuevo usuario en el sistema"

	def edit_text(self):
		return "EDITAR USUSARIO","Editar este usuario","Edita los datos del usuario en el sistema"

	def delete_text(self):
		return "ELIMINAR USUSARIO","Eliminar este usuario","Elimina los datos del usuario en el sistema","Se eliminará el usuario y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse('project:user-list')

	def get_absolute_url_add(self):
		return reverse('project:user-add')

	def get_absolute_url_update(self):
		return reverse('project:user-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse('project:user-delete',kwargs={'pk': self.pk})

	def get_all_master(self):
		return Company.objects.all()

	def perfiles(self):
		p = ''
		for group in self.groups.all():
			p += group.name + ', '
		return p[0:len(p)-2]

	def puntos_de_venta(self):
		p = ''
		for sellpoint in self.sellpoints.all():
			p += sellpoint.name + ', '
		return p[0:len(p)-2]

	def check_code_group(self, code):
		for group in self.groups.all():
			if code == group.code:
				return True
		return False

	def get_my_sellpoints(self, company):
		companysellpoints = company.mysellpoints()
		if self.is_superuser or self.check_code_group('superadminisrator') or self.check_code_group('adminisrator'):
			return companysellpoints
		if 'ped' in self.username:
			return companysellpoints.filter(have_orders=True).order_by('name')
		if not self.sellpoints.all():
			return companysellpoints
		return companysellpoints.filter(pk__in = self.sellpoints.all())

VERBOSE_NAME = "Dirección"
VERBOSE_NAME_PLURAL = "Direcciones"
MODEL = "address"
GENDER_NEW = "nueva"
GENDER_THIS = "esta"

class Address(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS

	user = models.ForeignKey(User, related_name='+', on_delete=models.CASCADE, verbose_name="", help_text="")

	name = models.CharField('Título de la dirección', max_length=255, blank=True, null=True, help_text="Para reconocer rapidamente la dirección **opcional")
	calle = models.CharField(max_length=255, blank=True, null=True)
	numero_exterior = models.CharField(max_length=150, blank=True, null=True)
	numero_interior = models.CharField(max_length=150, blank=True, null=True)
	colonia = models.CharField(max_length=255, blank=True, null=True)
	localidad = models.CharField(max_length=255, blank=True, null=True)
	municipio_delegacion = models.CharField('Municipio o Delegación', max_length=150, blank=True, null=True)
	estado = models.CharField(choices=STATE_CHOICES, max_length=100, blank=True, null=True, verbose_name='', help_text='Estado')
	codigo_postal = MXZipCodeField('Código Postal', blank=True, null=True)
	pais = CountryField(blank=True, null=True, default="MX")
	notes = models.TextField(blank=True, null=True)

	address = models.CharField('',max_length=255, blank=True, null=True, help_text="Dirección")
	lat = models.CharField(max_length=50, blank=True, null=True)
	lng = models.CharField(max_length=50, blank=True, null=True)

	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		if not self.numero_interior:
			numero_interior = 'S/N'
		else:
			numero_interior = self.numero_interior
		if not self.name:
			self.name = ''
		else:
			self.name = '<strong>'+self.name+'</strong><br />'
		return mark_safe("""
				<strong> {8} </strong><br />
				{0} {1}, ext.{2} <br /> Col. {4}, {5} <br /> {6}, {7}, CP.{3}
			""".format(
			self.calle,
			self.numero_exterior,
			numero_interior,
			self.codigo_postal,
			self.colonia,
			self.localidad,
			self.estado,
			self.pais,
			self.name)
		)

VERBOSE_NAME = "Banco"
VERBOSE_NAME_PLURAL = "Bancos"
MODEL = "bank"
GENDER_NEW = "nuevo"
GENDER_THIS = "este"

class Bank(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS

	user = models.ForeignKey(User, related_name='+', on_delete=models.CASCADE, verbose_name="", help_text="")

	banco = models.CharField(max_length=255, blank=True, null=True)
	no_cuenta = models.CharField(max_length=150, blank=True, null=True)
	sucursal = models.CharField(max_length=150, blank=True, null=True)
	clabe = models.CharField(max_length=255, blank=True, null=True)
	no_cuenta_habiente = models.CharField(max_length=255, blank=True, null=True)

	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.no_cuenta


	def string(self):
		return mark_safe("clabe: {0}, no. cuenta:{1}<br /> <strong>{2}</strong>".format(
			self.clabe,
			self.no_cuenta,
			self.banco,
			)
		)


def path_files_user(self, filename):
	date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
	upload_to = "companys/%s_%s/users/files/%s__%s" % (self.user.company.id, self.user.company.slug, date, self.code)
	return upload_to


VERBOSE_NAME = "Archivo"
VERBOSE_NAME_PLURAL = "Archivos"
MODEL = "file_user"
GENDER_NEW = "nuevo"
GENDER_THIS = "este"

class File_user(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS

	user = models.ForeignKey('User', on_delete=models.PROTECT, related_name='+',)

	code = models.CharField('Código del archivo', max_length=250, help_text="")
	file = models.FileField('', help_text='', upload_to=path_files_user)
	name = models.CharField('Nombre del archivo', blank=True, null=True, max_length=250, help_text="")
	creation_date = models.DateTimeField(auto_now_add=True)
	module = models.ForeignKey('Module', on_delete=models.PROTECT, related_name='+',)

	class Meta:
		default_permissions = ()
		verbose_name = "Archivo"
		verbose_name_plural = "Archivos"

	def string(self):
		return mark_safe("{0} - {2} ({1})".format(
				self.code,
				self.creation_date.strftime("%d/%m/%Y %H:%M:%S"),
				self.name
			)
		)

class Serial(models.Model):

	company = models.ForeignKey(Company, related_name='+', on_delete=models.CASCADE, verbose_name="", help_text="Selecciona la organización")
	module = models.ForeignKey(Module, null=True, blank=True, on_delete=models.PROTECT)
	name = models.CharField('Nombre con el que identificas la serie',max_length=120)
	prefix = models.CharField('Prefijo (Se agrega al inicio del folio)',max_length=120, blank=True, null=True)
	serial = models.IntegerField('Numeración actual de la serie', default=1)

	is_global = models.BooleanField('Es global?', default=False, help_text='Se comparte con todas tus sucursales?')
	is_active = models.BooleanField('Esta activa?',default=True, help_text='Sigue disponible para agregar nuevas sucursales a esta serie?')

	hexadecimal = models.BooleanField('Convertir a hexadecimal?',default=False, help_text='Esta serie se convierte a hexadecimal?')

	is_master = models.BooleanField(default=False)
	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = "Serie"
		verbose_name_plural = "Series"

	def __str__(self):
		if self.prefix:
			return '({0}) {1}'.format(self.prefix,self.name)
		else:
			return '{0}'.format(self.name,)


	def add_text(self):
		return "NUEVA SERIE","Agregar nueva serie","Crea una nueva serie para este módulo"

	def edit_text(self):
		return "EDITAR SERIE","Editar esta serie","Edita los datos de esta serie en el sistema"

	def delete_text(self):
		return "ELIMINAR SERIE","Eliminar esta serie","Elimina esta serie del sistema","La serie ya no estará disponible sin embargo aun seguira acumulando folios de los modulos donde aún este asignada!"

	def get_absolute_url_list(self):
		return reverse('project:serial-list')

	def get_absolute_url_add(self):
		return reverse('project:serial-add')

	def get_absolute_url_detail(self):
		return reverse('project:serial-detail',kwargs={'pk': self.pk})

	def get_absolute_url_update(self):
		return reverse('project:serial-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse('project:serial-delete',kwargs={'pk': self.pk})

	def get_next(self):
		self.serial += 1
		self.save()
		return self.serial

class Clave_Productos_Servicios(models.Model):

	codigo = models.CharField('Código de producto o servicio',max_length=120)
	nombre = models.CharField('Nombre de producto o servicio',max_length=255)
	iva_transladado = models.BooleanField(default=False)
	ieps_transladado = models.BooleanField(default=False)
	descripcion = models.CharField(max_length=255, blank=True, null=True)
	tipo = models.CharField(max_length=120, blank=True, null=True)
	division = models.CharField(max_length=120, blank=True, null=True)
	grupo = models.CharField(max_length=120, blank=True, null=True)
	clase = models.CharField(max_length=120, blank=True, null=True)

	class Meta:
		default_permissions = ()

	def __str__(self):
		return '{1} [ {0} ]'.format(self.codigo, self.nombre)

	@property
	def string(self):
		return '{1} [ {0} ]'.format(self.codigo, self.nombre)


class Clave_Unidades_Medida(models.Model):

	codigo = models.CharField('Código de producto o servicio',max_length=120)
	nombre = models.CharField('Nombre de producto o servicio',max_length=255)

	class Meta:
		default_permissions = ()

	def __str__(self):
		return '[ {0} ] {1}'.format(self.codigo, self.nombre)

	@property
	def string(self):
		return '[ {0} ] {1}'.format(self.codigo, self.nombre)


VERBOSE_NAME = "Historial"
VERBOSE_NAME_PLURAL = "Historial"
MODEL = "history"
GENDER_NEW = "Un nuevo"
GENDER_THIS = "este nuevo"

class History(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS

	user = models.ForeignKey('User', on_delete=models.CASCADE)
	action = models.CharField('Acción', max_length=250)
	text = models.TextField('Texto', blank=True, null=True)
	date = models.DateTimeField(auto_now_add=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return str(self.id)


def path_logo_company(self, filename):
	return True
