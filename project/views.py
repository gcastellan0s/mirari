from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import SingleObjectMixin, DetailView
from django.shortcuts import get_object_or_404, get_list_or_404, render
from django.contrib.auth import login, authenticate, logout
from validate_email import validate_email
from config.var_local import *
from django.http import HttpResponseRedirect, JsonResponse
from django.core.exceptions import PermissionDenied
from django.urls import reverse_lazy, reverse
from django.conf import settings
from django.http import Http404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from openpyxl import load_workbook
from django.contrib.auth.models import Group
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from .models import *
from .forms import *

from sellpoint.models import Sellpoint

def return_domain(request):
	default_domain = 'PASTELERIASLAESTRELLA'
	domain = request.META['HTTP_HOST'].split('.')[0]
	if domain == 'www':
		domain = request.META['HTTP_HOST'].split('.')[1]
	if domain == 'localhost:8000' or domain == '172' or domain == '192' or domain == '50' or domain == '192.168.0.115:8081':
		if request.user.is_superuser:
			if 'company' in request.session:
				domain = Company.objects.get(pk=request.session.get('company')).domain
			else:
				domain = default_domain
		else:
			domain = default_domain
	return domain

def return_var_local(domain):
	if domain in VAR_LOCAL[0]:
		var_local = VAR_LOCAL[0][domain]
	else:
		var_local = VAR_LOCAL[0]['DEFAULT']
	return var_local

def variables(request):
	domain = return_domain(request)
	var_local = return_var_local(domain.upper())
	media = settings.MEDIA_URL
	VAR = {
		'DOMAIN':domain.upper(),
		'VAR_GLOBAL':VAR_GLOBAL[0],
		'VAR_LOCAL':var_local,
		'STATIC_URL':settings.STATIC_URL,
		'MEDIA_URL': settings.MEDIA_URL,
		'SOCKETIO':{'PORT':settings.SOCKETIO,'DOMAIN':settings.SOCKETIODOMAIN}
	}
	return VAR

def log_variables(request, module=False):
	VAR = variables(request)
	if not request.user.is_superuser:
		if not VAR['DOMAIN'] == request.user.company.domain.upper():
			raise PermissionDenied
	VAR['COMPANY'] = Company.objects.get(id=request.session.get('company'))
	if not module:
		module = request.resolver_match.app_name
	VAR['MODULE'] = module
	return VAR

def check_permissions(request=True, app=False, permission='', obj=False, in_object=False):
	if request.user.is_superuser:
		return True
	print ("No es superusuario")
	if not app:
		app = request.resolver_match.app_name
	if not request.user.has_perm(app+'.'+permission):
		print ("No tiene el permiso: "+app+'.'+permission)
		raise PermissionDenied
	if obj:
		if not in_object:
			in_object = request.user.company.get_descendants(include_self=True)
		if not obj in in_object:
			print ("No tiene el modulo")
			raise Http404
	if not get_object_or_404(Module, code=app) in get_object_or_404(Company, id=request.session.get('company'), active=True).modules.all():
		print ("No le pertenece")
		raise PermissionDenied
	print ("Si tiene permiso")
	return True

def login_user(request):
	if request.user.is_authenticated:
		if not 'company' in request.session:
			request.session['company'] = request.user.company.pk
		return HttpResponseRedirect(reverse_lazy('project:dashboard', args=[]))
	VAR = variables(request)
	if request.method == "POST":
		flag = 0
		message = False
		if 'login_username' in request.POST:
			username = request.POST.get('login_username')
			password = request.POST.get('login_password')
			if validate_email(username):
				user = User.objects.filter(email=username).first()
				if user:
					username = user.username
				else:
					message = 'Usuario o Contraseña incorrectos'
			else:
				username = VAR['DOMAIN'] + '_' + username
			if not message:
				access = authenticate(username=username, password=password)
				if access:
					if access.is_active and access.active:
						login(request, access)
						flag = 1
					else:
						message = 'Tu usuario no esta activo'
				else:
					message = 'Usuario o Contraseña incorrectos'
		return JsonResponse({
					'message':message,
					'flag' : flag,
				})
	return render(request, 'login.html',locals())

def user_logout(request):
	logout(request)
	return HttpResponseRedirect('/')

def wl(request):
	user = User.objects.get(pk = request.GET.get('user'))
	user.backend = 'django.contrib.auth.backends.ModelBackend'
	login(request, user)
	return HttpResponseRedirect(reverse('project:login_user'))

def dashboard(request):#REVISAR
	VAR = log_variables(request)
	if get_object_or_404(Module, code='mps') in VAR['COMPANY'].modules.all():
		return HttpResponseRedirect(reverse_lazy('magicproseries:index'))
	if request.user.check_code_group('Administrator') or request.user.check_code_group('superadministrator') or request.user.is_superuser:
		return render(request, 'dashboard.html',locals())
	if get_object_or_404(Module, code='sellpoint') in VAR['COMPANY'].modules.all():
		if request.user.check_code_group('vendor_sellpoint'):
			return HttpResponseRedirect(reverse_lazy('sellpoint:sellpoint_vendor'))
		elif request.user.check_code_group('casher_sellpoint'):
			return HttpResponseRedirect(reverse_lazy('sellpoint:sellpoint_casher'))
	return render(request, 'dashboard.html',locals())

def api(request):#REVISAR
	method = request.GET.get('method')
	if method == 'change_company':
		request.session['company'] = Company.objects.get(id=request.GET.get('c')).pk
		if request.user.is_superuser:
			request.user.company = Company.objects.get(id=request.GET.get('c'))
			request.user.save()
		return JsonResponse({
			'api' : True,
			'data' : request.session['company'],
		})

def user_changepassword(request, pk):
	VAR = log_variables(request)
	user = get_object_or_404(User, pk=pk, active=True)
	module = request.GET.get('module')
	if not user == request.user:
		check_permissions(request=request, app=module, permission='can_edit_user', obj=user.company)
	form = UserPasswordForm(instance = user)
	if request.method == "POST":
		form = UserPasswordForm(request.POST, instance=user)
		if form.is_valid():
			user.set_password(form.cleaned_data['password1'])
			user.save()
			if module:
				return HttpResponseRedirect(reverse('project:user-detail', kwargs={'pk': pk})+'?module=' + module)
			else:
				return HttpResponseRedirect(reverse('project:dashboard'))
	return render(request, 'password.html',locals())


##############################################################
# COMPANY
##############################################################
######### LIST
class CompanyList(ListView):
	model, m, paginate_by = Company, 'company', 30
	template_name = m+'_list.html'
	def get_queryset(self):
		self.VAR, self.q = log_variables(self.request), self.request.GET.get('q')
		check_permissions(request=self.request, permission='can_view_'+self.m)
		query = self.request.user.company.get_descendants(include_self=True).filter(active=True)
		if self.q:
			query = query.filter(Q(name__icontains=self.q)|Q(default_mail__icontains=self.q)|Q(contact_mail__icontains=self.q))
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		company = get_object_or_404(Company, pk=self.request.session.get('company'))
		context['master'] = company.get_root()
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		return context

######### CREATE
class CompanyCreate(CreateView):
	model, m = Company, 'company'
	template_name = m+'_create.html'
	form_class = CompanyForm

	def get_success_url(self):
		return reverse('project:'+self.m+'-list')

	def form_valid(self, form):
		company = get_object_or_404(Company, pk=self.request.session.get('company'))
		master = company.get_root()
		form.instance.parent = company
		form.instance.slogan = master.slogan
		form.instance.web = master.web
		form.instance.domain = master.domain
		form.instance.payment_date = master.payment_date
		form.instance.ip = master.ip
		obj = form.save()
		obj.code = str( obj.pk ) +'_'+ master.code
		obj.save()
		for module in company.modules.all():
			obj.modules.add(module)
		return super().form_valid(form)

	def get_context_data(self, **kwargs):
		self.VAR = log_variables(self.request)
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, self.VAR
		context['class'] = 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

######### UPDATE
class CompanyUpdate(UpdateView):
	model, m = Company, 'company'
	template_name = m+'_create.html'
	form_class = CompanyForm

	def get_success_url(self):
		return reverse('project:'+self.m+'-list')

	def get_context_data(self, **kwargs):
		self.VAR = log_variables(self.request)
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, self.VAR
		context['class'] = 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object)
		return context

######### DELETE
class CompanyDelete(DeleteView):
	model, m = Company, 'company'

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('project:'+self.m+'-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object)
		self.object.save()
		messages.error(self.request, 'Aun no se pueden eliminar las organizaciones en el sistema')
		return HttpResponseRedirect(self.get_success_url())

##############################################################
# USER
##############################################################
######### LIST
class UserList(ListView):
	model, m, paginate_by = User, 'user', 30
	template_name = m+'_list.html'

	def get_queryset(self):
		self.module =  get_object_or_404(Module, code=self.request.GET.get('module'))
		self.groups = Group.objects.filter(module = self.module)
		self.VAR, self.q, self.q__ = log_variables(self.request, module=self.module.code), self.request.GET.get('q'), False
		self.filters = []
		check_permissions(request=self.request, app=self.module.code, permission='can_view_'+self.m)
		query = self.model.objects.filter(company=self.VAR['COMPANY'], groups__in=self.groups, active=True).exclude(is_superuser=True).distinct()
		if self.q:
			query = query.filter(visible_username__icontains=self.q)
		self.filters.append({'label':'Permiso', 'name':'q__profile', 'objects':self.groups,'l': self.request.GET.get('lq__profile', '-')})
		if self.request.GET.get('q__profile'):
			self.q__  =  True
			query, self.q__ = query.filter(groups=Group.objects.get(pk=self.request.GET.get('q__profile'))), True
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'], context['filters'], context['q__'] = self.model, self.VAR, self.q, self.filters, self.q__
		context['module'] = self.module #para el input en la busqueda
		context['posturl'] = 'module=' + self.module.code
		return context

######### CREATE
class UserCreate(CreateView):
	model, m, = User, 'user'
	template_name = m+'_create.html'
	form_class = UserForm

	def get_form_kwargs(self):
		self.company =get_object_or_404(Company, pk=self.request.session.get('company'))
		self.module = get_object_or_404(Module, code=self.request.GET.get('module'))
		self.sellpoints = False
		if self.module.code == 'sellpoint':
			self.sellpoints = Sellpoint.objects.filter(company=self.company, active=True, is_active=True)
		kwargs = super(UserCreate, self).get_form_kwargs()
		kwargs['groups'] = Group.objects.filter(module=self.module, is_public=True, company__isnull=True)
		kwargs['mode'] = 'new'
		kwargs['module'] = self.module.code
		kwargs['user'] = False
		kwargs['sellpoints'] = self.sellpoints
		return kwargs

	def get_success_url(self):
		return reverse('project:'+self.m+'-list')+'?module=' + self.module.code

	def form_valid(self, form):
		form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
		form.instance.set_password(form.instance.password2)
		form.instance.password2 = ''
		form.instance.username = (form.instance.company.get_root().code.upper() + '_' + form.instance.visible_username)
		response = super().form_valid(form)
		return response

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.module.code), 'CreateView'
		context['posturl'] = 'module=' + self.request.GET.get('module')
		check_permissions(request=self.request, app=self.module.code, permission='can_create_'+self.m)
		return context

######### UPDATE
class UserUpdate(UpdateView):
	model, m, = User, 'user'
	template_name = m+'_create.html'
	form_class = UserForm

	def get_form_kwargs(self):
		self.company =get_object_or_404(Company, pk=self.request.session.get('company'))
		self.module = get_object_or_404(Module, code=self.request.GET.get('module'))
		self.sellpoints = False
		if self.module.code == 'sellpoint':
			self.sellpoints = Sellpoint.objects.filter(company=self.company, active=True, is_active=True)
		kwargs = super().get_form_kwargs()
		kwargs['groups'] = Group.objects.filter(module=self.module, is_public=True, company__isnull=True)
		kwargs['mode'] = 'edit'
		kwargs['module'] = self.module.code
		kwargs['user'] = self.object
		kwargs['sellpoints'] = self.sellpoints
		return kwargs

	def get_success_url(self):
		return reverse('project:'+self.m+'-list')+'?module=' + self.module.code

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.module.code), 'UpdateView'
		context['posturl'] = 'module=' + self.request.GET.get('module')
		check_permissions(request=self.request, app=self.module.code, permission='can_edit_'+self.m, obj=self.object.company)
		return context

	def form_valid(self, form):
		company = Company.objects.get(pk=self.request.session.get('company'))
		form.instance.username = (company.get_root().code.upper() + '_' + form.instance.visible_username)
		return super().form_valid(form)

######### DELETE
class UserDelete(DeleteView):
	model, m, = User, 'user'

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('project:'+self.m+'-list')+'?module=' + self.module.code

	def delete(self, request, *args, **kwargs):
		self.module = get_object_or_404(Module, code=self.request.GET.get('module'))
		self.object = self.get_object()
		check_permissions(request=self.request, app=self.module.code, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())



##############################################################
# SERIAL
##############################################################
######### LIST
class SerialList(ListView):
	model, m, = Serial, 'serial'
	template_name = m+'_list.html'
	paginate_by = 30

	def get_queryset(self):
		self.module =  get_object_or_404(Module, code=self.request.GET.get('module'))
		self.q, self.VAR = self.request.GET.get('q'), log_variables(self.request, module=self.module.code)
		check_permissions(request=self.request, permission='can_view_'+self.m, app=self.module.code)
		query = Serial.objects.filter(company=self.VAR['COMPANY'], module=self.module, active=True)
		if self.q:
			query = query.filter(Q(name__icontains=self.q)|Q(prefix__icontains=self.q))
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		context['module'] = self.module #para el input en la busqueda
		context['posturl'] = 'module=' + self.module.code
		return context

######### CREATE
class SerialCreate(CreateView):
	model, m, = Serial, 'serial'
	template_name = m+'_create.html'
	form_class = SerialForm

	def get_form_kwargs(self):
		kwargs = super(SerialCreate, self).get_form_kwargs()
		kwargs['module'] = get_object_or_404(Module, code=self.request.GET.get('module'))
		return kwargs

	def get_success_url(self):
		return reverse('project:'+self.m+'-list')+'?module=' + self.request.GET.get('module')

	def get_context_data(self, **kwargs):
		self.module = get_object_or_404(Module, code=self.request.GET.get('module'))
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.module.code), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m, app=self.module.code)
		context['posturl'] = 'module=' + self.module.code
		return context

	def form_valid(self, form):
		form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
		form.instance.module = get_object_or_404(Module, code=self.request.GET.get('module'))
		return super().form_valid(form)

######### UPDATE
class SerialUpdate(UpdateView):
	model, m, = Serial, 'serial'
	template_name = m+'_create.html'
	form_class = SerialForm

	def get_form_kwargs(self):
		kwargs = super(SerialUpdate, self).get_form_kwargs()
		kwargs['module'] = get_object_or_404(Module, code=self.request.GET.get('module'))
		return kwargs

	def get_success_url(self):
		return reverse('project:'+self.m+'-list')+'?module=' + self.request.GET.get('module')

	def get_context_data(self, **kwargs):
		self.module = get_object_or_404(Module, code=self.request.GET.get('module'))
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.module.code), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, app=self.module.code, obj=self.object.company)
		context['posturl'] = 'module=' + self.module.code
		return context

######### DELETE
class SerialDelete(DeleteView):
	model, m, = Serial, 'serial'

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('project:'+self.m+'-list')+'?module=' + self.request.GET.get('module')

	def delete(self, request, *args, **kwargs):
		self.module = get_object_or_404(Module, code=self.request.GET.get('module'))
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, app=self.module.code, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())

#@csrf_exempt
#def send_mail(request):
	#method = request.GET.get('method')
	#if method == 'send_contact':

		#name = request.GET.get('name')
		#email = request.GET.get('email')
		#phone = request.GET.get('phone')

		#to = email_pruebas
		#from_email = 'INOTEC'+'<'+ miempresa.default_mail +'>'
		#subject = 'NUEVO CONTACTO'
		#content = (''+
			#'<hr />'+
			#'<p><strong>Nombre: </strong>'+unicode(name)+'</p>'+
			#'<p><strong>Email: </strong>'+unicode(email)+'</p>'+
			#'<p><strong>Tema: </strong>'+unicode(theme)+'</p>'+
			#'<p><strong>Mensaje: </strong>'+unicode(message)+'</p>'+
			#'')
		#c = Context({
			#'empresa': miempresa.nombre,
			#'eslogan': miempresa.eslogan,
			#'domain': miempresa.web,
			#'header': 'NUEVO CONTACTO',
			#'message': 'Gracias '+ name +' por contactarnos.',
			#'body': content,
		#})
		#template = get_template('app/mail/layout.html').render(c)
		#msg = EmailMultiAlternatives(
			#subject=subject,
			#body=template,
			#from_email= 'CREDIPyME'+'<'+ miempresa.default_mail +'>',
			#to=[to],
		#)
		#msg.attach_alternative(template, "text/html")
		#msg.send(True)
		#message = "Muchas gracias "+name+", por enviar tus datos, en breve nos pondremos en contacto contigo."
	#return render(request, 'app/mail/base.html',locals())
		#return JsonResponse({
			#'api' : True,
		#})



















#import requests
#import json
#from intranet.models import Team
#def api(request):
#
	#for team in Team.objects.all()
		#pass
#
	#response = requests.get('http://172.16.109.10/api/')
	#response = response.json()
	#b = json.loads(response['data'])
	#company =  Company.objects.get(pk='24')
	#return JsonResponse({
		#'data' : response,
	#})

#def csv(request):
	#import csv
	#with open('csv.csv', newline='', encoding='latin1') as myFile:
		#reader = csv.reader(myFile, delimiter=',', quoting=csv.QUOTE_NONE)
		#for row in reader:
			#for field in row:
				#print (field)
	#return JsonResponse({
		#'upload' : 'ok',
	#})
#
#from sellpoint.models import *
#def load(request):
#
	#wb = load_workbook(filename='Workbook1.xlsx', read_only=True)
	#ws = wb['Sheet1']
#
	#company = Company.objects.get(pk = 23)
	#sellpoint = Sellpoint.objects.get(pk = 9)
	#code = Clave_Productos_Servicios.objects.get(codigo='50181900')
	#units = Clave_Unidades_Medida.objects.get(codigo='H87')
#
	##Product_attributes.objects.filter(product__menu__company = company).delete()
	##Product.objects.filter(menu__company = company).delete()
	##Menu.objects.filter(company = company).delete()
#
#
	#for row in ws.rows:
		#menu = Menu.objects.filter(company=company, name=row[2].value).first()
		#if not menu:
			#menu = Menu()
			#menu.name = row[2].value
			#menu.company = company
			#menu.save()
		#product = Product()
		#product.name = row[0].value
		#product.code = code
		#product.units = units
		#product.quantity = 1
		#product.menu = menu
		#product.save()
		#product.sellpoints.add(sellpoint)
#
		#product_attributes = Product_attributes.objects.get(sellpoint=sellpoint, product=product)
		#product_attributes.price = float(row[1].value)
		#product_attributes.is_favorite = True
		#product_attributes.save()
#
	#return JsonResponse({
		#'upload' : 'ok',
	#})
#
#def script(request):
	#return JsonResponse({
		#'upload' : 'Nada',
	#})
