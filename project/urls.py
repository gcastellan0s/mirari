from django.urls import path
from .views import *

app_name = "project"

urlpatterns = [

    path('entrar/', login_user, name='login_user'),
    path('salir/', user_logout, name='user_logout'),
    path('inicio/', dashboard, name='dashboard'),
    path('api/', api, name='api'),

    path('organizaciones/', CompanyList.as_view(), name='company-list'),
    path('organizacion/agregar/', CompanyCreate.as_view(), name='company-add'),
    path('organizacion/actualizar/<int:pk>/', CompanyUpdate.as_view(), name='company-update'),
    path('organizacion/eliminar/<int:pk>/', CompanyDelete.as_view(), name='company-delete'),

    path('usuarios/', UserList.as_view(), name='user-list'),
    path('usuario/agregar/', UserCreate.as_view(), name='user-add'),
    path('usuario/actualizar/<int:pk>/', UserUpdate.as_view(), name='user-update'),
    path('usuario/eliminar/<int:pk>/', UserDelete.as_view(), name='user-delete'),

    path('usuario/password/<int:pk>/', user_changepassword, name='user-changepassword'),

    path('series/', SerialList.as_view(), name='serial-list'),
    path('serie/agregar/', SerialCreate.as_view(), name='serial-add'),
    path('serie/actualizar/<int:pk>/', SerialUpdate.as_view(), name='serial-update'),
    path('serie/eliminar/<int:pk>/', SerialDelete.as_view(), name='serial-delete'),

    path('wl/', wl, name='wl'),
]