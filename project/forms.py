# -*- encoding: utf-8 -*-
from django.forms import widgets
from django import forms
from django.utils.safestring import mark_safe
from django.contrib.auth.models import Group
from .models import *

class MaterialCheckboxWidget(widgets.CheckboxInput):
	def render(self, name, value, attrs=None):
		return mark_safe(u"""
				<div class="toggle-switch toggle-switch" style="margin-right:15px;">
					%s<i class="toggle-switch__helper"></i>
				</div>
			""" % (super(MaterialCheckboxWidget, self).render(name, value, attrs)))

class MaterialDateWidget(widgets.DateInput):
	def render(self, name, value, attrs=None):
		return mark_safe(u"""
				<div class="input-group">
					<span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
					<div class="form-group">
						%s
						<i class="form-group__bar"></i>
					</div>
				</div>
			"""% (super(MaterialDateWidget, self).render(name, value, attrs)))

class MaterialTextWidget(widgets.TextInput):
	def render(self, name, value, attrs=None):
		return mark_safe(u"""
					%s
			"""% (super(MaterialTextWidget, self).render(name, value, attrs)))

class SelectWidget(widgets.Select):
	def render(self, name, value, attrs=None):
		return mark_safe(u"""
					%s
			"""% (super(SelectWidget, self).render(name, value, attrs)))

class SelectMultipleWidget(widgets.SelectMultiple):
	def render(self, name, value, attrs=None):
		return mark_safe(u"""
					%s
			"""% (super(SelectMultipleWidget, self).render(name, value, attrs)))

class MaterialColorWidget(widgets.TextInput):
	def render(self, name, value, attrs=None):
		return mark_safe(u"""
				<div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
                    </div>
                    %s
                    <i class="color-picker__preview" style="background-color: rgb(228, 228, 228);"></i>
                    <i class="form-group__bar"></i>
                </div>
			"""% (super(MaterialColorWidget, self).render(name, value, attrs)))

class MaterialColorWidget(widgets.TextInput):
	def render(self, name, value, attrs=None):
		return mark_safe(u"""
					<div class="input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                        <div class="form-group">
                            %s
                            <i class="color-picker__preview" v-bind:style="{ backgroundColor: vcolor }"></i>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
			"""% (super(MaterialColorWidget, self).render(name, value, attrs)))

class WYSIWYGWidget(widgets.Textarea):
	def render(self, name, value, attrs=None):
		return mark_safe(u"""
				<div class="wysiwyg"></div>
				%s
			"""% (super(WYSIWYGWidget, self).render(name, value, attrs)))

class CompanyForm(forms.ModelForm):
	class Meta:
		model = Company
		fields = ('name','color','default_mail','contact_mail',)
		widgets={
			'estado':SelectWidget(attrs={'class':'select2',}),
			'persona':SelectWidget(attrs={'class':'select2','data-minimum-results-for-search':'Infinity'}),
		}

class BaseUserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('visible_username','password','password1','password2','groups','is_active','can_change_password','sellpoints','first_name','last_name','email')
		widgets={
			'password':MaterialTextWidget(attrs={'class':'form-control','required':'required','type':'password'}),
			'password1':MaterialTextWidget(attrs={'class':'form-control','required':'required','type':'password'}),
			'password2':MaterialTextWidget(attrs={'class':'form-control','required':'required','type':'password'}),
			'groups':SelectMultipleWidget(attrs={'class':'select2', 'multiple':'multiple',}),
			'can_change_password':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'sellpoints':SelectMultipleWidget(attrs={'class':'select2', 'multiple':'multiple',}),
			'is_active':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
		}

class UserForm(BaseUserForm):
	def __init__(self, *args, **kwargs):
		groups = kwargs.pop('groups')
		if 'sellpoints' in kwargs:
			sellpoints = kwargs.pop('sellpoints')
		else:
			sellpoints = False
		mode = kwargs.pop('mode')
		module = kwargs.pop('module')
		user = kwargs.pop('user')
		super(UserForm, self).__init__(*args, **kwargs)
		self.fields['groups'].queryset = groups
		if mode == 'new':
			self.fields.pop('is_active')
			self.fields.pop('password1')
		elif mode == 'edit':
			self.fields.pop('password')
			self.fields.pop('password1')
			self.fields.pop('password2')
		if module == 'sellpoint':
			self.fields['sellpoints'].queryset = sellpoints
			self.fields.pop('first_name')
			self.fields.pop('last_name')
		if not module == 'sellpoint':
			self.fields.pop('can_change_password')
			self.fields.pop('sellpoints')

class UserPasswordForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('password1','password2')
		widgets={
			'password1':MaterialTextWidget(attrs={'class':'form-control','required':'required','type':'password'}),
			'password2':MaterialTextWidget(attrs={'class':'form-control','required':'required','type':'password'}),
		}

class SerialForm(forms.ModelForm):
	class Meta:
		model = Serial
		fields = ('name','prefix','serial','is_global', 'hexadecimal','is_active')
		widgets={
			'is_active':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'is_global':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
			'hexadecimal':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
		}
	def __init__(self, *args, **kwargs):
		module = kwargs.pop('module')
		super(SerialForm, self).__init__(*args, **kwargs)
		if (module.code == 'sellpoint'):
			self.fields.pop('prefix')
