from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Permission, Group
from .models import *

class PassAdmin(admin.ModelAdmin):
    pass

admin.site.register(Module, PassAdmin)
#admin.site.register(Profile, PassAdmin)
#admin.site.register(Serial, PassAdmin)
#
admin.site.register(Clave_Productos_Servicios, PassAdmin)
#admin.site.register(Clave_Unidades_Medida, PassAdmin)

@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('name', 'parent', 'code','domain')
    search_fields = ('name','code')

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('username','visible_username','company','perfiles', 'is_active', 'active')
    search_fields = ('username','visible_username')
    list_filter = ('company',)
    filter_horizontal = ('user_permissions','groups', 'private_groups')


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('name', 'content_type', 'codename',)

@admin.register(Serial)
class SerialAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('name', 'prefix', 'serial', 'company', 'is_active', 'active')

@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('calle',)
    
@admin.register(File_user)
class File_userAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = False
    list_display = ('code','name','creation_date','module',)
