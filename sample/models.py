# -*- encoding: utf-8 -*-
from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from config.var_local import VAR_LOCAL, VAR_GLOBAL
from mptt.models import MPTTModel, TreeForeignKey
from django.urls import reverse_lazy, reverse
from django.db.models.signals import m2m_changed
from django.utils.safestring import mark_safe
from django.utils import timezone
import datetime

APP = "financialpartners"

VERBOSE_NAME = "Socio prestamista"
VERBOSE_NAME_PLURAL = "Socios prestamistas"
MODEL = "financialpartners"
GENDER_NEW = "un nuevo"
GENDER_THIS = "este"

class Financialpartners(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	actividad = models.ForeignKey('Clave_Actividad_Economica', on_delete=models.CASCADE, blank=True, null=True, help_text="Actividad económica del socio", verbose_name="")
	reference_number = models.CharField('Identificación del socio',max_length=250, unique=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.visible_username

	def add_text(self):
		return "NUEVO "+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')+'?module=financialpartners'

	def get_absolute_url_add(self):
		return reverse(APP+':'+self.MODEL.lower()+'-add')+'?module=financialpartners'

	def get_absolute_url_detail(self):
		return reverse(APP+':'+self.MODEL.lower()+'-detail',kwargs={'pk': self.pk})+'?module=financialpartners'

	def get_absolute_url_update(self):
		return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})+'?module=financialpartners'

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})+'?module=financialpartners'

	def rfcs(self):
		p = ''
		for obj in self.my_fiscaldata().all():
			p += obj.rfc + ', '
		return p[0:len(p)-2]

	def string(self):
		return mark_safe("{0} {1}<br > Email:({2})<br /> Número de referencia: {7}<br /> Actividad comercial: {6} <br />Nacionalidad:{3}<br /> Fecha de nacimiento: {4}<br /> Género: {5} <br />  ".format(
				self.first_name, 
				self.last_name,
				self.email,
				self.country,
				self.birthday,
				self.gender,
				self.actividad,
				self.reference_number,
			)
		) 

	def my_address(self):
		return Address.objects.filter(user = self)

	def my_fiscaldata(self):
		return FiscalData.objects.filter(user = self)

	def my_financialpartners_beneficiaries(self):
		return Financialpartners_beneficiarie.objects.filter(user = self)

	def my_banks(self):
		return Bank.objects.filter(user = self)

	def my_files(self):
		return File_user.objects.filter(user = self, module__code='financialpartners')

	def my_investments(self):
		return Investment.objects.filter(user = self)

