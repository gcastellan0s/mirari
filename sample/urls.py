from django.urls import path
from .views import *

app_name = "financialpartners"

urlpatterns = [
	path('socios-prestamistas/', FinancialpartnersList.as_view(), name='financialpartners-list'),
	path('socio-prestamista/agregar/', FinancialpartnersCreate.as_view(), name='financialpartners-add'),
	path('socio-prestamista/ver/<int:pk>/', FinancialpartnersDetail.as_view(), name='financialpartners-detail'),
	path('socio-prestamista/actualizar/<int:pk>/', FinancialpartnersUpdate.as_view(), name='financialpartners-update'),
	path('socio-prestamista/eliminar/<int:pk>/', FinancialpartnersDelete.as_view(), name='financialpartners-delete'),

]