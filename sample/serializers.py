from rest_framework import serializers
from .models import *

class Clave_Actividad_EconomicaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Clave_Actividad_Economica
		fields = ('id','codigo','nombre','string')