from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
from django.views.generic.detail import SingleObjectMixin, DetailView
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.shortcuts import render
from django.db.models import Q
from django.urls import reverse
from django.contrib import messages
from project.views import log_variables, variables, check_permissions
from project.models import *
from project.forms import *
import json

from .models import *
from .forms import *
from .serializers import *

##############################################################
# xxxx
##############################################################
######### LIST
class FinancialpartnersList(ListView):
	model = Financialpartners
	m = model.MODEL
	template_name = m+'_list.html'
	paginate_by = 30

	def get_queryset(self):
		self.VAR, self.q = log_variables(self.request, module=self.request.resolver_match.app_name), self.request.GET.get('q')
		check_permissions(request=self.request, permission='can_view_'+self.m)
		query = self.model.objects.filter(company=self.VAR['COMPANY'], active=True)
		if self.q:
			query.objects.filter( Q(email__icontains=self.q),Q(username__icontains=self.q),Q(first_name__icontains=self.q),Q(last_name__icontains=self.q) )
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		return context

######### DETAIL
class FinancialpartnersDetail(DetailView):
	model= Financialpartners
	m = model.MODEL
	template_name = m+'_detail.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name)
		check_permissions(request=self.request, permission='can_view_'+self.m)
		return context

######### CREATE
class FinancialpartnersCreate(CreateView):
	model = Financialpartners
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = FinancialpartnersForm
	#userfinancialpartnersform = User_FinancialpartnersForm
	#fiscaldataform = FiscalDataForm

	def get_success_url(self):
		return reverse( self.model.APP+':'+self.m+'-detail', kwargs={'pk': self.object.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		#if self.request.POST:
			#context['userfinancialpartnersform'] = self.userfinancialpartnersform(self.request.POST)
			#context['fiscaldataform'] = self.fiscaldataform(self.request.POST)
		#else:
			#context['userfinancialpartnersform'] = self.userfinancialpartnersform()
			#context['fiscaldataform'] = self.fiscaldataform()
		return context

	def form_valid(self, form):
		form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
		form.instance.set_password(settings.VAR_PRODUCTION['VAR_SECRET_KEY'])
		form.instance.username = self.request.user.company.get_root().code.upper() + '_' + form.instance.reference_number
		form.instance.visible_username = form.instance.reference_number
		return super().form_valid(form)

######### UPDATE
class FinancialpartnersUpdate(UpdateView):
	model, m = Financialpartners, 'financialpartners'
	template_name = m+'_create.html'
	form_class = FinancialpartnersForm

	def get_form_kwargs(self):
		company = Company.objects.get(id=self.request.session.get('company'))
		kwargs = super(FinancialpartnersUpdate, self).get_form_kwargs()
		return kwargs

	def get_success_url(self):
		return reverse('financialpartners:'+self.m+'-detail', kwargs={'pk': self.object.pk})

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_sellpoint', obj=self.object.company)
		return context

######### DELETE
class FinancialpartnersDelete(DeleteView):
	model, m = Financialpartners, 'financialpartners'

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('financialpartners:'+self.m+'-list')
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())

