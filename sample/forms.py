# -*- encoding: utf-8 -*-
from django import forms
from django.forms import widgets
from project.forms import MaterialCheckboxWidget, MaterialDateWidget, MaterialTextWidget, SelectWidget, SelectMultipleWidget, MaterialColorWidget, BaseUserForm, MaterialDateWidget
from project.models import User, Address, Bank, File_user
from invoice.models import FiscalData
from .models import *

class FinancialpartnersForm(forms.ModelForm):
	class Meta:
		model = Financialpartners
		fields = 'first_name', 'last_name', 'email', 'actividad', 'birthday', 'gender', 'country', 'reference_number'
		labels = {
			'first_name':'Nombre(s) del socio',
			'last_name':'Apellidos del socio',
			'birthday':'Fecha de nacimiento',
			'gender':'',
			'country':'',
			'reference_number':'Numero de referencia del socio',
		}
		help_texts = {
			'gender':'Genero',
			'country':'Pais de origen',
		}
		widgets={
			'actividad':SelectWidget(attrs={'class':'select2',}),
			'country':SelectWidget(attrs={'class':'select2',}),
			'gender':SelectWidget(attrs={'class':'select2',}),
			'birthday':MaterialDateWidget(attrs={'class':'date-picker','data-date-format':'d/m/Y', 'data-default-date':'01/01/2000'}),
		}