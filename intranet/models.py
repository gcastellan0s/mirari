# -*- encoding: utf-8 -*-
from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from config.var_local import VAR_LOCAL, VAR_GLOBAL
from mptt.models import MPTTModel, TreeForeignKey
from django.urls import reverse_lazy, reverse
from django.db.models.signals import m2m_changed
from django.utils.safestring import mark_safe
from django.utils import timezone
import datetime

from project.models import User, File_user

APP = "intranet"

VERBOSE_NAME = "Equipo"
VERBOSE_NAME_PLURAL = "Equipos"
MODEL = "team"
GENDER_NEW = "un nuevo"
GENDER_THIS = "este"

class Team(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	name = models.CharField('Nombre del equipo', max_length=255)
	company = models.ForeignKey('project.Company', on_delete=models.PROTECT, related_name='+')
	leaders = models.ManyToManyField('project.User', blank=True, help_text="Responsables del área", related_name='+', verbose_name="")

	active = models.BooleanField(default=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.name

	def add_text(self):
		return "NUEVO "+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')

	def get_absolute_url_add(self):
		return reverse(APP+':'+self.MODEL.lower()+'-add')

	def get_absolute_url_update(self):
		return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})

	def leaders_string(self):
		p = ''
		for obj in self.leaders.all():
			p += obj.visible_username + ', '
		return p[0:len(p)-2]


VERBOSE_NAME = "Empleado"
VERBOSE_NAME_PLURAL = "Empleados"
MODEL = "employee"
GENDER_NEW = "un nuevo"
GENDER_THIS = "este"

class Employee(User):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	extension = models.CharField('Ext. Telefónica', max_length=5)
	employment = models.CharField('Puesto del usuario', max_length=255)
	teams = models.ManyToManyField('Team', blank=True, help_text="Equipo al que pertenece", related_name='+', verbose_name="")

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return self.visible_username

	def add_text(self):
		return "NUEVO "+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')

	def get_absolute_url_add(self):
		return reverse(APP+':'+self.MODEL.lower()+'-add')

	def get_absolute_url_update(self):
		return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})

	def teams_string(self):
		p = ''
		for obj in self.teams.all():
			p += obj.name + ', '
		return p[0:len(p)-2]



Etapa = (
	('proyecto','Proyecto'),
	('oportunidad','Oportunidad'),
	('cotizacion','Cotización'),
	('solicitud','Solicitud'),
	('credito','Credito'),
	('seguimiento','Seguimiento'),
)

VERBOSE_NAME = "Catálogo"
VERBOSE_NAME_PLURAL = "Catálogos"
MODEL = "catalogue"
GENDER_NEW = "un nuevo"
GENDER_THIS = "este"

class Catalogue(models.Model):

	VERBOSE_NAME = VERBOSE_NAME
	VERBOSE_NAME_PLURAL = VERBOSE_NAME_PLURAL
	MODEL = MODEL
	GENDER_NEW = GENDER_NEW
	GENDER_THIS = GENDER_THIS
	APP = APP

	company = models.ForeignKey('project.Company', blank=True, null=True, on_delete=models.CASCADE)
	code = models.CharField('Código del documento', max_length=25, blank=True, null=True)
	name = models.CharField('Nombre del documento', max_length=500)
	description = models.TextField('Notas', max_length=500, blank=True, null=True)
	is_obligatory = models.BooleanField('Es Obligatorio?', default=False, help_text="Este archivo nunca debe de faltar en la solicitud de no ser asi enviará alarmas.")
	
	teams = models.ManyToManyField('Team', verbose_name="", related_name='+', help_text="Equipos involucrados en el documento" , blank=True)
	last_modify_user = models.ForeignKey('project.User', blank=True, null=True, on_delete=models.CASCADE)

	stage = models.CharField('Etapa en que se requiere el documento', choices=Etapa, max_length=144, default='proyecto')
	notify_client = models.BooleanField('Notifica al cliente?', default=False, help_text="Las alarmas se harán llegar a los clientes del credito.")
	notify_user = models.BooleanField('Notifica al usuario?', default=True, help_text="Las alarmas se harán llegar a los usuarios del sistema responsables de la alarma por departamento.")
	is_reusable = models.BooleanField('Reutilizable?', default=True, help_text="Indica si se puede reutilizar para otra línea de crédito")
	expire = models.IntegerField('Expira?', help_text="Este archivo se necesita actualizar en <strong>x</strong> días si no, enviará alarmas. <strong>Si es 0 o vacío nunca expira.</strong>", blank=True, null=True,)

	active = models.BooleanField(default=True)
	is_active = models.BooleanField('Esta activo?', default=True)

	class Meta:
		default_permissions = ()
		verbose_name = VERBOSE_NAME
		verbose_name_plural = VERBOSE_NAME_PLURAL
		permissions = (
			('can_view_'+ MODEL.lower(), 'Ve lista de '+ VERBOSE_NAME_PLURAL.lower()),
			('can_edit_'+ MODEL.lower(), 'Modifica '+ VERBOSE_NAME_PLURAL.lower()),
			('can_create_'+ MODEL.lower(), 'Crea '+ VERBOSE_NAME_PLURAL.lower()),
			('can_delete_'+ MODEL.lower(), 'Elimina '+ VERBOSE_NAME_PLURAL.lower()),
		)

	def __str__(self):
		return '({0}) {1}'.format(self.code, self.name)

	def add_text(self):
		return "NUEVO "+ self.VERBOSE_NAME.upper(), "Agregar "+ self.VERBOSE_NAME.lower(), "Crea "+self.GENDER_NEW+" "+ self.VERBOSE_NAME.lower() +" en el sistema"

	def edit_text(self):
		return "EDITAR "+ self.VERBOSE_NAME.upper(), "Editar "+ self.VERBOSE_NAME.lower(), "Edita los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" en el sistema"

	def delete_text(self):
		return "ELIMINAR "+ self.VERBOSE_NAME.upper(), "Eliminar "+ self.VERBOSE_NAME.lower(), "Elimina los datos de "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower(),"Se eliminará "+self.GENDER_THIS+" "+self.VERBOSE_NAME.lower()+" y todos sus datos asociados!"

	def get_absolute_url_list(self):
		return reverse(APP+':'+self.MODEL.lower()+'-list')

	def get_absolute_url_add(self):
		return reverse(APP+':'+self.MODEL.lower()+'-add')

	def get_absolute_url_update(self):
		return reverse(APP+':'+self.MODEL.lower()+'-update',kwargs={'pk': self.pk})

	def get_absolute_url_delete(self):
		return reverse(APP+':'+self.MODEL.lower()+'-delete',kwargs={'pk': self.pk})

	def teams_string(self):
		p = ''
		for obj in self.teams.all():
			p += obj.name + ', '
		return p[0:len(p)-2]

