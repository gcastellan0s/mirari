from project.views import log_variables, variables, check_permissions
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
from django.views.generic.detail import SingleObjectMixin, DetailView
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.shortcuts import render
from django.db.models import Q
from django.urls import reverse
from django.contrib import messages
from project.models import *
from project.forms import *
import json

from .models import *
from .forms import *
from .serializers import *

##############################################################
# TEAM
##############################################################
######### LIST
class TeamList(ListView):
	model = Team
	m = model.MODEL
	template_name = m+'_list.html'
	paginate_by = 30

	def get_queryset(self):
		self.VAR, self.q = log_variables(self.request, module=self.request.resolver_match.app_name), self.request.GET.get('q')
		check_permissions(request=self.request, permission='can_view_'+self.m)
		query = self.model.objects.filter(company=self.VAR['COMPANY'], active=True)
		if self.q:
			query.objects.filter( name = self.q ) 
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		return context

######### CREATE
class TeamCreate(CreateView):
	model = Team
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = TeamForm

	def get_form_kwargs(self):
		company = Company.objects.get(id=self.request.session.get('company'))
		kwargs = super().get_form_kwargs()
		kwargs['leaders'] = User.objects.filter(company = company, is_superuser = False, active=True)
		return kwargs

	def get_success_url(self):
		return reverse(APP+':'+self.m+'-list')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
		return super().form_valid(form)

######### UPDATE
class TeamUpdate(UpdateView):
	model = Team
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = TeamForm

	def get_form_kwargs(self):
		company = Company.objects.get(id=self.request.session.get('company'))
		kwargs = super().get_form_kwargs()
		kwargs['leaders'] = User.objects.filter(company = company, is_superuser = False, active=True)
		return kwargs

	def get_success_url(self):
		return reverse(APP+':'+self.m+'-list')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
		return context

######### DELETE
class TeamDelete(DeleteView):
	model = Team
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse(APP+':'+self.m+'-list')
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())




##############################################################
# Employee
##############################################################
######### LIST
class EmployeeList(ListView):
	model = Employee
	m = model.MODEL
	template_name = m+'_list.html'
	paginate_by = 100

	def get_queryset(self):
		self.VAR, self.q = log_variables(self.request, module=self.request.resolver_match.app_name), self.request.GET.get('q')
		check_permissions(request=self.request, permission='can_view_'+self.m)
		query = self.model.objects.filter(company=self.VAR['COMPANY'], active=True)
		if self.q:
			query = query.filter( Q(username__contains = self.q) | Q(email__contains = self.q) | Q(first_name__contains = self.q) | Q(last_name__contains = self.q) ) 
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		return context

######### CREATE
class EmployeeCreate(CreateView):
	model = Employee
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = EmployeeForm

	def get_form_kwargs(self):
		company = Company.objects.get(id=self.request.session.get('company'))
		kwargs = super().get_form_kwargs()
		kwargs['teams'] = Team.objects.filter(company = company, active=True)
		return kwargs

	def get_success_url(self):
		return reverse(APP+':'+self.m+'-list')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
		form.instance.set_password('CREDIPyME')
		form.instance.username = self.request.user.company.get_root().code.upper() + '_' + form.instance.visible_username
		return super().form_valid(form)



######### UPDATE
class EmployeeUpdate(UpdateView):
	model = Employee
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = EmployeeForm

	def get_form_kwargs(self):
		company = Company.objects.get(id=self.request.session.get('company'))
		kwargs = super().get_form_kwargs()
		kwargs['teams'] = Team.objects.filter(company = company, active=True)
		return kwargs

	def get_success_url(self):
		return reverse(APP+':'+self.m+'-list')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
		return context

######### DELETE
class EmployeeDelete(DeleteView):
	model = Employee
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse(APP+':'+self.m+'-list')
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())

##############################################################
# CATALOGUE
##############################################################
######### LIST
class CatalogueList(ListView):
	model = Catalogue
	m = model.MODEL
	template_name = m+'_list.html'
	paginate_by = 30

	def get_queryset(self):
		self.VAR, self.q = log_variables(self.request, module=self.request.resolver_match.app_name), self.request.GET.get('q')
		check_permissions(request=self.request, permission='can_view_'+self.m)
		query = self.model.objects.filter(company=self.VAR['COMPANY'], active=True)
		if self.q:
			query.objects.filter( name = self.q ) 
		return query

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['q'] = self.model, self.VAR, self.q
		return context

######### CREATE
class CatalogueCreate(CreateView):
	model = Catalogue
	m = model.MODEL
	template_name = m + '_create.html'
	form_class = CatalogueForm

	def get_form_kwargs(self):
		company = Company.objects.get(id=self.request.session.get('company'))
		kwargs = super().get_form_kwargs()
		kwargs['teams'] = Team.objects.filter(company = company, active=True)
		return kwargs

	def get_success_url(self):
		return reverse(APP+':'+self.m+'-list')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)	
		context['model'], context['VAR'], context['class']  = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'CreateView'
		check_permissions(request=self.request, permission='can_create_'+self.m)
		return context

	def form_valid(self, form):
		form.instance.company = Company.objects.get(pk=self.request.session.get('company'))
		return super().form_valid(form)



######### UPDATE
class CatalogueUpdate(UpdateView):
	model = Catalogue
	m = model.MODEL
	template_name = m+'_create.html'
	form_class = CatalogueForm

	def get_form_kwargs(self):
		company = Company.objects.get(id=self.request.session.get('company'))
		kwargs = super().get_form_kwargs()
		kwargs['teams'] = Team.objects.filter(company = company, active=True)
		return kwargs

	def get_success_url(self):
		return reverse(APP+':'+self.m+'-list')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['model'], context['VAR'], context['class'] = self.model, log_variables(self.request, module=self.request.resolver_match.app_name), 'UpdateView'
		check_permissions(request=self.request, permission='can_edit_'+self.m, obj=self.object.company)
		return context

######### DELETE
class CatalogueDelete(DeleteView):
	model = Catalogue
	m = model.MODEL

	def get(self, request, *args, **kwargs):
		return self.post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse(APP+':'+self.m+'-list')
		
	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		check_permissions(request=self.request, permission='can_delete_'+self.m, obj=self.object.company)
		self.object.active = False
		self.object.save()
		messages.success(self.request, 'Se eliminó correctamente el registro de la base de datos')
		return HttpResponseRedirect(self.get_success_url())





