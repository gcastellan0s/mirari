from rest_framework import serializers
from .models import *

class TeamaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Team
		fields = ('name',)