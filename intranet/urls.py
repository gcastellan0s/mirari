from django.urls import path
from .views import *

app_name = "intranet"

urlpatterns = [
	path('equipos/', TeamList.as_view(), name='team-list'),
	path('equipo/agregar/', TeamCreate.as_view(), name='team-add'),
	path('equipo/actualizar/<int:pk>/', TeamUpdate.as_view(), name='team-update'),
	path('equipo/eliminar/<int:pk>/', TeamDelete.as_view(), name='team-delete'),

	path('empleados/', EmployeeList.as_view(), name='employee-list'),
	path('empleado/agregar/', EmployeeCreate.as_view(), name='employee-add'),
	path('empleado/actualizar/<int:pk>/', EmployeeUpdate.as_view(), name='employee-update'),
	path('empleado/eliminar/<int:pk>/', EmployeeDelete.as_view(), name='employee-delete'),

	path('catalogos/', CatalogueList.as_view(), name='catalogue-list'),
	path('catalogo/agregar/', CatalogueCreate.as_view(), name='catalogue-add'),
	path('catalogo/actualizar/<int:pk>/', CatalogueUpdate.as_view(), name='catalogue-update'),
	path('catalogo/eliminar/<int:pk>/', CatalogueDelete.as_view(), name='catalogue-delete'),
]