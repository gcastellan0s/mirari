# -*- encoding: utf-8 -*-
from django import forms
from django.forms import widgets
from project.forms import MaterialCheckboxWidget, MaterialDateWidget, MaterialTextWidget, SelectWidget, SelectMultipleWidget, MaterialColorWidget, BaseUserForm, MaterialDateWidget
from .models import *

class TeamForm(forms.ModelForm):
	class Meta:
		model = Team
		fields = 'name','leaders'
		widgets={
			'leaders':SelectMultipleWidget(attrs={'class':'select2',}),
		}
	def __init__(self, *args, **kwargs):
		leaders = kwargs.pop('leaders')
		super().__init__(*args, **kwargs)
		self.fields['leaders'].queryset = leaders

class EmployeeForm(forms.ModelForm):

	class Meta:
		model = Employee
		fields = 'visible_username', 'first_name', 'last_name', 'email', 'birthday', 'gender', 'extension', 'employment', 'teams'
		labels={
			'gender':'',
		}
		help_texts={
			'gender':'Género',
		}
		widgets={
			'gender':SelectWidget(attrs={'class':'select2',}),
			'birthday':MaterialDateWidget(attrs={'class':'date-picker', 'data-date-format':'d/m/Y',}),
			'teams':SelectMultipleWidget(attrs={'class':'select2',}),
		}

	def __init__(self, *args, **kwargs):
		teams = kwargs.pop('teams')
		super().__init__(*args, **kwargs)
		self.fields['teams'].queryset = teams
	

class CatalogueForm(forms.ModelForm):

	class Meta:
		model = Catalogue
		fields = 'code', 'name', 'description', 'teams', 'is_active', 'expire'

		widgets={
			'teams':SelectMultipleWidget(attrs={'class':'select2',}),
			'is_active':MaterialCheckboxWidget(attrs={'class':'toggle-switch__checkbox', 'style':'display:none;'}),
		}

	def __init__(self, *args, **kwargs):
		teams = kwargs.pop('teams')
		super().__init__(*args, **kwargs)
		self.fields['teams'].queryset = teams
	
